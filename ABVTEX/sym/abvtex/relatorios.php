<?php
	require_once("connect.inc");
	include_once("functions.php");
	include_once("modules/auditoria/audi_libs.php");
    require_once("../date.inc");
	require_once("libs/lib.log.php");
	require_once("libs/lib.arrays.php");
	
	//Formato
	if($_GET['fm'] == 'exc'){
    	include('libs/excel_header.php');
    	
    	exportSetHTMLHeaders('relatorio_'.$_GET['ac']);
    }
	
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
<?php if($_GET['fm'] != 'exc'){ ?>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		

		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<?php if((($_GET['ac'] == 'dinamico') && ($_GET['ft'] == '1')) || ($_GET['ac'] == 'fornecedores') || ($_GET['ac'] == 'subcontratados')){ ?><link href="css/style_fs.css" rel="stylesheet" type="text/css" /> <?php } ?>
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->		
			<script type="text/javascript" src="js/jquery.js"></script>
			
			<script type="text/javascript" src="js/formValidation.js"></script>
			
			<script type="text/javascript" src="js/jquery.maskedinput-1.3.js"></script>
			
			
			<script type="text/javascript">
				//Mascaras de entrada
				jQuery(function($){
					$(".data").mask("99/99/9999");
					$(".telefone").mask("(99) 9999-9999");
					$(".celular").mask("(99) 9999-9999");
					$(".celularSP").mask("(99) 99999-9999");
					$(".cpf").mask("999.999.999-99");
					$(".cep").mask("99999-999");
					$(".cnpj").mask("99.999.999/9999-99");
					$(".placa").mask("aaa-9999");
					//$(".idFornecedor").mask("99999999999999");
				});
			</script>
			
			
			<link rel="stylesheet" type="text/css" href="plugins/css/main.css" />
		    <link rel="stylesheet" type="text/css" href="plugins/css/jquery.jqtimeline.css" />
	        <script type="text/javascript" src="plugins/js/jquery.jqtimeline.js"></script>
			
			
<?php 
	}else{ 
		exportSetStyleHeader();
	} ?>			
			
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
				<?php 
					$acao = $_GET['ac'];
					if($_GET['fm'] == ''){ include('topo.php'); 
				?>
			
				<div class="mainTitle">Relatórios</div>
				<?php } ?>

			
				<?php
					switch($acao){
						case '': include('modules/relatorios/relatorios.php'); break;
						case 'num_cad': include('modules/relatorios/num_cad.php'); break;
						case 'num_cad_for': include('modules/relatorios/num_cad_for.php'); break;
						case 'num_cad_sub': include('modules/relatorios/num_cad_sub.php'); break;
						case 'num_cad_per': include('modules/relatorios/num_cad_per.php'); break;
						case 'auditorias': include('modules/relatorios/auditorias.php'); break;
						case 'checklist_detail': include('modules/relatorios/checklist_detail.php'); break;
						case 'checklist_detail_item': include('modules/relatorios/checklist_detail_item.php'); break;
						case 'rel_atv': include('modules/relatorios/rel_atv.php'); break;
						case 'aud_real': include('modules/relatorios/aud_real.php'); break;
						case 'dinamico': include('modules/relatorios/dinamico.php'); break;
						case 'fornecedores': include('modules/relatorios/fornecedores.php'); break;
						case 'subcontratados': include('modules/relatorios/subcontratados.php'); break;
						case 'sat_cert': include('modules/relatorios/rel_sat_cert.php'); break;
						case 'sat_quest': include('modules/relatorios/rel_sat_quest.php'); break;
						case 'pesq_sat': include('modules/relatorios/rel_pesq_sat.php'); break;
						case 'historico': include('modules/relatorios/historico.php'); break;
						case 'historico2': include('modules/relatorios/historico2.php'); break;
						case 'audi_quant': include('modules/relatorios/auditorias_quant.php'); break;
						case 'audi_qtd_analitico': include('modules/relatorios/auditorias_quant_analitica.php'); break;
						case 'linha-tempo': include('modules/relatorios/linha-tempo.php'); break;
						case 'rel_auditor': include('modules/relatorios/rel_auditor.php'); break;
						case 'numero_anual': include('modules/relatorios/numero_anual.php'); break;
						case 'empresas_por_estado': include('modules/relatorios/empresas_por_estado.php'); break;
						case 'empresas_por_cidade': include('modules/relatorios/empresas_por_cidades.php'); break;
						case 'rel_auditor_excluidos': include('modules/relatorios/rel_auditor_excluidos.php'); break;
						case 'avcb_funcionarios': include('modules/relatorios/avcb_funcionarios.php'); break;
					}
				?>
			
			
			
			
			<!-- FIM CONTENT -->
			
		</div>	
		
	</body>

</html>	
