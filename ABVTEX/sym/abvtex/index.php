<?PHP
	setcookie("empr"    ,""      ,time()+1,'/sym/abvtex/');
	setcookie("empresa" ,""   	 ,time()+1,'/sym/abvtex/');
    setcookie("pass"    ,""      ,time()+1,'/sym/abvtex/');
    setcookie("user"    ,""      ,time()+1,'/sym/abvtex/');
    setcookie("perm"    ,""      ,time()+1,'/sym/abvtex/');
    setcookie("nomeUser",""  	 ,time()+1,'/sym/abvtex/');
    setcookie("sess"	,""		 ,time()+1,'/sym/abvtex/');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/formValidation.js"></script>
		<!-- JS -->
	</head>

	<body>
		
		<!-- CONTENT -->
		<center>
		<div style="width:90%; height:400px; margin: 60px auto 0 auto; background: url(images/login/screen_fundo.jpg) no-repeat center;" align="center">
			
			<div style="width:680px; height:280px;">
				
				<div style="background: url(images/white_60.png); width:290px; height:200px; padding:20px; float:left; margin-right:20px;" align="left">
				
					<div style="font-size: 21px; color: black; margin-bottom:5px;">Ainda n&atilde;o &eacute; cadastrado?</div>
					<div><a href="cadastro.php">Clique aqui para efetuar o cadastro</a></div>

					<div style="margin-top: 50px; font-size: 21px; color: black; margin-bottom:5px;">Manual de instru��es</div>
					<div><a target="_blank" href="http://www.techsocial.com.br/sym/abvtex/manuais/Manual-Fornecedores.pdf">Clique aqui para efetuar o download</a></div>
				
				</div>
				
				<div style="background: url(images/white_60.png); width:290px; height:200px; padding:20px; float:left;" align="left">
					<div style="font-size: 21px; color: black; margin-bottom:5px;">Acesse aqui:</div>
					
					<center>
						<div align="center" style="width:250px;font-size:90.01%;padding:5px;font-weight:bold;<?PHP if(''.$_GET["ern"]=='' && $message=="") echo('display:none;'); ?>text-align:center;" id="idx_message" class="ui-state-error">&nbsp;
					    <?PHP if($_GET["ern"]==2)  echo('Acesso Negado - Sua sess�o expirou. Logue novamente no sistema.'); ?>
					    <?PHP if($_GET["ern"]==3)  echo('Acesso Negado - Usu�rio ou senha inv�lidos.'); ?>
					    <?PHP if($_GET["ern"]==4)  echo('Acesso Negado - Voc� n�o est� logado no sistema.'); ?>
					    <?PHP if($_GET["ern"]==5)  echo('Acesso Negado - Voc� tentou acessar informa��es n�o pertinentes ao seu escopo de atua��o.'); ?>
					    <?PHP if($_GET["ern"]==6)  echo('Voc� ainda n�o est� logado no sistema. Por favor, insira seu usu�rio e senha para prosseguir.'); ?>
					    <?PHP if($_GET["ern"]==7)  echo('Acesso Negado - Voc� tentou acessar informa��es n�o pertinentes ao seu escopo de atua��o.'); ?>
					    <?PHP if($_GET["ern"]==8)  echo('O sistema detectou uma mudan�a no comportamento da rede. Por favor, fa�a login novamente para retomar suas atividades.'); ?>
					    <?PHP if($_GET["ern"]==10) echo('Sess�o Encerrada - Obrigado por sua participa��o.'); ?>
					    <?PHP if($_GET["ern"]==11) echo('O sistema est� em manuten��o e estar� de volta em breve.'); ?>
					    <?PHP if($message !="")  echo($message); ?>
					    </div>
				    </center>
					
					<form name="frm" action="indexin.php?lc=<?=$_GET['lc']?>&pg=<?=$_GET['pg']?>&ac=<?=$_GET['ac']?>&frid=<?=$empr?>&audi=<?=$_GET['audi']?>" method="post">
						<ul>
							<li>Usu&aacute;rio</li>
							<li><input type="text" name="user" id="user" style="width:290px;" /></li>
							<li>Senha</li>
							<li><input type="password" name="pass" id="pass" style="width:290px;" /></li>
							<li><input type="submit" value="Acessar &gt;" /></li>
							<li>
								<a href="esquecisenha.php">Esqueci minha senha</a>
							</li>
						</ul>
					</form>
				</div>
			</div>
			
			<div style="width:680px; height:100px;">
				<img src="images/logo_tech.png"   alt="ABVTex"     style="height:100px;background:white;border:4px solid white;float:left;" />
				<img src="images/logo_abvtex.png" alt="TechSocial" style="height:100px;background:white;border:4px solid white;float:right;" />
			</div>
    	</div>
		
		<div style="width:90%; height:40px; margin: 0px auto 0 auto; background: url(images/pc_degrade.png) no-repeat top;" align="center">
    	&nbsp;
		</div>
		</center>
    	<!-- FIM CONTENT --> 
		
	</body>

</html>