<?php
	if($_COOKIE["perm"]!="Administradora") { echo('<script> history.back(); </script>'); exit(0); }
	
	require_once("connect.inc");
	include_once("functions.php");
    require_once("libs/date.inc");
	require_once("libs/lib.log.php");
	
	$acao       = ''.$_GET['ac'];
	$acao_model = ''.$_GET['mac'];
	$cat        = intval(0+$_GET['cat']);
	$sub        = intval(0+$_GET['sub']);
	
	if($acao_model!='')
	{
		switch($acao_model){
			case 'catadd'   : include_once('modules/adm/categorias/cat_model_add.php');      break;
			case 'catupd'   : include_once('modules/adm/categorias/cat_model_edit.php');     break;
			case 'catsubadd': include_once('modules/adm/categorias/cat_model_sub_add.php');  break;
			case 'catsubupd': include_once('modules/adm/categorias/cat_model_sub_edit.php'); break;
		}
	}
	
	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/visibility.js"></script>
			<script type="text/javascript" src="js/time_validation.js"></script>
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
			<!-- HEADER -->
				<?php include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			<?php
				switch($acao){
					case '':          include_once('modules/adm/categorias/cat_list.php');     break;
					case 'catnew':    include_once('modules/adm/categorias/cat_new.php');      break;
					case 'catedt':    include_once('modules/adm/categorias/cat_edit.php');     break;
					case 'catsubnew': include_once('modules/adm/categorias/cat_sub_new.php');  break;
					case 'catsubedt': include_once('modules/adm/categorias/cat_sub_edit.php'); break;
				}
			?>
		</div>
	</body>
</html>