<?PHP
	require_once("connect.inc");
	include_once("functions.php");
    require_once("../date.inc");
    require_once('libs/lib.log.php');
    require_once('libs/lib.arrays.php');
    
    require_once('libs/lib.sendmail.php');
    require_once('libs/mailLayout/lib.mailLayout.php');

	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	
	//GETs
	$fornecedor = tratarVariaveis($_GET['fr']);
	$action = $_GET['ac'];
	$empresa = $_COOKIE['empresa'];
	
	if($fornecedor == ''){ header('Location: fornecedores.php?ac=fornecedores'); exit(0); }
	
	
	if($_COOKIE['perm'] != 'Administradora'){
		
		if($_GET['tipo'] != '2'){
			$sql_fornecedor = "SELECT * FROM CC_Fornecedor
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor  
				JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.ativo = '1'
			WHERE
				CC_Fornecedor.fornecedor = '".$fornecedor."'
				AND CC_Fr.empresa = '".$empresa."'
				AND CC_Fr_Vinculo.status  = '2'
				AND CC_Fr_Vinculo.empresa = '".$_COOKIE['empr']."' LIMIT 1 ";
		}else{
			$sql_fornecedor = "SELECT * FROM CC_Fornecedor
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor  
				LEFT JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor
			WHERE
				CC_Fornecedor.fornecedor = '".$fornecedor."'
				AND CC_Fr.empresa = '".$empresa."'
			LIMIT 1 ";
		}
		
	}else{
		$sql_fornecedor = "SELECT * FROM CC_Fornecedor
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor  
				LEFT JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor
			WHERE
				CC_Fornecedor.fornecedor = '".$fornecedor."'
				AND CC_Fr.empresa = '".$empresa."'
			LIMIT 1 ";
	}
	$query_fornecedor = mysql_query($sql_fornecedor) or die(mysql_error());

	
	if(mysql_num_rows($query_fornecedor)<=0) { header('Location: fornecedores.php?ac=fornecedores'); exit(0); }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?PHP echo $metaDescription; ?>">
		<meta name="keywords" value="<?PHP echo $metaKeywords; ?>">
		<meta name="copyright" content="<?PHP echo $metaCopyright; ?>">
		<meta name="date" content="<?PHP echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/formValidation.js"></script>
			
			<script type="text/javascript" src="js/jquery.maskedinput-1.3.js"></script>
			
			<script type="text/javascript">				
				//Mascaras de entrada
						jQuery(function($){
							$(".data").mask("99/99/9999");
							$(".telefone").mask("(99) 9999-9999");
							$(".celular").mask("(99) 9999-9999");
							$(".celularSP").mask("(99) 99999-9999");
							$(".cpf").mask("999.999.999-99");
							$(".cep").mask("99999-999");
							$(".cnpj").mask("99.999.999/9999-99");
							$(".placa").mask("aaa-9999");
						});

		</script>

		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
			<!-- HEADER -->
			<?PHP include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			<!-- CONTENT -->
			<div class="mainTitle"><?php if(mysql_result($query_fornecedor,0,'CC_Fr.classe')) echo $arr_tipo[mysql_result($query_fornecedor,0,'CC_Fr.classe')]; ?> - <?=mysql_result($query_fornecedor,0,'CC_Fornecedor.razaoSocial') ?> <a href="fornecedores.php?ac=fornecedores" style="float:right;"><img src="images/voltar.png" alt="Voltar"></a></div>
			
			
			<!-- MENU FORNECEDOR -->
			<div class="menu_sec">
				<ul>
					<li style="<?PHP if($action == 'cadastro'){ echo ' background: #333333; '; }?>">
    					<a href="?&fr=<?=$fornecedor ?>&ac=cadastro<?php echo (($_GET['tipo'] == '2')?"&tipo=2":"");?>">Dados</a>
    				</li>
    				
    				<?php
    					if($_GET['tipo'] != '2'){
    						$passo = mysql_result($query_fornecedor,0,'CC_Fr_Vinculo.passo');
    					}else{
    						$passo = '3';
    					}
    				?>
    				
    				
    			<?php if(($passo >= '') || ($_COOKIE['perm'] == 'Varejista')){ ?>
    			
					<?php if( 

							(					
								( ( ($passo >= '1') || ($_COOKIE['perm'] == 'Varejista')  ) ) 
							||  
								( ($_GET['tipo'] == '2') && ($passo >= '1') )
							)
								
							){ ?>
						<li style="<?PHP if(($action == 'subcontratados')){ echo ' background: #333333; '; }?>">
							<a href="?&fr=<?=$fornecedor ?>&ac=subcontratados<?php echo (($_GET['tipo'] == '2')?"&tipo=2":"");?>">Subcontratados</a>
						</li>
					<?php } if(($passo >= '2') || ($_COOKIE['perm'] == 'Varejista')){ ?>
						<li style="<?PHP if($action == 'documentos'){ echo ' background: #333333; '; }?>">
							<a href="?&fr=<?=$fornecedor ?>&ac=documentos<?php echo (($_GET['tipo'] == '2')?"&tipo=2":"");?>">Documentos</a>
						</li>
					<?php } if(($passo >= '3') || ($_COOKIE['perm'] == 'Varejista')){ ?>
						<li style="<?PHP if($action == 'auditoria'){ echo ' background: #333333; ';}?>">
							<a href="?&fr=<?=$fornecedor ?>&ac=auditoria<?php echo (($_GET['tipo'] == '2')?"&tipo=2":"");?>">Auditorias</a>
						</li>
					<?php } ?>
					
				<?php } ?>
					<li style="<?PHP if($action == 'fornecimento'){ echo ' background: #333333; ';}?>">
						<a href="?&fr=<?=$fornecedor ?>&ac=fornecimento<?php echo (($_GET['tipo'] == '2')?"&tipo=2":"");?>">Fornecimento</a>
					</li>
				<?php if(($_COOKIE['perm'] == 'Varejista')||($_COOKIE['perm'] == 'Administradora')){ ?>
					<li style="<?PHP if(($action == 'denuncia') || ($action == 'denunciaAdd') || ($action == 'denunciaEdit')){ echo ' background: #333333; ';}?>">
						<a href="?&fr=<?=$fornecedor ?>&ac=denuncia<?php echo (($_GET['tipo'] == '2')?"&tipo=2":"");?>">Den�ncia</a>
					</li>
				<?php } ?>
				</ul>
			</div>
			<br clear="all" />
			<!-- FIM MENU FORNECEDOR -->
			<?PHP
				$action = $_GET['ac'];
				switch($action){
					case 'cadastro': include('modules/cadastro/cadastroatualizar_for.php'); 
						break;
					case 'auditoria': include('modules/auditoria/auditorias_cert.php'); 
						break;
					case 'documentos': include('modules/documento/documentos.php'); 
						break;
					case 'subcontratados': include('modules/subcontratado/sub_listagem.php'); 
						break;
					case 'fornecimento': include('modules/subcontratado/fornecimento.php'); 
						break;
					case 'denuncia': include('modules/fornecedores/denuncia.php'); 
						break;
					case 'denunciaAdd': include('modules/fornecedores/denuncia_add.php'); 
						break;
					case 'denunciaEdit': include('modules/fornecedores/denuncia_edit.php'); 
						break;
					case 'alterar': include('modules/fornecedores/alterar.php'); 
						break;
					
				}
			?>
		</div>
	</body>
</html>	