<?php
	$sql = "
	SELECT
		UPPER(
			Audi_Fornecedor.certificadora
		) AS Certificadora,
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 0 THEN
				1
			END
		) AS 'Certificacao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 1 THEN
				1
			END
		) AS 'Manutencao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 2 THEN
				1
			END
		) AS 'Certificacao - Plano',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 3 THEN
				1
			END
		) AS 'Manutencao - Plano',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 4 THEN
				1
			END
		) AS 'Suspensao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 5 THEN
				1
			END
		) AS 'Recertificacao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 6 THEN
				1
			END
		) AS 'Recertificacao - Plano',
		COUNT(*) AS 'Total'
	FROM
		Audi_Fornecedor
	WHERE
		YEAR (Audi_Fornecedor.`data`) = YEAR (NOW())
	GROUP BY
		Audi_Fornecedor.certificadora;
	" ;
	
	$query = mysql_query( $sql ) or die( mysql_error() ) ;
	
	if( mysql_num_rows( $query ) > 0 ) {
?>
	<font style="font-size:15px; font-weight: bold;"><?=htmlentities("Quantidade de auditorias por Certificadora / Ano corrente : ", ENT_QUOTES, "ISO-8859-1")?><?php echo date("Y"); ?></font>
	<table width="100%" align="center" cellspacing="0" cellpadding="4" style="margin: 10px auto 50px auto;">
		
		<tr class="" align="center" valign="middle" style="background:#F7E0BB; ">
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Certificadora</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Certificação</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Manutenção</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Certificação&nbsp;-&nbsp;Plano</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Manutenção&nbsp;-&nbsp;Plano</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Suspensão</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Recertificação</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Recertificação&nbsp;-&nbsp;Plano</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Total</b></td>
		</tr>
		
		<?php
			$i=0; 
			while( $values = mysql_fetch_array( $query ) ) {
			?>
			<tr align="center" class="<?php if((($i++)%2)==0) echo "zebra-dark"; ?> hover">
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Certificadora']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Certificacao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Manutencao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Certificacao - Plano']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Manutencao - Plano']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Suspensao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Recertificacao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Recertificacao - Plano']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Total']?></td>
			</tr>

		<?php 
			}//Fim while
		} else {
			echo '<center><br><br><br><font style="font-size:18px; font-weigth:bold;">Dados insuficientes para gerar o relatório.</font><br><br><a href="javascript:history.back(-1)">Voltar</a><br><br><br></center>';
		} ?>
	</table>
	
<?php
	$sql = "
	SELECT
		UPPER(
			Audi_Fornecedor.certificadora
		) AS Certificadora,
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 0 THEN
				1
			END
		) AS 'Certificacao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 1 THEN
				1
			END
		) AS 'Manutencao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 2 THEN
				1
			END
		) AS 'Certificacao - Plano',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 3 THEN
				1
			END
		) AS 'Manutencao - Plano',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 4 THEN
				1
			END
		) AS 'Suspensao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 5 THEN
				1
			END
		) AS 'Recertificacao',
		COUNT(
			CASE
			WHEN Audi_Fornecedor.tipo = 6 THEN
				1
			END
		) AS 'Recertificacao - Plano',
		COUNT(*) AS 'Total'
	FROM
		Audi_Fornecedor
	WHERE
		YEAR (Audi_Fornecedor.`data`) = YEAR (NOW()) - 1
	GROUP BY
		Audi_Fornecedor.certificadora;
	" ;
	
	$query = mysql_query( $sql ) or die( mysql_error() ) ;
	
	if( mysql_num_rows( $query ) > 0 ) {
?>
	<font style="font-size:15px; font-weight: bold;"><?=htmlentities("Quantidade de auditorias por Certificadora / Ano anterior : ", ENT_QUOTES, "ISO-8859-1")?><?php echo date("Y") - 1; ?></font>
	<table width="100%" align="center" cellspacing="0" cellpadding="4" style="margin: 10px auto 50px auto;">
		
		<tr align="center" class="" valign="middle" style="background:#F7E0BB; ">
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Certificadora</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Certificação</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Manutenção</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Certificação&nbsp;-&nbsp;Plano</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Manutenção&nbsp;-&nbsp;Plano</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Suspensão</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Recertificação</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Recertificação&nbsp;-&nbsp;Plano</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Total</b></td>
		</tr>
		
		<?php
			$i=0; 
			while( $values = mysql_fetch_array( $query ) ) {
			?>
			<tr align="center" class="<?php if((($i++)%2)==0) echo "zebra-dark"; ?> hover">
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Certificadora']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Certificacao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Manutencao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Certificacao - Plano']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Manutencao - Plano']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Suspensao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Recertificacao']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Recertificacao - Plano']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Total']?></td>
			</tr>

		<?php 
			}//Fim while
		} else {
			echo '<center><br><br><br><font style="font-size:18px; font-weigth:bold;">Dados insuficientes para gerar o relatório.</font><br><br><a href="javascript:history.back(-1)">Voltar</a><br><br><br></center>';
		} ?>
	</table>
		