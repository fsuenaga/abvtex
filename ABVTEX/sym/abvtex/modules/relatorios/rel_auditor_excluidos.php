<?php if($_GET['fm'] != 'exc'){ ?>
<script type="text/javascript">
	
	function exibirHistorico(id){
		if($('.'+id).attr('exc') == '1'){
			$('.'+id).css('display','none');
			$('.'+id).attr('exc','0');
		}else{
			$('.'+id).css('display','');
			$('.'+id).attr('exc','1');
		}
	}

</script>
<?php } ?>

<?php if($_GET['ft'] != 1){ ?>
	<form action="" method="get" target="_blank">
		<input type="hidden" name="ac" value="rel_auditor_excluidos" />
		<input type="hidden" name="ft" value="1" />
		<ul>
			<li style="font-size:15px;">
				Filtros
				<a style="float:right;" href="relatorios.php"><img alt="Voltar" src="images/voltar.png"></a>
			</li>
			<li style="font-weight:normal;">Selecione os filtros desejados</li>
			
				<li class="zebra-dark">Estado</li>
				<li>
					<select name="uf">
						<option value="">Todos</option>
						<?php 
						$sql = "SELECT * FROM TB_Estado ORDER BY uf";
						$query = mysql_query($sql);
						while($value=mysql_fetch_array($query)){
						
							echo '<option value="'.$value['uf'].'">'.$value['uf'].'</option>';
	    				} ?>
					</select>
				</li>
			
			<li class="zebra-dark">N&ordm; ID do fornecedor/subcontratado <font style="font-size:9px;">(Para pesquisar mais de um, separe por ";")</font></li>
			<li><textarea name="id" class="idFornecedor" style="width:500px; height:30px;"></textarea></li>
			
				<li class="zebra-dark">CNPJ <font style="font-size:9px;">(Para pesquisar mais de um, separe por ";")</font></li>
				<li><textarea name="cnpj" style="width:500px; height:30px;"></textarea></li>
			
			<li class="zebra-dark">Raz&#227;o Social</li>
			<li><input type="text" name="rs" class="" style="width:200px;"></li>
			
			<li class="zebra-dark">Data da auditoria</li>
			<li>
				<input type="text" name="dta1" id="dataAudi1" style="width: 80px" class="data" /> at&#233; 
				<input type="text" name="dta2" id="dataAudi2" style="width: 80px" class="data" />
			</li>
			
			<li class="zebra-dark">Status</li>
			<li>
				<select name="st">
					<option value="">Todos</option>
					<?php
						foreach($audi_resultado as $k=>$v){
							echo '<option value="'.str_replace("_", "", $k).'">'.$v.'</option>';
						}
					?>
				</select>
			</li>
			<?php if($_COOKIE['perm'] == 'Varejista'){ ?>
			<li class="zebra-dark">Status de Fornecimento</li>
			<li>
				<select name="st_fr">
					<option value="">Todos</option>
					<option value="2">Ativo</option>
					<option value="1">Pendente de Aprova&ccedil;&atilde;o</option>
					<option value="3">Aprovados para Auditoria</option>
				</select>
			</li>
			<?php } ?>
			
			<?php if($_COOKIE['perm'] != 'Certificadora'){ ?>
				<li class="zebra-dark">Certificadora</li>
				<li>
					<select name="ct">
						<option value="">Todos</option>
						<?php 
						foreach($arr_certificadoras as $k=>$v){
							echo '<option value="'.$k.'">'.$v.'</option>';
    					} ?>
					</select>
				</li>
			<?php } ?>
			
			
			<li class="zebra-dark">Ordenar por</li>
    		<li>
    			<select name="ordenar">
    				<option value="razaoSocial">Raz&atilde;o Social</option>
    				<option value="nomeFantasia">Nome Fantasia</option>
    				<option value="CNPJ">CNPJ</option>
    				<option value="fornecedor">ID do Fornecedor</option>
    			</select>
    		</li>
    		
    		<li class="zebra-dark">Exibir hist�rico</li>
    		<li>
    			<select name="hist">
    				<option value="sim" selected="selected">Sim</option>
    				<option value="nao">N�o</option>
    			</select>
    		</li>
			
			
			<li class="">Formato</li>
    		<li>
    			<select name="fm">
    				<option value="web">P&#225;gina Web</option>
    				<option value="exc">Excel</option>
    			</select>
    		</li>
			
			
			<li class="zebra-dark"><input type="submit" value="Criar Relat&#243;rio" /></li>
		</ul>
		
		
	</form>
<?php }else{
		
		unset($ORs);
		unset($ANDs);
		
		//Filtrar por Estado
		if($_GET['uf'] != ''){
			$ORs[] = " CC_Fornecedor.estado = '".tratarVariaveis($_GET['uf'])."' ";
		}

		//Filtrar por id Fornecedor
		if($_GET['id'] != ''){
			$td_ids = explode(';', $_GET['id']);
			foreach($td_ids as $id){
				$arr_id[] = "'".trim(str_replace(array('.','/', '-'),"", $id))."'";
			}
			$ANDs[] = " CC_Fornecedor.fornecedor IN (".implode(',',$arr_id).") ";
			
		}
		
		//CNPJ
		if($_GET['cnpj'] != ''){
			$td_cnpjs = explode(';', $_GET['cnpj']);
			foreach($td_cnpjs as $cnpj){
				$arr_cnpj[] = "'".trim(str_replace(array('.','/', '-'),"", $cnpj))."'";
			}
			$ORs[] = " CC_Fornecedor.CNPJ IN (".implode(',',$arr_cnpj).") ";
		}
		
		//RazaoSocial ou NomeFantasia
		if($_GET['rs'] != ''){
			$ORs[] = " ( ((CC_Fornecedor.razaoSocial LIKE '%".tratarVariaveis($_GET['rs'])."%') OR (CC_Fornecedor.nomeFantasia LIKE '%".tratarVariaveis($_GET['rs'])."%')) ) ";
		}
		
		//Tipo de Fornecedor
		if($_GET['tp'] != ''){
			if($_GET['tp'] == '1'){
				$ORs[] = " CC_Fr.classe = '".tratarVariaveis($_GET['tp'])."' ";
			}elseif($_GET['tp'] == '2'){
				//Muda query
				if( ($_COOKIE['perm'] == 'Certificadora') || ($_COOKIE['perm'] == 'Administradora')){
					$ANDs[] = " CC_Fr.classe = '".tratarVariaveis($_GET['tp'])."' ";
				}
			}
			
		}
		
		//Data Auditoria
		if($_GET['dta1'] != ''){
			$_GET['dta1'] =  convertDateSys($_GET['dta1']);
			if($_GET['dta2'] == ''){ $_GET['dta2'] = date('Y-m-d'); }else{ $_GET['dta2'] =  convertDateSys($_GET['dta2']); }
			
			$ORs[] = " ( CC_Fr.data_certificacao BETWEEN '".$_GET['dta1']."' AND '".$_GET['dta2']."' ) ";
		}
		
		
		if($_GET['dtv1'] != ''){
			if($_GET['dtv2'] != ''){
				
			}else{
				$_GET['dtv2'] = date('Y-m-d');
			}
		}
		
		//Status do Fornecedor
		if($_GET['st'] != ''){
			$ORs[] = " CC_Fr.status_fornecedor = '".tratarVariaveis($_GET['st'])."' ";
		}
		
		//Certificadora
		if($_GET['ct'] != ''){
			$ORs[] = " ( CC_Fr.certificadora = '".tratarVariaveis($_GET['ct'])."' AND CC_Fr.certificadoraTipo = 'Certificadora' ) ";
		}
		
		//Varejista
		if($_GET['vr'] != ''){
			$ORs[] = " ( CC_Associacao.associado = '".tratarVariaveis($_GET['vr'])."' AND CC_Associacao.tipo = 'Varejista' ) ";
		}
		
		//Status de Fornecimento
		if($_COOKIE['perm'] == 'Varejista'){
			if($_GET['st_fr'] != ''){
				$ANDs[] = " CC_Fr_Vinculo.status = '".$_GET['st_fr']."' ";
			}
		}
		
		
		if(($_COOKIE['perm'] != 'Administradora') && ($_COOKIE['perm'] != 'Certificadora')){
			$ANDs[] = " ( CC_Fr_Vinculo.empresa = '".$_COOKIE['empr']."' AND CC_Fr_Vinculo.ativo = '1' ) ";
		}
		
		if(($_COOKIE['perm'] != 'Administradora')){
			$JOIN = "
			 JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.ativo = '1'
			 JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."' AND CC_Associacao.associado = '".$_COOKIE['empr']."' ";
		}

	
	$ordenar = " ORDER BY CC_Fornecedor.".$_GET['ordenar']." ASC";

	?>
	
	
		<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="100%">
		<tr>
			<td colspan="25" class="mainTitle">Relat&#243;rio Auditor</td>
		</tr>
		<tr>
			<td colspan="25">
				<b>Filtros Aplicados</b>:<br/>
		<?php
			if($_GET['uf'] != ''){ echo '<b>Estado</b>: '.$_GET['uf'].'<br/>'; }
			if($_GET['id'] != ''){ echo '<b>N&ordm; ID</b>: '.$_GET['id'].'<br/>'; }
			if($_GET['cnpj'] != ''){ echo '<b>CNPJ</b>: '.$_GET['cnpj'].'<br/>'; }
			if($_GET['rs'] != ''){ echo '<b>Raz&#227;o Social</b>: '.$_GET['rs'].'<br/>'; }
			if($_GET['tp'] != ''){ echo '<b>Tipo</b>: '.$arr_tipo[$_GET['tp']].'<br/>';}
			if($_GET['dta1'] != ''){ echo '<b>Per&#237;odo de auditoria</b>: '.convertSysDate($_GET['dta1']).' at&#233; '.convertSysDate($_GET['dta2']).'<br/>'; }
			if($_GET['dtv1'] != ''){ echo '<b>Per&#237;odo de validade</b>: '.convertSysDate($_GET['dtv1']).' at&#233; '.convertSysDate($_GET['dtv2']).'<br/>'; }
			if($_GET['st_fr'] != ''){ echo '<b>Status Fornecedor</b>: '.$arr_fornecimento[$_GET['st_fr']].'<br/>'; }
			if($_GET['st'] != ''){ echo '<b>Status</b>: '.$audi_resultado['_'.$_GET['st']].'<br/>'; }
			if($_GET['ct'] != ''){ echo '<b>Certificadora</b>: '.$arr_certificadoras[$_GET['ct']].'<br/>'; }
			if($_GET['vr'] != ''){ echo '<b>Varejista</b>: '.$arr_varejistas[$_GET['vr']].'<br/>'; }
		?>
			</td>
		</tr>
		</table>
	
	<?php 
	//---------------------------------------------------------------------------------------
	//Fornecedores
		//Query para buscar Fornecedores
	 	$sql = "SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fornecedor 
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor "
				
				.$JOIN."
			WHERE 
				CC_Fr.empresa = '".$_COOKIE['empresa']."'
				".( ( is_array($ORs) )? " AND ( ".implode(' OR ',$ORs).") " : "" )."
				".( ( is_array($ANDs) )? "AND ".implode(' AND ',$ANDs)." " : "" )."				
				".$ordenar."
			";
		$query = mysql_query($sql) or die(mysql_error());
	
	?>
	
	
	
	<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="100%">
		<tr>
			<td colspan="25" align="center" style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Fornecedores</td>
		</tr>
		<tr>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 84px; text-align:center;">#</td>
			<!-- <td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 84px">Tipo</td> -->
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 90px;">N&ordm; ID</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 112px;">CNPJ</td>
			<?php if($_GET['fm'] == 'exc'){ ?><td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 90px;">ID Subcontratado</td><?php } ?>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width:310px;">Raz&atilde;o Social</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width:310px;">Nome Fantasia</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 170px;">Estado</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 170px;">Data Auditoria</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 170px;">Data Registro</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 170px;">Data Certificado</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 175px;">Certificado ABVTEX</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 175px;">Status Atual do Fornecedor</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 175px;">Subcontratados</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 98px;">Data de V&iacute;nculo / Exclus�o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 175px;">Status Atual do Fornecedor</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 175px;">Certificadora</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; width: 175px;">Auditor</td>
		</tr>
	
	<?php
	if(mysql_num_rows($query) > 0){
		for($i=0;mysql_num_rows($query) > $i;$i++){
		
		//Query para buscar subcontratados
		$sql2 = "SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fr_Vinculo 
				JOIN CC_Fr vc_sub ON CC_Fr_Vinculo.fornecedor = vc_sub.empresa
				JOIN CC_Fr ON vc_sub.fornecedor = CC_Fr.fornecedor
				JOIN CC_Fornecedor ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
				".(($_COOKIE['perm'] != 'Administradora')? " JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."' AND CC_Associacao.associado = '".$_COOKIE['empr']."'":"").
				" WHERE 
				CC_Fr.empresa = '".$_COOKIE['empresa']."'
				AND CC_Fr_Vinculo.ativo = '1'
				AND vc_sub.empresa = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."'
				".(($_COOKIE['perm'] == 'Administradora')? " AND CC_Fr_Vinculo.empresaTipo = 'Certificadora' ":"")."
			ORDER BY CC_Fornecedor.razaoSocial ASC";
		$query2 = mysql_query($sql2) or die(mysql_error());
	
		?>
		<!--  style="background: <?php if(($i%2) == 0) echo "#f1ecec"; else echo "#D8D8D8"; ?>;" -->
		<tr class="hover">
			<td align="center"><?php echo $i+1; ?></td>
			<!-- <td>Fornecedor</td> -->
			<td>
				<?php if($_GET['fm'] != 'exc'){ ?><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?>&ac=cadastro" target="_blank"><?php } ?>
					<?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?>
				<?php if($_GET['fm'] != 'exc'){ ?></a><?php } ?>
			</td>
			<td><?php echo mascaras(mysql_result($query,$i,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
			<?php if($_GET['fm'] == 'exc'){ ?><td>&nbsp;</td><?php } ?>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.estado');?></td>
			<?php
				//Verifica status da Cadeia
				$sql = "SELECT 
							v2.* 
						FROM CC_Fr 
							JOIN CC_Fr v2 ON v2.fornecedor = CC_Fr.fornecedor AND v2.empresa = '".$_COOKIE['empresa']."'
						WHERE CC_Fr.empresa = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."' AND CC_Fr.ativo = '1' ";
				$querySubs = mysql_query($sql) or die(mysql_error());
				unset($subs);
				unset($status_cadeia);
				
				
				if( (mysql_num_rows($querySubs) > 0) && (mysql_result($query,$i,'CC_Fr.classe') != '2') ){
					
					while($vSubs = mysql_fetch_array($querySubs)){
					/*	if( ($vSubs['status_certificacao'] == '2') && ( ($vSubs['tipo_auditoria'] == '1') || ($vSubs['tipo_auditoria'] == '3') || ($vSubs['tipo_auditoria'] == '5') ) ){
							$subs[] = '_1';
						}else{
							$subs[] = '_'.$vSubs['status_certificacao'];
						} */
						$subs[] = '_'.$vSubs['status_certificacao'];
					}
						// '' - nao iniciado, 0 - reprovado, 1 - aprovado, 2 - pendente
						
						if(count(array_keys($subs,'_0'))>0) $status_cadeia = 0;
						elseif(count(array_keys($subs,'_6'))>0) $status_cadeia = 6;
						elseif(count(array_keys($subs,'_5'))>0) $status_cadeia = 5;
						elseif(count(array_keys($subs,'_2'))>0) $status_cadeia = 2;
						elseif(count(array_keys($subs,'_3'))>0) $status_cadeia = 3;
						elseif(count(array_keys($subs,'_4'))>0)	$status_cadeia = 4; 
						elseif(count(array_keys($subs,'_'))>0) 	$status_cadeia = '';
						else 									$status_cadeia = 1;
						
						if(mysql_result($query,$i,'CC_Fr.classe') == '2') $status_cadeia = 'nao';
					
					
					
					unset($vSubs2);
					unset($subs2);
					//Agrupa valor do fornecedor
					$subs2[] = '_'.mysql_result($query,$i,'CC_Fr.status_fornecedor');
					
					mysql_data_seek($querySubs,0);
					while($vSubs2 = mysql_fetch_array($querySubs)){
						//Se for pendente de plano de a��o na manuten��o
						if( ($vSubs2['status_certificacao'] == '2') && ( ($vSubs2['tipo_auditoria'] == '1') || ($vSubs2['tipo_auditoria'] == '3') || ($vSubs2['tipo_auditoria'] == '5') || ($vSubs2['tipo_auditoria'] == '6') ) ){
							$subs2[] = '_1';
						}else{
							$subs2[] = '_'.$vSubs2['status_certificacao'];
						}
					}
					
					
					//Se Reprovado, Suspenso, Cancelado
					if((count(array_keys($subs2,'_0'))>0) || (count(array_keys($subs2,'_6'))>0) || (count(array_keys($subs2,'_5'))>0)) 		$certificado = 0;
					//Se Pendente, Auditoria Agendada, Aguardando Aprova��o
					elseif((count(array_keys($subs2,'_2'))>0) || (count(array_keys($subs2,'_3'))>0) || (count(array_keys($subs2,'_4'))>0)) 	$certificado = 0; 
					//Se N�o Iniciado
					elseif(count(array_keys($subs2,'_'))>0) 		$certificado = 0;
					else 										$certificado = 1;
					
					if(mysql_result($query,$i,'CC_Fr.classe') == '2') $certificado = 0;

				}else{
					$status_cadeia = 'nao';
					if(mysql_result($query,$i,'CC_Fr.status_fornecedor') == '1'){ $certificado = 1; }else{ $certificado = 0; }
					
				}




				
				
				$certificacao = mysql_result($query,$i,'CC_Fr.data_certificacao');
				$data_auditoria = mysql_result($query,$i,'CC_Fr.data_certificacao');
				$data_registro =  mysql_result($query,$i,'CC_Fr.criado');
							
				if($certificacao != ''){
					$val = explode('-', $certificacao);
					$validade = ($val[0]+2).'-'.$val[1].'-'.$val[2];
				}else{
					$validade = '';
				}				
				
				if($data_auditoria != ''){
					$val = explode('-', $data_auditoria);
					$data_auditoria = $val[0].'-'.$val[1].'-'.$val[2];
				}else{
					$data_auditoria = '';
				}		
				
				if($data_registro != ''){
					$val = explode('-', $data_registro);
					$data_registro = $val[0].'-'.$val[1].'-'.$val[2];
				}else{
					$data_registro = '';
				}
				
			?>
			
			
			
			<td align="center"><?php if($data_auditoria != '') echo convertSysDate($data_auditoria); else echo '--'; ?></td>
			<td align="center"><?php if($data_registro != '') echo convertSysDate($data_registro); else echo '--'; ?></td>
			<td align="center"><?php if($validade != '') echo convertSysDate($validade); else echo '--'; ?></td>
			
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php echo $audi_certificacao_cor['_'.$certificado]; ?>;color:white;font-weight:bold;">
				<?php if( ($_COOKIE['perm'] != 'Certificadora') && ($_GET['fm'] != 'exc')){ ?><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i,'CC_Fr.fornecedor'); ?>&ac=auditoria" style="color:white;"><?php } ?>
				<?php echo htmlentities($audi_certificacao['_'.$certificado], ENT_QUOTES, "ISO-8859-1"); ?>
				<?php if( ($_COOKIE['perm'] != 'Certificadora') && ($_GET['fm'] != 'exc') ){ ?></a><?php } ?>
			</td>
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php echo $audi_resultado_cor['_'.mysql_result($query,$i,'CC_Fr.status_certificacao')]; ?>;color:white;font-weight:bold;"><?php echo htmlentities($audi_resultado['_'.mysql_result($query,$i,'CC_Fr.status_certificacao')], ENT_QUOTES, "ISO-8859-1").( (mysql_result($query,$i,'CC_Fr.status_fornecedor') == '6')? '<br>'.convertSysDate(mysql_result($query,$i,'CC_Fr.data_status')) : '' ); ?></td>
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php if($status_cadeia === 'nao') echo $audi_resultado_cor['_1']; else echo $audi_resultado_cor['_'.$status_cadeia]; ?>;color:white;font-weight:bold;">
				<?php if($status_cadeia === 'nao') echo 'Sem Subcontratados'; else echo (($_GET['fm'] != 'exc')? '<a href="fornecedor_dados.php?fr='.mysql_result($query,$i,'CC_Fr.fornecedor').'&ac=subcontratados" style="color:#FFF;">': '').htmlentities($audi_resultado['_'.$status_cadeia], ENT_QUOTES, "ISO-8859-1").(($_GET['fm'] != 'exc')? '</a>': ''); ?>
			</td>
			<td align="center"><?php if($_COOKIE['perm'] != 'Administradora') echo convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.criacao')); else echo '--'; ?></td>
			<td align="center"><?php if($_COOKIE['perm'] != 'Administradora') echo $arr_fornecimento[mysql_result($query,$i,'CC_Fr_Vinculo.status')]; else echo '--'; ?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>			
		</tr>
			
		<?php
				// mostrando hist�rico de exclus�o... se houver
				// $sql3 = "SELECT * FROM CC_Fr_Exclusao WHERE CC_Fr_Exclusao.fornecedor = '".mysql_result($query2,$i2,'CC_Fr.fornecedor')."' AND CC_Fr_Exclusao.empresa = '".mysql_result($query,$i,'CC_Fr.fornecedor')."'";
				$sql3 = "SELECT * FROM CC_Fr_Exclusao WHERE CC_Fr_Exclusao.fornecedor = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."'";
				$query3 = mysql_query($sql3) or die(mysql_error());
				if(mysql_num_rows($query3) > 0) { 
		?>
		<tr class="<?php echo mysql_result($query2,$i2,'CC_Fr.fornecedor'); ?>-<?php echo mysql_result($query,$i,'CC_Fr.fornecedor'); ?>" style="background: #FFF;">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<!-- <?php if($_GET['fm'] != 'exc'){ ?><td>&nbsp;</td><?php }else{ ?><td>Hist&oacute;rico</td><?php } ?>
			<td style="><?php echo mysql_result($query3,0,'fornecedor');?></td> -->
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<!-- td>&nbsp;</td-->
			<td>&nbsp;</td>
			<td style="text-align: center;"><?php echo convertSysDate(mysql_result($query3,0,'data'));?></td>
			<td>&nbsp;</td>
			<td style="border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_resultado_cor['_3'];?>">Exclu�do</td>
			<td style="border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_resultado_cor['_3'];?>">Exclu�do</td>
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php if($status_cadeia === 'nao') echo $audi_resultado_cor['_1']; else echo $audi_resultado_cor['_'.$status_cadeia]; ?>;color:white;font-weight:bold;">
				<?php if($status_cadeia === 'nao') echo 'Sem Subcontratados'; else echo (($_GET['fm'] != 'exc')? '<a href="fornecedor_dados.php?fr='.mysql_result($query,$i,'CC_Fr.fornecedor').'&ac=subcontratados" style="color:#FFF;">': '').htmlentities($audi_resultado['_'.$status_cadeia], ENT_QUOTES, "ISO-8859-1").(($_GET['fm'] != 'exc')? '</a>': ''); ?>
			</td>
			<td style="text-align: center;"><?php echo htmlentities(mysql_result($query3,0,'motivo'), ENT_QUOTES, "ISO-8859-1");?></td>
		</tr>
		
		<?php } ?>
			
		<?php
			if($_GET['hist'] == 'sim'){
			// Hist�rico de auditorias
			$sqlAudis = "SELECT * FROM Audi_Fornecedor WHERE fornecedor = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."' AND empresa = '".$_COOKIE['empresa']."' AND status = '1' ORDER BY data DESC, criado DESC";
			$queryAudis = mysql_query($sqlAudis) or die(mysql_error());
			
			if(mysql_num_rows($queryAudis) > 0){
			while($valuesAudis = mysql_fetch_array($queryAudis)){
				
				
				//Se Reprovado, Suspenso, Cancelado
					if( ($valuesAudis['resultado'] == 0) || ($valuesAudis['resultado'] == 6) || ($valuesAudis['resultado'] == 5)) 		$certificado = 0;
					//Se Pendente, Auditoria Agendada, Aguardando Aprova��o
					elseif(($valuesAudis['resultado'] == 2) || ($valuesAudis['resultado'] == 3) || ($valuesAudis['resultado'] == 4) )	$certificado = 0; 
					//Se N�o Iniciado
					elseif($valuesAudis['resultado'] == '') 		$certificado = 0;
					else 										$certificado = 1;
				 
		?>
			<tr class="<?php echo $valuesAudis['fornecedor']; ?>" style="background: #FFF;" exp="0">
				<td>&nbsp;</td>
				
				<!-- <?php if($_GET['fm'] != 'exc'){ ?><td>&nbsp;</td><?php }else{ ?><td>Hist&oacute;rico</td><?php } ?><td style=""><a href="auditoria.php?ac=resumo&frid=<?php echo $valuesAudis['fornecedor'];?>&audi=<?php echo $valuesAudis['audi'];?>" target="_blank"><?php echo $valuesAudis['fornecedor'];?></a></td> -->
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<?php if($_GET['fm'] == 'exc'){ ?><td>&nbsp;</td><?php } ?>
				<td style="text-align: center;"><?php echo convertSysDate($valuesAudis['data']);?></td>
				<td style="text-align: center;"><?php echo convertSysDate($valuesAudis['criado']);?></td>
				<td style="text-align: center;"></td>
				
				
				
				
				<?php
				//Verifica status da Cadeia
				$sql = "SELECT 
							Audi_Fornecedor.* 
						FROM CC_Fr 
							JOIN CC_Fr v2 ON v2.fornecedor = CC_Fr.fornecedor AND v2.empresa = '".$_COOKIE['empresa']."'
							
							JOIN Audi_Fornecedor ON Audi_Fornecedor.fornecedor = v2.fornecedor AND Audi_Fornecedor.empresa = '".$_COOKIE['empresa']."'
							  
							
						WHERE 
							CC_Fr.empresa = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."' 
							AND CC_Fr.ativo = '1'
							AND Audi_Fornecedor.data BETWEEN v2.criado AND '".$valuesAudis['data']."'
						";
				$querySubs = mysql_query($sql) or die(mysql_error());
				unset($subs);
				unset($status_cadeia);
				
				$status_cadeia = '';
				if( (mysql_num_rows($querySubs) > 0) && (mysql_result($query,$i,'CC_Fr.classe') != '2') ){
					
					while($vSubs = mysql_fetch_array($querySubs)){
						$subs[] = '_'.$vSubs['resultado'];
					}
						// '' - nao iniciado, 0 - reprovado, 1 - aprovado, 2 - pendente
						
						if(count(array_keys($subs,'_0'))>0) $status_cadeia = 0;
						elseif(count(array_keys($subs,'_6'))>0) $status_cadeia = 6;
						elseif(count(array_keys($subs,'_5'))>0) $status_cadeia = 5;
						elseif(count(array_keys($subs,'_2'))>0) $status_cadeia = 2;
						elseif(count(array_keys($subs,'_3'))>0) $status_cadeia = 3;
						elseif(count(array_keys($subs,'_4'))>0)	$status_cadeia = 4; 
						elseif(count(array_keys($subs,'_'))>0) 	$status_cadeia = '';
						else 									$status_cadeia = 1;
						
						if(mysql_result($query,$i,'CC_Fr.classe') == '2') $status_cadeia = 'nao';
					
					
					
					unset($vSubs2);
					unset($subs2);
					//Agrupa valor do fornecedor
					$subs2[] = '_'.$valuesAudis['resultado'];
					
					mysql_data_seek($querySubs,0);
					while($vSubs2 = mysql_fetch_array($querySubs)){
						//Se for pendente de plano de a��o na manuten��o
						if( ($vSubs2['resultado'] == '2') && ( ($vSubs2['tipo'] == '1') || ($vSubs2['tipo'] == '3') || ($vSubs2['tipo'] == '5') || ($vSubs2['tipo'] == '6') ) ){
							$subs2[] = '_1';
						}else{
							$subs2[] = '_'.$vSubs2['resultado'];
						}
					}
					
					
					//Se Reprovado, Suspenso, Cancelado
					if((count(array_keys($subs2,'_0'))>0) || (count(array_keys($subs2,'_6'))>0) || (count(array_keys($subs2,'_5'))>0)) 		$certificado = 0;
					//Se Pendente, Auditoria Agendada, Aguardando Aprova��o
					elseif((count(array_keys($subs2,'_2'))>0) || (count(array_keys($subs2,'_3'))>0) || (count(array_keys($subs2,'_4'))>0)) 	$certificado = 0; 
					//Se N�o Iniciado
					elseif(count(array_keys($subs2,'_'))>0) 		$certificado = 0;
					else 										$certificado = 1;
					
					if(mysql_result($query,$i,'CC_Fr.classe') == '2') $certificado = 0;

				}else{
					$status_cadeia = 'nao';
					if($valuesAudis['resultado'] == '1'){ $certificado = 1; }else{ $certificado = 0; }
					
				}
	
				
			?>
				
				
				
				
				
				<td style=" border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_certificacao_cor['_'.$certificado];?>"><?php echo htmlentities($audi_certificacao['_'.$certificado], ENT_QUOTES, "ISO-8859-1");?></td>
				<td style=" border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_resultado_cor['_'.$valuesAudis['resultado']];?>"><?php echo htmlentities($audi_resultado['_'.$valuesAudis['resultado']], ENT_QUOTES, "ISO-8859-1");?></td>
				<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php if($status_cadeia === 'nao') echo $audi_resultado_cor['_1']; else echo $audi_resultado_cor['_'.$status_cadeia]; ?>;color:white;font-weight:bold;">
					<?php if($status_cadeia === 'nao') echo 'Sem Subcontratados'; else echo (($_GET['fm'] != 'exc')? '<a href="fornecedor_dados.php?fr='.mysql_result($query,$i,'CC_Fr.fornecedor').'&ac=subcontratados" style="color:#FFF;">': '').htmlentities($audi_resultado['_'.$status_cadeia], ENT_QUOTES, "ISO-8859-1").(($_GET['fm'] != 'exc')? '</a>': ''); ?>
				</td>
				<td style=" text-align: center;"><?php echo $valuesAudis['motivo'];?></td>
				<td style=" text-align: center;"></td>
				<td align="center"><?php echo strtoupper( $valuesAudis['certificadora'] );?></td>
				<td align="center"><?php echo strtoupper( $valuesAudis['auditor'] );?></td>
			</tr>
			
		<?php	
			}
			}else{
				echo '<tr class="'.mysql_result($query,$i,'CC_Fr.fornecedor').'" style="background: #FFF;" exp="0">
						<td colspan="13" style="background: #F2E3C9; padding: 0 3px; text-align: center;" >Sem Hist&oacute;rico</td>
					</tr>';
				
			}
			}
		?>
		
		
	
			

				<?php
				/* listagem do hist�rico dos subcontratados - begin */
				if(mysql_num_rows($query2) > 0){
					for($i2=0;mysql_num_rows($query2) > $i2;$i2++){
					
					if($_GET['hist'] == 'sim'){
							
				//Query para buscar subcontratados
				$sql3 = "SELECT * FROM CC_Fr_Exclusao WHERE CC_Fr_Exclusao.fornecedor = '".mysql_result($query2,$i2,'CC_Fr.fornecedor')."' AND CC_Fr_Exclusao.empresa = '".mysql_result($query,$i,'CC_Fr.fornecedor')."'";
				$query3 = mysql_query($sql3) or die(mysql_error());
				if(mysql_num_rows($query3) > 0){ 
				
				
					?>
					<tr class="zebra-dark hover">
						<!-- td>&nbsp;</td -->
						<td>Subcontratado</td>
						<td>
							<?php if($_GET['fm'] != 'exc'){ ?><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query2,$i2,'CC_Fornecedor.fornecedor');?>&ac=cadastro<?=(($_COOKIE['perm'] != 'Administradora')?"&tipo=2":"")?>" target="_blank"><?php } ?>
								<?php if($_GET['fm'] != 'exc'){ echo mysql_result($query2,$i2,'CC_Fornecedor.fornecedor'); }else{ echo mysql_result($query,$i,'CC_Fr.fornecedor'); } ?> 
							<?php if($_GET['fm'] != 'exc'){ ?></a><?php } ?>
						</td>
						<td><?php echo mascaras(mysql_result($query2,$i2,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
						<?php if($_GET['fm'] == 'exc'){ ?><td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.fornecedor');?></td><?php } ?>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1");?></td>
						
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.estado');?></td>
						
						<?php
							$certificacao = mysql_result($query2,$i2,'CC_Fr.data_certificacao');
							
							if(mysql_result($query2,$i2,'CC_Fr.status_fornecedor') == '1'){ $certificado = 1; }else{ $certificado = 0; }


						if($certificacao != ''){
							$val = explode('-', $certificacao);
							$validade = ($val[0]+2).'-'.$val[1].'-'.$val[2];
						}else{
							$validade = '';
						}									
							
						$data_registro =  mysql_result($query2,$i2,'CC_Fr.criado');	
						
						if($data_registro != ''){
							$val = explode('-', $data_registro);
							$data_registro = $val[0].'-'.$val[1].'-'.$val[2];
						}else{
							$data_registro = '';
						}		
							
						?>
						
						<td align="center"><?php if($certificacao != '') echo convertSysDate($certificacao); else echo '--'; ?></td>
						<td align="center"><?php if($data_registro != '') echo convertSysDate($data_registro); else echo '--'; ?></td>
						<td align="center"><?php if($validade != '') echo convertSysDate($validade); else echo '--'; ?></td>
						
						<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php echo $audi_certificacao_cor['_'.$certificado]; ?>;color:white;font-weight:bold;">
							<?php if( ($_COOKIE['perm'] != 'Certificadora') && ($_GET['fm'] != 'exc') ){ ?><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query2,$i2,'CC_Fr.fornecedor'); ?>&ac=auditoria" style="color:white;"><?php } ?>
							<?php echo $audi_certificacao['_'.$certificado]; ?>
							<?php if( ($_COOKIE['perm'] != 'Certificadora') && ($_GET['fm'] != 'exc') ){ ?></a><?php } ?>
						</td>
						
						<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php echo $audi_resultado_cor['_'.mysql_result($query2,$i2,'CC_Fr.status_certificacao')]; ?>;color:white;font-weight:bold;"><?php echo $audi_resultado['_'.mysql_result($query2,$i2,'CC_Fr.status_certificacao')].( (mysql_result($query2,$i2,'CC_Fr.status_fornecedor') == '6')? '<br>'.convertSysDate(mysql_result($query2,$i2,'CC_Fr.data_status')) : '' ); ?></td>
						<td align="center">--</td>
						<td align="center"><?php if($certificacao != '') echo convertSysDate(mysql_result($query2,$i2,'vc_sub.criado')); else echo '--'; ?></td>
						<td align="center">Subcontratado</td>
						<td align="center">Certificadora</td>
						<td align="center">Auditor</td>						
					</tr>
					
					<?php
						?>
						<tr class="<?php echo mysql_result($query2,$i2,'CC_Fr.fornecedor'); ?>-<?php echo mysql_result($query,$i,'CC_Fr.fornecedor'); ?>" style="background: #FFF;">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<!-- <?php if($_GET['fm'] != 'exc'){ ?><td>&nbsp;</td><?php }else{ ?><td>Hist&oacute;rico</td><?php } ?>
							<td style="><?php echo mysql_result($query3,0,'fornecedor');?></td> -->
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<!-- td>&nbsp;</td-->
							<td>&nbsp;</td>
							<td style="text-align: center;"><?php echo convertSysDate(mysql_result($query3,0,'data'));?></td>
							<td>&nbsp;</td>
							<td style="border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_resultado_cor['_3'];?>">Exclu�do</td>
							<td style="border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_resultado_cor['_3'];?>">Exclu�do</td>
							<td align="center">--</td>
							<td style="text-align: center;"><?php echo htmlentities(mysql_result($query3,0,'motivo'), ENT_QUOTES, "ISO-8859-1");?></td>
						</tr>
			<?php	//}
							
							
							
						// Hist�rico de auditorias
						$sqlAudis = "SELECT * FROM Audi_Fornecedor WHERE fornecedor = '".mysql_result($query2,$i2,'CC_Fr.fornecedor')."' AND empresa = '".$_COOKIE['empresa']."' AND status = '1' ORDER BY data DESC, criado DESC";
						
						$queryAudis = mysql_query($sqlAudis) or die(mysql_error());
						if(mysql_num_rows($queryAudis) > 0){
						while($valuesAudis = mysql_fetch_array($queryAudis)){
							
							
							//Se Reprovado, Suspenso, Cancelado
					if( ($valuesAudis['resultado'] == 0) || ($valuesAudis['resultado'] == 6) || ($valuesAudis['resultado'] == 5)) 		$certificado = 0;
					//Se Pendente, Auditoria Agendada, Aguardando Aprova��o
					elseif(($valuesAudis['resultado'] == 2) || ($valuesAudis['resultado'] == 3) || ($valuesAudis['resultado'] == 4) )	$certificado = 0; 
					//Se N�o Iniciado
					elseif($valuesAudis['resultado'] == '') 		$certificado = 0;
					else 										$certificado = 1;
							 
					?>
						<tr class="<?php echo $valuesAudis['fornecedor']; ?>" style="background: #FFF;" exp="0">
				<td>&nbsp;</td>
				<?php if($_GET['fm'] != 'exc'){ ?><td>&nbsp;</td><?php }else{ ?><td>Hist&oacute;rico</td><?php } ?>
				<!-- <td style=""><a href="auditoria.php?ac=resumo&frid=<?php echo $valuesAudis['fornecedor'];?>&audi=<?php echo $valuesAudis['audi'];?>" target="_blank"><?php echo $valuesAudis['fornecedor'];?></a></td> -->
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<!-- td>&nbsp;</td-->
				<td style="text-align: center;"><?php echo convertSysDate($valuesAudis['data']);?></td>
				<td style="text-align: center;"><?php echo convertSysDate($valuesAudis['criado']);?></td>
				<td style="text-align: center;"></td>
				<td style="border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_certificacao_cor['_'.$certificado];?>"><?php echo htmlentities($audi_resultado['_'.$certificado], ENT_QUOTES, "ISO-8859-1");?></td>
				<td style="border-collapse: collapse; border: #FFF; color:white;font-weight:bold; text-align: center; background: <?php echo $audi_resultado_cor['_'.$valuesAudis['resultado']];?>"><?php echo htmlentities($audi_resultado['_'.$valuesAudis['resultado']], ENT_QUOTES, "ISO-8859-1");?></td>
				<td align="center">--</td>
				<td style=" text-align: center;"><?php echo $valuesAudis['motivo'];?></td>
				<td>&nbsp;</td>
				<td align="center"><?php echo strtoupper( $valuesAudis['certificadora'] );?></td>
				<td align="center"><?php echo strtoupper( $valuesAudis['auditor'] );?></td>				
			</tr>
						
					<?php	
						}}
						}else{
							// echo '<tr class="'.mysql_result($query2,$i2,'CC_Fr.fornecedor').'-'.mysql_result($query,$i,'CC_Fr.fornecedor').'" style="background: #FFF;" exp="0">
									// <td colspan="13" style="background: #F2E3C9; padding: 0 3px; text-align: center;" >Sem Hist&oacute;rico</td>
								// </tr>';
							
						}
						}
					?>

				<?php
					}
				}
				/* listagem do hist�rico dos subcontratados - end */
				?>

		
	<?php
	
	
		}
	}
	?>
	</table>
	
<?php 

		
		
		
		//--------	
	?>
	
	
	
	
	
	
	
	
	
	
<?php } ?>