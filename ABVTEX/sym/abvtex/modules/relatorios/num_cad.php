	<?php
		
		if($_COOKIE['perm'] != 'Administradora'){
			$filtro = " JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.empresa = '".$_COOKIE['empr']."' AND CC_Fr_Vinculo.ativo = '1'";
		}else{
			$filtro = "";
		}
		
		echo $sql = "
				SELECT 
					".(($_COOKIE['perm'] != 'Administradora')?
						" ((SELECT count(*) FROM CC_Fr WHERE CC_Fr.classe = '2' AND CC_Fr.empresa IN (SELECT CC_Fr.fornecedor FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.classe = '1')) + (SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."')) AS totalEmpresas, ":
						" (SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."') AS totalEmpresas, ")."
					".(($_COOKIE['perm'] != 'Administradora')?
						" (SELECT count(*) FROM CC_Fr WHERE CC_Fr.classe = '2' AND CC_Fr.empresa IN (SELECT CC_Fr.fornecedor FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.classe = '1')) AS subcontratados,   ":
						" (SELECT count(*) FROM CC_Fr WHERE CC_Fr.classe = '2' AND CC_Fr.empresa = '".$_COOKIE['empresa']."') AS subcontratados,")."
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."') AS fornecedores,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao IS NULL) AS nao_planejado,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '2') AS pendentes,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '1') AS aprovados,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '0' AND status_certificacao IS NOT NULL) AS reprovadas,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '6' AND status_certificacao IS NOT NULL) AS suspensas,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '5') AS cancelado,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '4') AS aguardando,
					(SELECT count(*) FROM CC_Fr ".$filtro." WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.status_certificacao = '3') AS agendada
				";
				exit();
		$query = mysql_query($sql) or die(mysql_error());
	
	?>
	
	
	<a style="float:right;" href="javascript:history.back(-1)"><img alt="Voltar" src="images/voltar.png"></a>
	
	<br clear="both" /><br/>
	
	<!-- JS -->		
		<!-- 1. Add these JavaScript inclusions in the head of your page -->
			<script type="text/javascript" src="js/highcharts/highcharts.js"></script>

		<!-- 1b) Optional: the exporting module -->
			<script type="text/javascript" src="js/highcharts/modules/exporting.js"></script>
            
            <script type="text/javascript">
			
				w = screen.availWidth;
				
				h = screen.availHeight;
				
				$(document).ready(function() {
					$('#container').css('min-height', h);
				});
			</script>
			
			
			
			<script type="text/javascript">
		
			var chart;
			$(document).ready(function() {
				
				var colors = Highcharts.getOptions().colors,
					categories = ['Total de Empresas', 
								  'Fornecedores', 
								  'Subcontratados', 
								  'Aprovados', 
								  'Pendentes',
								  'Reprovados',
								  'Suspensas',
								  'Cancelamento',
								  'Aguardando',
								  'Agendada'],
					name = 'Fornecedores',
					data = [{
							y: <?php echo mysql_result($query,0,'totalEmpresas'); ?>,
							color: colors[0]
						}, {
						 	y: <?php echo mysql_result($query,0,'fornecedores') ?>,
							color: colors[1]
						}, {
							y: <?php echo mysql_result($query,0,'subcontratados'); ?>,
							color: colors[2]
						}, {
							y: <?php echo mysql_result($query,0,'aprovados'); ?>,
							color: colors[3]
						}, {
							y: <?php echo mysql_result($query,0,'pendentes'); ?>,
							color: colors[4]
						}, {
							y: <?php echo mysql_result($query,0,'reprovadas'); ?>,
							color: colors[5]
						}, {
							y: <?php echo mysql_result($query,0,'suspensas'); ?>,
							color: colors[6]
						}, {
							y: <?php echo mysql_result($query,0,'cancelado'); ?>,
							color: colors[7]
						}, {
							y: <?php echo mysql_result($query,0,'aguardando'); ?>,
							color: colors[8]
						}, {
							y: <?php echo mysql_result($query,0,'agendada'); ?>,
							color: colors[9]
						}];
				
				function setChart(name, categories, data, color) {
					chart.xAxis[0].setCategories(categories);
					chart.series[0].remove();
					chart.addSeries({
						name: name,
						data: data,
						color: color || 'white'
					});
				}
				
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'grafico', 
						type: 'column'
					},
					legend: {
				        enabled: false
				    },
					title: {
						text: 'Dados de Cadastro'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: categories,
						labels: {
							style: {
								color: '#000000'
							}
						}					
					},
					yAxis: {
						max: <?php echo mysql_result($query,0,'totalEmpresas'); ?>,
						title: {
							text: 'N�meros do Cadastro'
						}
					},
					plotOptions: {
						column: {
							//cursor: 'pointer',
							point: {
								events: {
									/*click: function() {
										var drilldown = this.drilldown;
										if (drilldown) { // drill down
											setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
										} else { // restore
											setChart(name, categories, data);
										}
									}*/
								}
							},
							dataLabels: {
								enabled: true,
								color: colors[0],
								style: {
									fontWeight: 'bold'
								},
								formatter: function() {
									//return this.y +'%';
									return this.y;
								}
							}					
						}
					},
					tooltip: {
						formatter: function() {
							var point = this.point,
								s = this.x +':<b>'+ this.y +' Fornecedores</b>';
							/*if (point.drilldown) {
								s += 'Clique para ver '+ point.category +' vers�es';
							} else {
								s += 'Clique para retornar para as marcas';
							}*/
							return s;
						}
					},
					series: [{
						name: name,
						data: data,
						color: '#000000'
					}],
					exporting: {
						enabled: false
					}
				});
				
				
			});
				
		</script>
		
		<div id="grafico" style="width: 800px; height: 400px; margin: 0 auto"></div>

	
	
	
	
	<table cellpadding="2" cellspacing="2" width="561" align="center" style="margin: 10px auto 50px auto;">
		<tr class="zebra-dark">
			<td><a href="relatorios.php?ac=fornecedores&qr=totalEmpresas" target="_blank">Total de Empresas</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'totalEmpresas');?></td>
		</tr>
		<tr>
			<td><a href="relatorios.php?ac=fornecedores&qr=fornecedores" target="_blank">Total de Fornecedores</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'fornecedores');?></td>
		</tr>
		<tr class="zebra-dark">
			<td><a href="relatorios.php?ac=fornecedores&qr=subcontratados" target="_blank">Total de Subcontratados</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'subcontratados');?></td>
		</tr>
		<tr>
			<td><a href="relatorios.php?ac=fornecedores&qr=aprovados" target="_blank">Empresas Aprovadas</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'aprovados');?></td>
		</tr>
		<tr class="zebra-dark">
			<td><a href="relatorios.php?ac=fornecedores&qr=pendentes" target="_blank">Empresas Pendente de Plano de A��o</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'pendentes');?></td>
		</tr>
		<tr>
			<td><a href="relatorios.php?ac=fornecedores&qr=reprovadas" target="_blank">Empresas Reprovadas</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'reprovadas');?></td>
		</tr>
		<tr>
			<td><a href="relatorios.php?ac=fornecedores&qr=suspensas" target="_blank">Empresas Suspensas</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'suspensas');?></td>
		</tr>
		<tr class="zebra-dark">
			<td><a href="relatorios.php?ac=fornecedores&qr=nao_planejado" target="_blank">Empresas que n�o planejaram auditorias</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'nao_planejado');?></td>
		</tr>
		<tr class="">
			<td><a href="relatorios.php?ac=fornecedores&qr=nao_planejado" target="_blank">Empresas que n�o planejaram auditorias</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'nao_planejado');?></td>
		</tr>
		<tr class="zebra-dark">
			<td><a href="relatorios.php?ac=fornecedores&qr=nao_planejado" target="_blank">Empresas Canceladas</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'cancelado');?></td>
		</tr>
		<tr class="">
			<td><a href="relatorios.php?ac=fornecedores&qr=nao_planejado" target="_blank">Empresas Aguardando Auditoria</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'aguardando');?></td>
		</tr>
		<tr class="zebra-dark">
			<td><a href="relatorios.php?ac=fornecedores&qr=nao_planejado" target="_blank">Empresas com Auditoria Agendada</a></td>
			<td style="width:20px; text-align: right;"><?php echo mysql_result($query,0,'agendada');?></td>
		</tr>
	</table>