	
	<?php
		$filtro = '';
		if($_POST['ac'] == 'filtrar'){
			
			if($_POST['filtro'] != ''){
				$filtro .= " AND CC_Empresa.empresa = '".$_POST['filtro']."' ";
			}
			
			if($_POST['data1'] != ''){
				$filtro .= " AND CC_Log.time BETWEEN '".convertDateSys($_POST['data1'])."' AND '".convertDateSys($_POST['data2'])."'";
			}
			
		}
		
		if($_COOKIE['perm'] == 'Certificadora'){ 
			$filtro .= " AND CC_Empresa.empresa = '".$_COOKIE['empr']."' ";
		}
		
		
	?>
	
	<script type="text/javascript">
		$(function(){
			$('#data1').change(function(){
				if($('#data1').val() != ''){
					$('#data2').addClass('required');
				}else{
					$('#data2').removeClass('required');
				}
			});
			
		});
	</script>
	
	<a style="float:right;" href="relatorios.php"><img alt="Voltar" src="images/voltar.png"></a>
	<br clear="both" />
	
	<div class="title">Auditorias Realizadas</div>
	
	<?php if($_COOKIE['perm'] == 'Administradora'){ ?>
		<form action="" method="post">
			<input type="hidden" name="ac" value="filtrar" />
			<ul>
				<li>Certificadora</li>
				<li>
					<select name="filtro">
						<option value="">Todos</option>
						<?php 
							$sql = "SELECT * FROM CC_Empresa 
										JOIN CC_Associacao ON CC_Associacao.associado = CC_Empresa.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
										JOIN CC_Empresa_Dados ON CC_Empresa_Dados.empresa = CC_Empresa.empresa
									WHERE CC_Associacao.tipo = 'Certificadora'"; 
							$query = mysql_query($sql) or die(mysql_error());
							if(mysql_num_rows($query) > 0){
								while($values = mysql_fetch_array($query)){ ?>
									<option value="<?php echo $values['empresa']; ?>" <?php if($_POST['filtro'] == $values['empresa']) echo 'selected="selected"'; ?>><?php echo $values['nomeFantasia']; ?></option>
							<?php } } ?>
					</select>
					
					<input type="text" name="data1" id="data1" alt="Data Inicial" style="width: 80px" class="data" /> at� <input type="text" name="data2" id="data2" alt="Data Final" style="width: 80px" class="data" />
					
					<input type="submit" value="Filtrar" />
				</li>
			</ul>
		</form>
	<?php } ?>
	
	
	
	<?php
		
	$sql = "
			SELECT * FROM CC_Log 
				JOIN CC_Log_Indice ON CC_Log_Indice.indice = CC_Log.indice AND CC_Log_Indice.Administradora = '1'
				JOIN CC_Empresa ON CC_Empresa.empresa = CC_Log.empresa
				JOIN CC_Associacao ON CC_Associacao.associado = CC_Empresa.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
				JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.empresa = CC_Empresa.empresa AND CC_Log.fornecedor = CC_Fr_Vinculo.fornecedor AND CC_Fr_Vinculo.ativo = '1'
				
				JOIN Audi_Fornecedor ON Audi_Fornecedor.fornecedor = CC_Fr_Vinculo.fornecedor AND Audi_Fornecedor.empresa = CC_Associacao.empresa AND Audi_Fornecedor.status = '1'
				
				JOIN CC_Fornecedor ON CC_Fornecedor.fornecedor = CC_Fr_Vinculo.fornecedor 
			WHERE CC_Associacao.tipo = 'Certificadora'
				".$filtro."
			ORDER BY CC_Log.time DESC
		";
		$query = mysql_query($sql) or die(mysql_error());
	?>
	
	<table width="100%" cellspacing="0" cellpadding="4" expandir="0" id="table_processo" style="font-size: 12px; margin-bottom:10px;">	    
		<tr class="tabela_titulos">
			<td>#</td>
    		<td>Certificadora</td>
    		<td>Fornecedor</td>
    		<td align="center">Status</td>
    		<td width="146px">Data da Certifica&ccedil;&atilde;o</td>
	    </tr>
	    
	    <?php
	    	if(mysql_num_rows($query) > 0){
	    		for($i=0;mysql_num_rows($query)>$i;$i++){
	    ?>
		    <tr class="<?php if($i%2!=0) echo 'zebra-dark'; ?>">
		    	<td><?php echo ($i+1); ?></td>
	    	    <td><?php echo mysql_result($query, $i, 'CC_Empresa.razaoSocial'); ?></td>
	    	    <td><?php echo mysql_result($query, $i, 'CC_Fornecedor.razaoSocial'); ?></td>
	    	    <td style="color:#FFF; font-weight: bold; width:182px; text-align:center; background:<?php echo $audi_resultado_cor['_'.mysql_result($query, $i, 'Audi_Fornecedor.resultado')]; ?>;"><?php echo $audi_resultado['_'.mysql_result($query, $i, 'Audi_Fornecedor.resultado')]; ?></td>
	    	    <td align="center"><?php echo convertSysDate(mysql_result($query, $i, 'Audi_Fornecedor.data')); ?></td>
		    </tr>
	    <?php } 
	    	}else{
	    		echo '<tr class="zebra-dark">
		    		    <td colspan="4" align="center" height="30">
		    		    	Nenhum Registro encontrado.
		    		    </td>
			    	</tr>'; 
	    	}
	    ?>
	</table>
	