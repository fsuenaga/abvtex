<?php
	$sql = "
	SELECT
			CC_Fornecedor.estado AS Estado,
			COUNT(
				CASE
				WHEN CC_Fr.status_certificacao = 1
				AND CC_Fr.status_fornecedor = 1 THEN
					1
				END
			) AS 'Aprovados',
			COUNT(
				CASE
				WHEN CC_Fr.status_certificacao = 2
				AND CC_Fr.status_fornecedor = 1 THEN
					1
				END
			) AS 'Pendentes de plano de a��o',
			COUNT(
				CASE
				WHEN CC_Fr.status_certificacao = 5 THEN
					1
				END
			) AS 'Cancelados',
			COUNT(
				CASE
				WHEN CC_Fr.status_certificacao = 6 THEN
					1
				END
			) AS 'Suspensos',
			COUNT(
				CASE
				WHEN CC_Fr.status_certificacao = 0 THEN
					1
				END
			) AS 'Reprovados',
			COUNT(
				CASE
				WHEN CC_Fr.status_certificacao <> '' THEN
					1
				END
			) AS 'Total'
		FROM
			CC_Fr
		INNER JOIN CC_Fornecedor ON CC_Fornecedor.fornecedor = CC_Fr.fornecedor
		WHERE
			CC_Fr.empresa = 'abvtex'
		GROUP BY
			CC_Fornecedor.estado
		ORDER BY
			CC_Fornecedor.estado ASC;
	" ;
	
	$query = mysql_query( $sql ) or die( mysql_error() ) ;
	
	if( mysql_num_rows( $query ) > 0 ) {
?>
	<font style="font-size:15px; font-weight: bold;"><?=htmlentities("Quantidade de empresas por estado", ENT_QUOTES, "ISO-8859-1")?></font>
	<table width="100%" align="center" cellspacing="0" cellpadding="4" style="margin: 10px auto 50px auto;">
		
		<tr class="" align="center" valign="middle" style="background:#F7E0BB; ">
		
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Estado</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Aprovados</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Pendentes de plano de a��o</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Suspensos</b></td>
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Cancelados</b></td>
			<!-- <td style="font-size:15px; padding: 10px; width: 250px;"><b>Reprovados</b></td> -->
			<td style="font-size:15px; padding: 10px; width: 250px;"><b>Total</b></td>

		</tr>
		
		<?php
			$i=0; 
			$somas = array();
			$somas['Aprovados'] = 0;
			$somas['Pendentes de plano de a��o'] = 0;
			$somas['Suspensos'] = 0;
			$somas['Cancelados'] = 0;
			$values['Reprovados'] = 0;
			$values['Total'] = 0;
			while( $values = mysql_fetch_array( $query ) ) {
			$somas['Aprovados'] += $values['Aprovados'];
			$somas['Pendentes de plano de a��o'] += $values['Pendentes de plano de a��o'];
			$somas['Suspensos'] += $values['Suspensos'];
			$somas['Cancelados'] += $values['Cancelados'];
			$somas['Reprovados'] += $values['Reprovados'];
			$somas['Total'] += $values['Total'];
			?>
			<tr align="center" class="<?php if((($i++)%2)==0) echo "zebra-dark"; ?> hover">
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Estado']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Aprovados']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Pendentes de plano de a��o']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Suspensos']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Cancelados']?></td>
				<!-- <td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Reprovados']?></td> -->
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $values['Total']?></td>
			</tr>

		<?php 
			}//Fim while
			?>
			<tr align="center" class="<?php if((($i++)%2)==0) echo "zebra-dark"; ?> hover">
				<td style="font-size:15px; padding: 10px; width: 250px;">Totais</td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $somas['Aprovados']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $somas['Pendentes de plano de a��o']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $somas['Suspensos']?></td>
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $somas['Cancelados']?></td>
				<!-- <td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $somas['Reprovados']?></td> -->
				<td style="font-size:15px; padding: 10px; width: 250px;"><?php echo $somas['Total']?></td>
			</tr>
			<?php
		} else {
			echo '<center><br><br><br><font style="font-size:18px; font-weigth:bold;">Dados insuficientes para gerar o relat�rio.</font><br><br><a href="javascript:history.back(-1)">Voltar</a><br><br><br></center>';
		} ?>
	</table>
	

		