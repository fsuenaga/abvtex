	<?php
		
		if($_COOKIE['perm'] != 'Administradora'){
			$filtro = " JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.empresa = '".$_COOKIE['empr']."' AND CC_Fr_Vinculo.ativo = '1'";
		}else{
			$filtro = "";
		}
		
		$sql = "
			SELECT 
				CC_Fr.status_fornecedor, count(*) as linhas
			FROM CC_Fornecedor 
			
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor AND CC_Fr.empresa = '".$_COOKIE['empresa']."'
			".$filtro."	
				
				JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."' AND CC_Associacao.associado = '".$_COOKIE['empr']."'
				
			GROUP BY CC_Fr.status_fornecedor
		";
		$query = mysql_query($sql) or die(mysql_error());
		while($values = mysql_fetch_array($query)){
			$results["_".$values['status_fornecedor']] = $values['linhas'];
		}
		
		$grafico['\'Fornecedores\''] = array_sum($results);
		foreach($audi_resultado as $k=>$v){
			if(isset($results[$k])){
				$grafico["'".$v."'"] = $results[$k];
			}else{
				$grafico["'".$v."'"] = 0;
			}
			
		}
	
	?>
	
	
	<a style="float:right;" href="javascript:history.back(-1)"><img alt="Voltar" src="images/voltar.png"></a>
	
	<br clear="both" /><br/>
	
	<!-- JS -->		
		<!-- 1. Add these JavaScript inclusions in the head of your page -->
			<script type="text/javascript" src="js/highcharts/highcharts.js"></script>

		<!-- 1b) Optional: the exporting module -->
			<script type="text/javascript" src="js/highcharts/modules/exporting.js"></script>
            
            <script type="text/javascript">
			
				w = screen.availWidth;
				
				h = screen.availHeight;
				
				$(document).ready(function() {
					$('#container').css('min-height', h);
				});
			</script>
			
			
			
			<script type="text/javascript">
		
			var chart;
			$(document).ready(function() {
				
				var colors = Highcharts.getOptions().colors,
					categories = [<?php echo implode(',',array_keys($grafico));  ?>],
					name = 'Fornecedores',
					data = [<?php 
							$i=0;
							echo '{y: '. number_format((($grafico['\'Fornecedores\'']*100)/$grafico['\'Fornecedores\'']), 2, '.', ' ').',color: colors['.$i.']},';
							foreach($grafico as $k => $v){
								if($k != '\'Fornecedores\''){
									echo '{y: '. number_format((($v*100)/$grafico['\'Fornecedores\'']), 2, '.', ' ').',color: colors['.$i.']}';
									if($i<count($grafico)) echo ',';
								}
							}
					?>];
				
				function setChart(name, categories, data, color) {
					chart.xAxis[0].setCategories(categories);
					chart.series[0].remove();
					chart.addSeries({
						name: name,
						data: data,
						color: color || 'white'
					});
				}
				
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'grafico', 
						type: 'column'
					},
					legend: {
				        enabled: false
				    },
					title: {
						text: 'Status dos Fornecedores'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: categories,
						labels: {
							style: {
								color: '#000000'
							}
						}					
					},
					yAxis: {
						max: 100,
						title: {
							text: 'N�meros do Cadastro'
						}
					},
					plotOptions: {
						column: {
							//cursor: 'pointer',
							point: {
								events: {
									/*click: function() {
										var drilldown = this.drilldown;
										if (drilldown) { // drill down
											setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
										} else { // restore
											setChart(name, categories, data);
										}
									}*/
								}
							},
							dataLabels: {
								enabled: true,
								color: colors[0],
								style: {
									fontWeight: 'bold'
								},
								formatter: function() {
									return this.y +'%';
									//return this.y;
								}
							}					
						}
					},
					tooltip: {
						formatter: function() {
							var point = this.point,
								s = this.x +':<b>'+ this.y +'% Fornecedores</b>';
							/*if (point.drilldown) {
								s += 'Clique para ver '+ point.category +' vers�es';
							} else {
								s += 'Clique para retornar para as marcas';
							}*/
							return s;
						}
					},
					series: [{
						name: name,
						data: data,
						color: '#000000'
					}],
					exporting: {
						enabled: false
					}
				});
				
				
			});
				
		</script>
		
		<div id="grafico" style="width: 800px; height: 400px; margin: 0 auto"></div>

	
	
	
	
	<table cellpadding="2" cellspacing="2" width="561" align="center" style="margin: 10px auto 50px auto;">
		<?php
			$i=0; 
			
			$tabela["'Fornecedores'"] = 'fornecedores';
			foreach($audi_resultado as $k => $v){
				$tabela["'".$v."'"] = $k;
			}
			
			foreach($grafico as $k => $v){ ?>
			
			<tr class="<?php if((($i++)%2)==0) echo "zebra-dark"; ?>">
				<td><a href="relatorios.php?ac=fornecedores&qr=<?php echo $tabela[$k]; ?>" target="_blank"><?=str_replace("'","",$k)?></a></td>
				<td style="width:20px; text-align: right;"><?php echo $v;?></td>
			</tr>
			
		<?php } ?>
	</table>