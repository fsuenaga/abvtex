

<?php if($_GET['ft'] != 1){ ?>
	<form action="" method="get" target="_blank">
		<input type="hidden" name="ac" value="auditorias" />
		<input type="hidden" name="ft" value="1" />
		<ul>
			<li style="font-size:15px;">
				Filtros
				<a style="float:right;" href="relatorios.php"><img alt="Voltar" src="images/voltar.png"></a>
			</li>
			<li style="font-weight:normal;">Selecione os filtros desejados</li>
			
				<li class="zebra-dark">Estado</li>
				<li>
					<select name="uf">
						<option value="">Todos</option>
						<?php 
						$sql = "SELECT * FROM TB_Estado ORDER BY uf";
						$query = mysql_query($sql);
						while($value=mysql_fetch_array($query)){
						
							echo '<option value="'.$value['uf'].'">'.$value['uf'].'</option>';
	    				} ?>
					</select>
				</li>
			
			<li class="zebra-dark">Data da auditoria</li>
			<li>
				<input type="text" name="dta1" id="dataAudi1" style="width: 80px" class="data" /> at&#233; 
				<input type="text" name="dta2" id="dataAudi2" style="width: 80px" class="data" />
			</li>
			
			<li class="zebra-dark">Status</li>
			<li>
				<select name="st">
					<option value="">Todos</option>
					<?php
						foreach($audi_resultado as $k=>$v){
							echo '<option value="'.str_replace("_", "", $k).'">'.$v.'</option>';
						}
					?>
				</select>
			</li>
			
			<?php if($_COOKIE['perm'] != 'Certificadora'){ ?>
				<li class="zebra-dark">Certificadora</li>
				<li>
					<select name="ct">
						<option value="">Todos</option>
						<?php 
						foreach($arr_certificadoras as $k=>$v){
							echo '<option value="'.$k.'">'.$v.'</option>';
    					} ?>
					</select>
				</li>
			<?php } ?>
			
			
			
			<li class="zebra-dark">Ordenar por</li>
    		<li>
    			<select name="ordenar">
    				<option value="CC_Fornecedor.razaoSocial">Raz&atilde;o Social</option>
    				<option value="CC_Fornecedor.nomeFantasia">Nome Fantasia</option>
    				<option value="CC_Fornecedor.CNPJ">CNPJ</option>
    				<option value="CC_Fornecedor.fornecedor">ID do Fornecedor</option>
    				<option value="Audi_Fornecedor.data">Data Auditoria</option>
    			</select>
    		</li>
    		
    		<li class="zebra-dark">Ordenar por</li>
    		<li>
    			<select name="ordenar_tipo">
    				<option value="ASC">Crescente</option>
    				<option value="DESC">Decrescente</option>
    			</select>
    		</li>
			
			
			<li class="">Formato</li>
    		<li>
    			<select name="fm">
    				<option value="web">P&#225;gina Web</option>
    				<option value="exc">Excel</option>
    			</select>
    		</li>
			
			
			<li class="zebra-dark"><input type="submit" value="Criar Relat&#243;rio" /></li>
		</ul>
	</form>
<?php }else{
		
		unset($ORs);
		unset($ANDs);
		
		//Filtrar por Estado
		if($_GET['uf'] != ''){
			$ORs[] = " CC_Fornecedor.estado = '".tratarVariaveis($_GET['uf'])."' ";
		}

		//Filtrar por id Fornecedor
		if($_GET['id'] != ''){
			$td_ids = explode(';', $_GET['id']);
			foreach($td_ids as $id){
				$arr_id[] = "'".trim(str_replace(array('.','/', '-'),"", $id))."'";
			}
			$ORs[] = " CC_Fornecedor.fornecedor IN (".implode(',',$arr_id).") ";
			
		}
		
		//CNPJ
		if($_GET['cnpj'] != ''){
			$td_cnpjs = explode(';', $_GET['cnpj']);
			foreach($td_cnpjs as $cnpj){
				$arr_cnpj[] = "'".trim(str_replace(array('.','/', '-'),"", $cnpj))."'";
			}
			$ORs[] = " CC_Fornecedor.CNPJ IN (".implode(',',$arr_cnpj).") ";
		}
		
		//RazaoSocial ou NomeFantasia
		if($_GET['rs'] != ''){
			$ORs[] = " ( ((CC_Fornecedor.razaoSocial LIKE '%".tratarVariaveis($_GET['rs'])."%') OR (CC_Fornecedor.nomeFantasia LIKE '%".tratarVariaveis($_GET['rs'])."%')) ) ";
		}
		
		//Tipo de Fornecedor
		if($_GET['tp'] != ''){
			if($_GET['tp'] == '1'){
				$ORs[] = " CC_Fr.classe = '".tratarVariaveis($_GET['tp'])."' ";
			}elseif($_GET['tp'] == '2'){
				//Muda query
				if($_COOKIE['perm'] == 'Certificadora'){
					$ORs[] = " CC_Fr.classe = '".tratarVariaveis($_GET['tp'])."' ";
				}
			}
			
		}
		
		//Data Auditoria
		if($_GET['dta1'] != ''){
			$_GET['dta1'] =  convertDateSys($_GET['dta1']);
			if($_GET['dta2'] == ''){ $_GET['dta2'] = date('Y-m-d'); }else{ $_GET['dta2'] =  convertDateSys($_GET['dta2']); }
			
			$ANDs[] = " ( Audi_Fornecedor.data BETWEEN '".$_GET['dta1']."' AND '".$_GET['dta2']."' ) ";
		}
		
		
		if($_GET['dtv1'] != ''){
			if($_GET['dtv2'] != ''){
				
			}else{
				$_GET['dtv2'] = date('Y-m-d');
			}
		}
		
		//Status do Fornecedor
		if($_GET['st'] != ''){
			$ORs[] = " CC_Fr.status_fornecedor = '".tratarVariaveis($_GET['st'])."' ";
		}
		
		//Certificadora
		if($_GET['ct'] != ''){
			$ANDs[] = " ( CC_Fr.certificadora = '".tratarVariaveis($_GET['ct'])."' ) ";
		}
		
		//Varejista
		if($_GET['vr'] != ''){
			$ORs[] = " ( CC_Associacao.associado = '".tratarVariaveis($_GET['vr'])."' AND CC_Associacao.tipo = 'Varejista' ) ";
		}
		
		//Status de Fornecimento
		if($_GET['st_fr'] != ''){
			$ANDs[] = " AND CC_Fr_Vinculo.status = '".$_GET['st_fr']."'";
			 
		}
		
		
		if($_COOKIE['perm'] != 'Administradora'){
			$ANDs[] = " ( CC_Fr_Vinculo.empresa = '".$_COOKIE['empr']."' AND CC_Fr_Vinculo.ativo = '1' ) ";
		}

	
		$ordenar = " ORDER BY ".$_GET['ordenar']." ".$_GET['ordenar_tipo']."";

	?>
	
	
		<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="3000px">
		<tr>
			<td colspan="24" class="mainTitle">Relat&#243;rio de Auditorias</td>
		</tr>
		<tr>
			<td colspan="24">
				<b>Filtros Aplicados</b>:<br/>
		<?php
			if($_GET['uf'] != ''){ echo '<b>Estado</b>: '.$_GET['uf'].'<br/>'; }
			if($_GET['id'] != ''){ echo '<b>N&ordm; ID</b>: '.$_GET['id'].'<br/>'; }
			if($_GET['cnpj'] != ''){ echo '<b>CNPJ</b>: '.$_GET['cnpj'].'<br/>'; }
			if($_GET['rs'] != ''){ echo '<b>Raz&#227;o Social</b>: '.$_GET['rs'].'<br/>'; }
			if($_GET['tp'] != ''){ echo '<b>Tipo</b>: '.$arr_tipo[$_GET['tp']].'<br/>';}
			if($_GET['dta1'] != ''){ echo '<b>Per&#237;odo de auditoria</b>: '.convertSysDate($_GET['dta1']).' at&#233; '.convertSysDate($_GET['dta2']).'<br/>'; }
			if($_GET['dtv1'] != ''){ echo '<b>Per&#237;odo de validade</b>: '.convertSysDate($_GET['dtv1']).' at&#233; '.convertSysDate($_GET['dtv2']).'<br/>'; }
			if($_GET['st'] != ''){ echo '<b>Status</b>: '.$audi_resultado['_'.$_GET['st']].'<br/>'; }
			if($_GET['ct'] != ''){ echo '<b>Certificadora</b>: '.$arr_certificadoras[$_GET['ct']].'<br/>'; }
			if($_GET['vr'] != ''){ echo '<b>Varejista</b>: '.$arr_varejistas[$_GET['vr']].'<br/>'; }
		?>
			</td>
		</tr>
		</table>
	
	<?php 
	 
	
	//---------------------------------------------------------------------------------------
	//Fornecedores
		//Query para buscar Fornecedores
	 	$sql = "SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fornecedor 
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
				JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.ativo = '1'
				JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa
			WHERE 
				CC_Associacao.empresa = '".$_COOKIE['empresa']."'
				AND CC_Associacao.associado = '".$_COOKIE['empr']."'
				AND CC_Fr.empresa = '".$_COOKIE['empresa']."'
				
				".( ( is_array($ORs) )? " AND ( ".implode(' OR ',$ORs).") " : "" )."
				".( ( is_array($ANDs) )? " ".implode(' AND ',$ANDs)." " : "" )."
				
				".$ordenar."
			";
			
			
			$sql = "
			SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fornecedor 
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
				JOIN Audi_Fornecedor ON Audi_Fornecedor.fornecedor = CC_Fornecedor.fornecedor AND Audi_Fornecedor.empresa = CC_Fr.empresa
				LEFT JOIN Audi_Proposta ON Audi_Proposta.empresa = CC_Fr.empresa AND Audi_Proposta.certificadora = Audi_Fornecedor.certificadora AND Audi_Proposta.fornecedor = CC_Fornecedor.fornecedor
			WHERE 
				CC_Fr.empresa = '".$_COOKIE['empresa']."'
				
				".( ( is_array($ORs) )? " AND ( ".implode(' OR ',$ORs).") " : "" )."
				".( ( is_array($ANDs) )? " AND ".implode(' AND ',$ANDs)." " : "" )."
				
				".$ordenar."
			";
			
			//if($_COOKIE['user'] == 'yuri3') echo $sql;
			
		$query = mysql_query($sql) or die(mysql_error());
		
	?>
	
	
	
	<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="3000px">
		<tr>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">#</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">N&ordm; ID</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Raz&#227;o Social</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CNPJ</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Telefone Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Email Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Tipo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Certificadora</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Auditoria tipo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Resultado</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nota</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Valor da proposta para o per�odo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">N&ordm; HD</td>
		</tr>
	
	<?php
	if(mysql_num_rows($query) > 0){
		for($i=0;mysql_num_rows($query) > $i;$i++){
		?>
		<tr class="<?php echo(($i%2==0)?"zebra-dark":""); ?> hover">
			<td><?php echo ($i+1); ?></td>
			<td><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?>&ac=cadastro" target="_blank"><?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?></a></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mascaras(mysql_result($query,$i,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.resp_nome'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_telefone');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_email');?></td>
			<td><?php echo $arr_tipo[mysql_result($query,$i,'CC_Fr.classe')];?></td>
			<td><?php echo $arr_certificadoras[mysql_result($query,$i,'nomeFantasiaCertificadora')]; ?></td>
			<td><?php echo htmlentities($audi_tipo[mysql_result($query,$i,'Audi_Fornecedor.tipo')], ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo convertSysDate(mysql_result($query,$i,'Audi_Fornecedor.data')); ?></td>
			<td><?php echo htmlentities($audi_resultado['_'.mysql_result($query,$i,'Audi_Fornecedor.resultado')], ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo number_format(mysql_result($query,$i,'Audi_Fornecedor.nota'),2,",",".");?></td>
			<td><?php echo ((mysql_result($query,$i,'Audi_Proposta.valor') != '')?'R$ '.mysql_result($query,$i,'Audi_Proposta.valor'):""); ?></td>
			<td><?php echo $total_hds[] = mysql_result($query,$i,'Audi_Fornecedor.num_hd'); ?></td>
		</tr>
		
	<?php } ?>
		<tr class="<?php echo(($i%2==0)?"zebra-dark":""); ?> hover">
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;" colspan="14"><?php echo 'Total: '.($i); ?></td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px; text-align: right;"><?php echo array_sum($total_hds); ?></td>
		</tr>

	<?php } ?>
	</table>
	
<?php 
	}
 ?>
	
		