

<?php if($_GET['ft'] != 1){ ?>
	<form action="" method="get" target="_blank">
		<input type="hidden" name="ac" value="dinamico" />
		<input type="hidden" name="ft" value="1" />
		<ul>
			<li style="font-size:15px;">
				Filtros
				<a style="float:right;" href="relatorios.php"><img alt="Voltar" src="images/voltar.png"></a>
			</li>
			<li style="font-weight:normal;">Selecione os filtros desejados</li>
			
				<li class="zebra-dark">Estado</li>
				<li>
					<select name="uf">
						<option value="">Todos</option>
						<?php 
						$sql = "SELECT * FROM TB_Estado ORDER BY uf";
						$query = mysql_query($sql);
						while($value=mysql_fetch_array($query)){
						
							echo '<option value="'.$value['uf'].'">'.$value['uf'].'</option>';
	    				} ?>
					</select>
				</li>
			
			<li class="zebra-dark">N&ordm; ID do fornecedor/subcontratado <font style="font-size:9px;">(Para pesquisar mais de um, separe por ";")</font></li>
			<li><textarea name="id" class="idFornecedor" style="width:500px; height:30px;"></textarea></li>
			
				<li class="zebra-dark">CNPJ <font style="font-size:9px;">(Para pesquisar mais de um, separe por ";")</font></li>
				<li><textarea name="cnpj" style="width:500px; height:30px;"></textarea></li>
			
			<li class="zebra-dark">Raz&#227;o Social</li>
			<li><input type="text" name="rs" class="" style="width:200px;"></li>
			
				<li class="zebra-dark">Tipo</li>
				<li>
					<select name="tp">
						<option value="">Todos</option>
						<option value="1">Fornecedor</option>
						<option value="2">Subcontratado</option>
						<option value="cadeia">Cadeia</option>
					</select>
				</li>
			
			<li class="zebra-dark">Data da auditoria</li>
			<li>
				<input type="text" name="dta1" id="dataAudi1" style="width: 80px" class="data" /> at&#233; 
				<input type="text" name="dta2" id="dataAudi2" style="width: 80px" class="data" />
			</li>
			
			<li class="zebra-dark">Status</li>
			<li>
				<select name="st">
					<option value="">Todos</option>
					<?php
						foreach($audi_resultado as $k=>$v){
							echo '<option value="'.str_replace("_", "", $k).'">'.$v.'</option>';
						}
					?>
				</select>
			</li>
			<?php if($_COOKIE['perm'] == 'Varejista'){ ?>
			<li class="zebra-dark">Status de Fornecimento</li>
			<li>
				<select name="st_fr">
					<option value="2">Ativo</option>
					<option value="1" selected="">Pendente de Aprova&ccedil;&atilde;o</option>
					<option value="3" selected="">Aprovados para Auditoria</option>
					<option value="">Todos</option>
				</select>
			</li>
			<?php } ?>
			
			<?php if($_COOKIE['perm'] != 'Certificadora'){ ?>
				<li class="zebra-dark">Certificadora</li>
				<li>
					<select name="ct">
						<option value="">Todos</option>
						<?php 
						foreach($arr_certificadoras as $k=>$v){
							echo '<option value="'.$k.'">'.$v.'</option>';
    					} ?>
					</select>
				</li>
			<?php } ?>
			
			
			<?php if($_COOKIE['perm'] == 'Administradora213'){ ?>
				<li class="zebra-dark">Varejista</li>
				<li>
					<select name="vr">
						<option value="">Todos</option>
						<?php 
						foreach($arr_varejistas as $k=>$v){
							echo '<option value="'.$k.'">'.$v.'</option>';
    					} ?>
					</select>
				</li>
			<?php } ?>
			
			
			<li class="zebra-dark">Ordenar por</li>
    		<li>
    			<select name="ordenar">
    				<option value="razaoSocial">Raz&atilde;o Social</option>
    				<option value="nomeFantasia">Nome Fantasia</option>
    				<option value="CNPJ">CNPJ</option>
    				<option value="fornecedor">ID do Fornecedor</option>
    			</select>
    		</li>
			
			
			<li class="">Formato</li>
    		<li>
    			<select name="fm">
    				<option value="web">P&#225;gina Web</option>
    				<option value="exc">Excel</option>
    			</select>
    		</li>
			
			
			<li class="zebra-dark"><input type="submit" value="Criar Relat&#243;rio" /></li>
		</ul>
	</form>
<?php }else{
		
		unset($ORs);
		unset($ANDs);
		
		//Filtrar por Estado
		if($_GET['uf'] != ''){
			$ORs[] = " CC_Fornecedor.estado = '".tratarVariaveis($_GET['uf'])."' ";
		}

		//Filtrar por id Fornecedor
		if($_GET['id'] != ''){
			$td_ids = explode(';', $_GET['id']);
			foreach($td_ids as $id){
				$arr_id[] = "'".trim(str_replace(array('.','/', '-'),"", $id))."'";
			}
			$ORs[] = " CC_Fornecedor.fornecedor IN (".implode(',',$arr_id).") ";
			
		}
		
		//CNPJ
		if($_GET['cnpj'] != ''){
			$td_cnpjs = explode(';', $_GET['cnpj']);
			foreach($td_cnpjs as $cnpj){
				$arr_cnpj[] = "'".trim(str_replace(array('.','/', '-'),"", $cnpj))."'";
			}
			$ORs[] = " CC_Fornecedor.CNPJ IN (".implode(',',$arr_cnpj).") ";
		}
		
		//RazaoSocial ou NomeFantasia
		if($_GET['rs'] != ''){
			$ORs[] = " ( ((CC_Fornecedor.razaoSocial LIKE '%".tratarVariaveis($_GET['rs'])."%') OR (CC_Fornecedor.nomeFantasia LIKE '%".tratarVariaveis($_GET['rs'])."%')) ) ";
		}
		
		//Tipo de Fornecedor
		if($_GET['tp'] != ''){
			if($_GET['tp'] == '1'){
				$ORs[] = " CC_Fr.classe = '".tratarVariaveis($_GET['tp'])."' ";
			}elseif($_GET['tp'] == '2'){
				//Muda query
				if( ($_COOKIE['perm'] == 'Certificadora') || ($_COOKIE['perm'] == 'Administradora')){
					$ANDs[] = " CC_Fr.classe = '".tratarVariaveis($_GET['tp'])."' ";
				}
			}
			
		}
		
		//Data Auditoria
		if($_GET['dta1'] != ''){
			$_GET['dta1'] =  convertDateSys($_GET['dta1']);
			if($_GET['dta2'] == ''){ $_GET['dta2'] = date('Y-m-d'); }else{ $_GET['dta2'] =  convertDateSys($_GET['dta2']); }
			
			$ORs[] = " ( CC_Fr.data_certificacao BETWEEN '".$_GET['dta1']."' AND '".$_GET['dta2']."' ) ";
		}
		
		
		if($_GET['dtv1'] != ''){
			if($_GET['dtv2'] != ''){
				
			}else{
				$_GET['dtv2'] = date('Y-m-d');
			}
		}
		
		//Status do Fornecedor
		if($_GET['st'] != ''){
			$ORs[] = " CC_Fr.status_fornecedor = '".tratarVariaveis($_GET['st'])."' ";
		}
		
		//Certificadora
		if($_GET['ct'] != ''){
			$ORs[] = " ( CC_Fr.certificadora = '".tratarVariaveis($_GET['ct'])."' AND CC_Fr.certificadoraTipo = 'Certificadora' ) ";
		}
		
		//Varejista
		if($_GET['vr'] != ''){
			$ORs[] = " ( CC_Associacao.associado = '".tratarVariaveis($_GET['vr'])."' AND CC_Associacao.tipo = 'Varejista' ) ";
		}
		
		//Status de Fornecimento
		if($_COOKIE['perm'] == 'Varejista'){
			if($_GET['st_fr'] != ''){
				$ANDs[] = " CC_Fr_Vinculo.status = '".$_GET['st_fr']."' ";
			}
		}
		
		
		if(($_COOKIE['perm'] != 'Administradora') && ($_COOKIE['perm'] != 'Certificadora')){
			$ANDs[] = " ( CC_Fr_Vinculo.empresa = '".$_COOKIE['empr']."' AND CC_Fr_Vinculo.ativo = '1' ) ";
		}
		
		if(($_COOKIE['perm'] != 'Administradora')){
			$JOIN = "
			 JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.ativo = '1'
			 JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."' AND CC_Associacao.associado = '".$_COOKIE['empr']."' ";
		}

	
	$ordenar = " ORDER BY CC_Fornecedor.".$_GET['ordenar']." ASC";

	?>
	
	
		<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="3000px">
		<tr>
			<td colspan="24" class="mainTitle">Relat&#243;rio Din&#226;mico</td>
		</tr>
		<tr>
			<td colspan="24">
				<b>Filtros Aplicados</b>:<br/>
		<?php
			if($_GET['uf'] != ''){ echo '<b>Estado</b>: '.$_GET['uf'].'<br/>'; }
			if($_GET['id'] != ''){ echo '<b>N&ordm; ID</b>: '.$_GET['id'].'<br/>'; }
			if($_GET['cnpj'] != ''){ echo '<b>CNPJ</b>: '.$_GET['cnpj'].'<br/>'; }
			if($_GET['rs'] != ''){ echo '<b>Raz&#227;o Social</b>: '.$_GET['rs'].'<br/>'; }
			if($_GET['tp'] != ''){ echo '<b>Tipo</b>: '.$arr_tipo[$_GET['tp']].'<br/>';}
			if($_GET['dta1'] != ''){ echo '<b>Per&#237;odo de auditoria</b>: '.convertSysDate($_GET['dta1']).' at&#233; '.convertSysDate($_GET['dta2']).'<br/>'; }
			if($_GET['dtv1'] != ''){ echo '<b>Per&#237;odo de validade</b>: '.convertSysDate($_GET['dtv1']).' at&#233; '.convertSysDate($_GET['dtv2']).'<br/>'; }
			if($_GET['st_fr'] != ''){ echo '<b>Status Fornecedor</b>: '.$arr_fornecimento[$_GET['st_fr']].'<br/>'; }
			if($_GET['st'] != ''){ echo '<b>Status</b>: '.$audi_resultado['_'.$_GET['st']].'<br/>'; }
			if($_GET['ct'] != ''){ echo '<b>Certificadora</b>: '.$arr_certificadoras[$_GET['ct']].'<br/>'; }
			if($_GET['vr'] != ''){ echo '<b>Varejista</b>: '.$arr_varejistas[$_GET['vr']].'<br/>'; }
		?>
			</td>
		</tr>
		</table>
	
	<?php 
	
	if(($_GET['tp'] == '1') || ($_GET['tp'] == '') || ((($_COOKIE['perm'] == 'Certificadora') || ($_COOKIE['perm'] == 'Administradora')) && ($_GET['tp'] == '2') )){ 
	
	//---------------------------------------------------------------------------------------
	//Fornecedores
		//Query para buscar Fornecedores
		

		
	 	$sql = "SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fornecedor 
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
				".$JOIN."
			WHERE 
				CC_Fr.empresa = '".$_COOKIE['empresa']."'
				
				".( ( is_array($ORs) )? " AND ( ".implode(' OR ',$ORs).") " : "" )."
				".( ( is_array($ANDs) )? "AND ".implode(' AND ',$ANDs)." " : "" )."
				
				".$ordenar."
			";
			//if($_COOKIE['user'] == 'yuri3'){ echo $sql; } 
			
		$query = mysql_query($sql) or die(mysql_error());
		
		
	?>
	
	
	
	<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="3000px">
		<tr>
			<td colspan="24" align="center" style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Fornecedores</td>
		</tr>
		<tr>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">#</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">N&ordm; ID</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Raz&#227;o Social</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Fantasia</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CNPJ</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Inscri&#231;&#227;o Estadual</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Telefone Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Celular Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Email Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Endere&#231;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Bairro</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Cidade</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Estado</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CEP</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Tipo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Certificadora</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de Validade da Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Fornecedor</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Cadeia</td>
			
			
			<?php if($_COOKIE['perm'] != 'Administradora'){ ?>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de V&#237;nculo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de Confima&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Empresa</td>
			<?php } ?>
		</tr>
	
	<?php
	if(mysql_num_rows($query) > 0){
		for($i=0;mysql_num_rows($query) > $i;$i++){
		?>
		<tr class="<?php echo(($i%2==0)?"zebra-dark":""); ?> hover">
			<td><?php echo ($i+1); ?></td>
			<td><?php echo (($_GET['fm'] != 'exc')?'<a href="fornecedor_dados.php?fr='.mysql_result($query,$i,'CC_Fornecedor.fornecedor').'&ac=cadastro" target="_blank">':"").mysql_result($query,$i,'CC_Fornecedor.fornecedor').(($_GET['fm'] != 'exc')?'</a>':""); ?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mascaras(mysql_result($query,$i,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.inscricao');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.resp_nome'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_telefone');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_celular');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_email');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.rua'), ENT_QUOTES, "ISO-8859-1").','.mysql_result($query,$i,'numero');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.bairro'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.cidade'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.estado');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.cep9');?></td>
			<td><?php echo $arr_tipo[mysql_result($query,$i,'CC_Fr.classe')];?></td>
			<td><?php echo $arr_certificadoras[mysql_result($query,$i,'nomeFantasiaCertificadora')]; ?></td>
			
			
			
			<?php
				//Verifica status da Cadeia
				$sql = "SELECT 
							v2.* 
						FROM CC_Fr 
							JOIN CC_Fr v2 ON v2.fornecedor = CC_Fr.fornecedor AND v2.empresa = '".$_COOKIE['empresa']."'
						WHERE CC_Fr.empresa = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."'";
				$querySubs = mysql_query($sql) or die(mysql_error());
				unset($subs);
				unset($status_cadeia);
				//echo $value['fornecedor'].' - '.mysql_num_rows($querySubs);
				
				
				if( (mysql_num_rows($querySubs) > 0) && ($value['classe'] != '2') ){
					
					while($vSubs = mysql_fetch_array($querySubs)){ $subs[] = '_'.$vSubs['status_certificacao']; }
					// '' - nao iniciado, 0 - reprovado, 1 - aprovado, 2 - pendente
					/*
					//Se Reprovado, Suspenso, Cancelado
					if((count(array_keys($subs,'_0'))>0) || (count(array_keys($subs,'_6'))>0) || (count(array_keys($subs,'_5'))>0)) 		$status_cadeia = 0;
					//Se Pendente, Auditoria Agendada, Aguardando Aprova��o
					elseif((count(array_keys($subs,'_2'))>0) || (count(array_keys($subs,'_3'))>0) || (count(array_keys($subs,'_4'))>0)) 	$status_cadeia = 2; 
					//Se N�o Iniciado
					elseif(count(array_keys($subs,'_'))>0) 		$status_cadeia = '';
					else 										$status_cadeia = 1;
						if($value['classe'] == '2') $status_cadeia = 'nao';
					*/
					
					if(count(array_keys($subs,'_0'))>0) $status_cadeia = 0;
					elseif(count(array_keys($subs,'_6'))>0) $status_cadeia = 6;
					elseif(count(array_keys($subs,'_5'))>0) $status_cadeia = 5;
					elseif(count(array_keys($subs,'_2'))>0) $status_cadeia = 2;
					elseif(count(array_keys($subs,'_3'))>0) $status_cadeia = 3;
					elseif(count(array_keys($subs,'_4'))>0)	$status_cadeia = 4; 
					elseif(count(array_keys($subs,'_'))>0) 	$status_cadeia = '';
					else 									$status_cadeia = 1;
					
					if($value['classe'] == '2') $status_cadeia = 'nao';
					
					
					
					unset($vSubs2);
					unset($subs2);
					mysql_data_seek($querySubs,0);
					while($vSubs2 = mysql_fetch_array($querySubs)){
						//Se for pendente de plano de a��o na manuten��o
						if(($vSubs2['status_certificacao'] == '2') && (($vSubs2['tipo_auditoria'] == '1') || ($vSubs2['tipo_auditoria'] == '3')) ){

							$subs2[] = '_1';
						}else{
							$subs2[] = '_'.$vSubs2['status_certificacao'];
						}
					}
					
					
					//Se Reprovado, Suspenso, Cancelado
					if((count(array_keys($subs2,'_0'))>0) || (count(array_keys($subs2,'_6'))>0) || (count(array_keys($subs2,'_5'))>0)) 		$certificado = 0;
					//Se Pendente, Auditoria Agendada, Aguardando Aprova��o
					elseif((count(array_keys($subs2,'_2'))>0) || (count(array_keys($subs2,'_3'))>0) || (count(array_keys($subs2,'_4'))>0)) 	$certificado = 0; 
					//Se N�o Iniciado
					elseif(count(array_keys($subs2,'_'))>0) 		$certificado = 0;
					else 										$certificado = 1;
					
					if($value['classe'] == '2') $certificado = 0;

				}else{
					$status_cadeia = 'nao';
					if(mysql_result($query,$i,'CC_Fr.status_fornecedor') == '1'){ $certificado = 1; }else{ $certificado = 0; }
					
				}
				
				
				$certificacao = mysql_result($query,$i,'CC_Fr.data_certificacao');
			
				if($certificacao != ''){
					$val = explode('-', $certificacao);								
					$validade = ($val[0]+2).'-'.$val[1].'-'.$val[2];
				}else{
					$validade = '';
				}
				
			?>
			
			
			
			<td align="center"><?php if($certificacao != '') echo convertSysDate($certificacao); else echo '--'; ?></td>
			<td align="center"><?php if($validade != '') echo convertSysDate($validade); else echo '--'; ?></td>
			
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php echo $audi_certificacao_cor['_'.$certificado]; ?>;color:white;font-weight:bold;">
				<?php if($_COOKIE['perm'] != 'Certificadora'){ ?><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i,'CC_Fr.fornecedor'); ?>&ac=auditoria" style="color:white;"><?php } ?>
				<?php echo $audi_certificacao['_'.$certificado]; ?>
				<?php if($_COOKIE['perm'] != 'Certificadora'){ ?></a><?php } ?>
			</td>
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php echo $audi_resultado_cor['_'.mysql_result($query,$i,'CC_Fr.status_certificacao')]; ?>;color:white;font-weight:bold;"><?php echo $audi_resultado['_'.mysql_result($query,$i,'CC_Fr.status_certificacao')].( (mysql_result($query,$i,'CC_Fr.status_fornecedor') == '6')? '<br>'.convertSysDate(mysql_result($query,$i,'CC_Fr.data_status')) : '' ); ?></td>
			<td align="center" style="border-collapse: collapse; border: #FFF; background-color:<?php if($status_cadeia === 'nao') echo $audi_resultado_cor['_1']; else echo $audi_resultado_cor['_'.$status_cadeia]; ?>;color:white;font-weight:bold;"><?php if($status_cadeia === 'nao') echo 'Sem Subcontratados'; else echo '<a href="fornecedor_dados.php?fr='.mysql_result($query,$i,'CC_Fr.fornecedor').'&ac=subcontratados" style="color:#FFF;">'.$audi_resultado['_'.$status_cadeia].'</a>'; ?></td>

			
			
			<?php if($_COOKIE['perm'] != 'Administradora'){ ?>
			<td align="center"><?php echo convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.criacao'));?></td>
			<td align="center"><?php echo ((mysql_result($query,$i,'CC_Fr_Vinculo.confirmacao') != '')?convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.confirmacao')):" --- "); ?></td>
			<td><?php echo htmlentities($arr_associados[mysql_result($query,$i,'CC_Fr_Vinculo.empresa')], ENT_QUOTES, "ISO-8859-1");?></td>
			<?php } ?>
		</tr>
		
	<?php
		}
	}
	?>
	</table>
	
<?php 
	}//Fim fornecedores
	
	//---------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------
	//Subcontratados

	if(($_COOKIE['perm'] != 'Certificadora') && ($_COOKIE['perm'] != 'Administradora')){
	
	if(($_GET['tp'] == '2') || ($_GET['tp'] == '')){ 
	
			//Query para buscar subcontratados
			$sql = "SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fr_Vinculo 
				JOIN CC_Fr vc_sub ON CC_Fr_Vinculo.fornecedor = vc_sub.empresa
				JOIN CC_Fr ON vc_sub.fornecedor = CC_Fr.fornecedor
				JOIN CC_Fornecedor ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
				
				JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa
			WHERE 
				CC_Fr.empresa = '".$_COOKIE['empresa']."'
				AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
				AND CC_Associacao.associado = '".$_COOKIE['empr']."'
				AND CC_Fr_Vinculo.ativo = '1'
				
				".( ( is_array($ORs) )? " AND ( ".implode(' OR ',$ORs).") " : "" )."
				".( ( is_array($ANDs) )? " AND ".implode(' AND ',$ANDs)." " : "" )."
				
				GROUP BY CC_Fr.fornecedor
				".$ordenar."";
		$query = mysql_query($sql) or die (mysql_error());
	?>
	
	<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="3000px">
		<tr>
			<td colspan="24" align="center" style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Subcontratados</td>
		</tr>
		<tr>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">#</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">N&ordm; ID</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Raz&#227;o Social</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Fantasia</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CNPJ</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Inscri&#231;&#227;o Estadual</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Telefone Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Celular Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Email Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Endere&#231;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Bairro</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Cidade</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Estado</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CEP</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Tipo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Certificadora</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Fornecedor</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de V&#237;nculo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de Confima&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Empresa</td>
		</tr>
	
	<?php
	if(mysql_num_rows($query) > 0){
		for($i=0;mysql_num_rows($query) > $i;$i++){
		?>
		<tr class="<?php echo(($i%2==0)?"zebra-dark":""); ?> hover">
			<td><?php echo ($i+1); ?></td>
			<td><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?>&ac=cadastro" target="_blank"><?php echo (mysql_result($query,$i,'CC_Fornecedor.fornecedor') + 0);?></a></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mascaras(mysql_result($query,$i,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.inscricao');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.resp_nome'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_telefone');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_celular');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_email');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.rua'), ENT_QUOTES, "ISO-8859-1").','.mysql_result($query,$i,'numero');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.bairro'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.cidade'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.estado');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.cep9');?></td>
			<td>Subcontratado</td>
			<td><?php echo $arr_certificadoras[mysql_result($query,$i,'nomeFantasiaCertificadora')]; ?></td>
			<td align="center"><?php echo ((mysql_result($query,$i,'CC_Fr.data_certificacao') != '')?convertSysDate(mysql_result($query,$i,'CC_Fr.data_certificacao')):" --- ");?></td>
			<td align="center"><?php echo htmlentities($audi_resultado['_'.mysql_result($query,$i,'CC_Fr.status_fornecedor')], ENT_QUOTES, "ISO-8859-1"); ?></td>
			<td align="center"><?php echo htmlentities($audi_resultado['_'.mysql_result($query,$i,'CC_Fr.status_certificacao')], ENT_QUOTES, "ISO-8859-1"); ?></td>
			<td align="center"><?php echo convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.criacao'));?></td>
			<td align="center"><?php echo ((mysql_result($query,$i,'CC_Fr_Vinculo.confirmacao') != '')?convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.confirmacao')):" --- "); ?></td>
			<td><?php echo htmlentities($arr_associados[mysql_result($query,$i,'CC_Fr_Vinculo.empresa')], ENT_QUOTES, "ISO-8859-1");?></td>
		</tr>
		
	<?php
		}
	}
	?>
	</table>
	<?php
		}//FIm subcontratados
		}
	?>
	
	<?php
		//-------- 

	
	if(($_GET['tp'] == 'cadeia')){ 
	
	//---------------------------------------------------------------------------------------
	//Fornecedores
		//Query para buscar Fornecedores
	 	$sql = "
	 		SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fornecedor 
				JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
				JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor AND CC_Fr_Vinculo.ativo = '1'
				JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa
			WHERE 
				CC_Associacao.empresa = '".$_COOKIE['empresa']."'
				AND CC_Associacao.associado = '".$_COOKIE['empr']."'
				AND CC_Fr.empresa = '".$_COOKIE['empresa']."'
				
				".( ( is_array($ORs) )? " AND ( ".implode(' OR ',$ORs).") " : "" )."
				".( ( is_array($ANDs) )? " AND  ".implode(' AND ',$ANDs)." " : "" )."
				
				".$ordenar."
			";
		$query = mysql_query($sql);
	?>
	
	
	
	<table cellspacing="1" cellpadding="2" style="font-size: 12px;" width="3000px">
		<tr>
			<td colspan="24" align="center" style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Fornecedores</td>
		</tr>
		<tr>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">#</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Tipo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">N&ordm; ID</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Raz&#227;o Social</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Fantasia</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CNPJ</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Inscri&#231;&#227;o Estadual</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Nome Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Telefone Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Celular Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Email Respons&#225;vel</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Endere&#231;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Bairro</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Cidade</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Estado</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">CEP</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Certificadora</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Fornecedor</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Certifica&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Status Cadeia</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de V&#237;nculo</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Data de Confima&#231;&#227;o</td>
			<td style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;">Empresa</td>
		</tr>
	
	<?php
	if(mysql_num_rows($query) > 0){
		for($i=0;mysql_num_rows($query) > $i;$i++){
		
		//Query para buscar subcontratados
		$sql2 = "SELECT 
				*, CC_Fr.certificadora AS nomeFantasiaCertificadora
			FROM CC_Fr_Vinculo 
				JOIN CC_Fr vc_sub ON CC_Fr_Vinculo.fornecedor = vc_sub.empresa
				JOIN CC_Fr ON vc_sub.fornecedor = CC_Fr.fornecedor
				JOIN CC_Fornecedor ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor

				JOIN CC_Associacao ON CC_Associacao.associado = CC_Fr_Vinculo.empresa
			WHERE 
				CC_Fr.empresa = '".$_COOKIE['empresa']."'
				AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
				AND CC_Associacao.associado = '".$_COOKIE['empr']."'
				AND CC_Fr_Vinculo.ativo = '1'
				AND vc_sub.empresa = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."'
			
			ORDER BY CC_Fornecedor.razaoSocial ASC";
		$query2 = mysql_query($sql2);
		
		?>
		<tr class="<?php echo(($i%2==0)?"zebra-dark":""); ?> hover">
			<td rowspan="<?=(mysql_num_rows($query2) + 1)?>" valign="middle" style="border:1px solid #000; background: #D8D8D8; padding: 0 3px;"><?php echo ($i+1); ?></td>
			<td>Fornecedor</td>
			<td><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?>&ac=cadastro" target="_blank"><?php echo mysql_result($query,$i,'CC_Fornecedor.fornecedor');?></a></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mascaras(mysql_result($query,$i,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.inscricao');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.resp_nome'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_telefone');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_celular');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.resp_email');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.rua'), ENT_QUOTES, "ISO-8859-1").','.mysql_result($query,$i,'numero');?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.bairro'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo htmlentities(mysql_result($query,$i,'CC_Fornecedor.cidade'), ENT_QUOTES, "ISO-8859-1");?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.estado');?></td>
			<td><?php echo mysql_result($query,$i,'CC_Fornecedor.cep9');?></td>
			<td><?php echo $arr_certificadoras[mysql_result($query,$i,'nomeFantasiaCertificadora')]; ?></td>
			<td align="center"><?php echo ((mysql_result($query,$i,'CC_Fr.data_certificacao') != '')?convertSysDate(mysql_result($query,$i,'CC_Fr.data_certificacao')):" --- ");?></td>
			<td align="center"><?php echo htmlentities($audi_resultado['_'.mysql_result($query,$i,'CC_Fr.status_fornecedor')], ENT_QUOTES, "ISO-8859-1"); ?></td>
			<td align="center"><?php echo htmlentities($audi_resultado['_'.mysql_result($query,$i,'CC_Fr.status_certificacao')], ENT_QUOTES, "ISO-8859-1"); ?></td>

			<?php
				//Verifica status da Cadeia
				$sql = "SELECT * FROM CC_Fr WHERE CC_Fr.empresa = '".mysql_result($query,$i,'CC_Fornecedor.fornecedor')."'";
				$querySubs = mysql_query($sql) or die(mysql_error());
				unset($subs);
				if(mysql_num_rows($querySubs) > 0){
				    while($vSubs = mysql_fetch_array($querySubs)){ $subs[] = '_'.$vSubs['status_fornecedor']; }
				    // '' - nao iniciado, 0 - reprovado, 1 - aprovado, 2 - pendente
				    if(count(array_keys($subs,'_0'))>0) 		$status_cadeia = 0;
				    elseif(count(array_keys($subs,'_2'))>0) 	$status_cadeia = 2; 
				    elseif(count(array_keys($subs,'_'))>0) 		$status_cadeia = '';
				    else 										$status_cadeia = 1;
				}else{
				    $status_cadeia = '';
				}
			?>
			<td align="center"><?php echo htmlentities($audi_resultado['_'.$status_cadeia], ENT_QUOTES, "ISO-8859-1"); ?></td>
			<td align="center"><?php echo convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.criacao'));?></td>
			<td align="center"><?php echo ((mysql_result($query,$i,'CC_Fr_Vinculo.confirmacao') != '')?convertSysDate(mysql_result($query,$i,'CC_Fr_Vinculo.confirmacao')):" --- "); ?></td>
			<td><?php echo htmlentities($arr_associados[mysql_result($query,$i,'CC_Fr.certificadora')], ENT_QUOTES, "ISO-8859-1");?></td>
		</tr>
			

				<?php
				if(mysql_num_rows($query2) > 0){
					for($i2=0;mysql_num_rows($query2) > $i2;$i2++){
					?>
					<tr class="<?php echo(($i2%2==0)?"zebra-dark":"zebra-light"); ?> hover">
						<td>Subcontratado</td>
						<td><a href="fornecedor_dados.php?fr=<?php echo mysql_result($query,$i2,'CC_Fornecedor.fornecedor');?>&ac=cadastro" target="_blank"><?php echo mysql_result($query2,$i2,'CC_Fornecedor.fornecedor');?></a></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1");?></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1");?></td>
						<td><?php echo mascaras(mysql_result($query2,$i2,'CC_Fornecedor.CNPJ'),'##.###.###/####-##');?></td>
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.inscricao');?></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.resp_nome'), ENT_QUOTES, "ISO-8859-1");?></td>
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.resp_telefone');?></td>
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.resp_celular');?></td>
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.resp_email');?></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.rua'), ENT_QUOTES, "ISO-8859-1").','.mysql_result($query2,$i2,'numero');?></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.bairro'), ENT_QUOTES, "ISO-8859-1");?></td>
						<td><?php echo htmlentities(mysql_result($query2,$i2,'CC_Fornecedor.cidade'), ENT_QUOTES, "ISO-8859-1");?></td>
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.estado');?></td>
						<td><?php echo mysql_result($query2,$i2,'CC_Fornecedor.cep9');?></td>
						<td><?php echo $arr_certificadoras[mysql_result($query2,$i2,'nomeFantasiaCertificadora')]; ?></td>
						<td align="center"><?php echo ((mysql_result($query2,$i2,'CC_Fr.data_certificacao') != '')?convertSysDate(mysql_result($query2,$i2,'CC_Fr.data_certificacao')):" --- ");?></td>
						<td align="center"><?php echo htmlentities($audi_resultado['_'.mysql_result($query2,$i2,'CC_Fr.status_fornecedor')], ENT_QUOTES, "ISO-8859-1"); ?></td>
						<td align="center"><?php echo htmlentities($audi_resultado['_'.mysql_result($query2,$i2,'CC_Fr.status_certificacao')], ENT_QUOTES, "ISO-8859-1"); ?></td>
						<td align="center">&nbsp;</td>
						<td align="center"><?php echo convertSysDate(mysql_result($query2,$i2,'CC_Fr_Vinculo.criacao'));?></td>
						<td align="center"><?php echo ((mysql_result($query2,$i2,'CC_Fr_Vinculo.confirmacao') != '')?convertSysDate(mysql_result($query2,$i2,'CC_Fr_Vinculo.confirmacao')):" --- "); ?></td>
						<td align="center">&nbsp;</td>
					</tr>
	
				<?php
					}
				}
				?>

		
		
		
	<?php
		}
	}
	?>
	</table>
	
<?php 
	}//Fim Cadeia
		
		
		
		//--------	
	?>
	
	
	
	
	
	
	
	
	
	
<?php } ?>
	
		