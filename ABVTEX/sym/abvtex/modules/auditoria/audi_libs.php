<?PHP
	$ParametroTiposAuditorias = array();
	//matriz 1 Tipo da �ltima Auditoria
	//matriz 2 Status Atual do Fornecedor
	//matriz 3 Resultado �ltima Auditoria
	
	//Caso de Certifica��o
	$ParametroTiposAuditorias[0][0][0]['tipo'] = 0;
	$ParametroTiposAuditorias[0][1][1]['tipo'] = 1;
	$ParametroTiposAuditorias[0][2][2]['tipo'] = 2;
	$ParametroTiposAuditorias[0][6][1]['tipo'] = 1;
	$ParametroTiposAuditorias[0][6][2]['tipo'] = 2;
	
	//Caso de Manuten��o
	$ParametroTiposAuditorias[1][0][0]['tipo'] = 5;
	$ParametroTiposAuditorias[1][1][1]['tipo'] = 5;
	$ParametroTiposAuditorias[1][2][2]['tipo'] = 3;
	$ParametroTiposAuditorias[1][6][1]['tipo'] = 5;
	$ParametroTiposAuditorias[1][6][2]['tipo'] = 3;
	
	//Caso de Certifica��o: Auditoria de Plano de A��o
	$ParametroTiposAuditorias[2][0][0]['tipo'] = 0;
	$ParametroTiposAuditorias[2][1][1]['tipo'] = 1;
	$ParametroTiposAuditorias[2][2][2]['tipo'] = 2;
	$ParametroTiposAuditorias[2][6][1]['tipo'] = 1;
	$ParametroTiposAuditorias[2][6][2]['tipo'] = 2;
	
	//Caso de Manuten��o: Auditoria de Plano de A��o
	$ParametroTiposAuditorias[3][0][0]['tipo'] = 5;
	$ParametroTiposAuditorias[3][1][1]['tipo'] = 5;
	$ParametroTiposAuditorias[3][2][2]['tipo'] = 3;
	$ParametroTiposAuditorias[3][6][1]['tipo'] = 5;
	$ParametroTiposAuditorias[3][6][2]['tipo'] = 3;
		
	//Caso de Recertifica��o
	$ParametroTiposAuditorias[5][0][0]['tipo'] = 5;
	$ParametroTiposAuditorias[5][1][1]['tipo'] = 1;
	$ParametroTiposAuditorias[5][2][2]['tipo'] = 6;
	$ParametroTiposAuditorias[5][6][1]['tipo'] = 1;
	$ParametroTiposAuditorias[5][6][2]['tipo'] = 6;
	
	//Caso de Recertifica��o: Auditoria de Plano de A��o
	$ParametroTiposAuditorias[6][0][0]['tipo'] = 5;
	$ParametroTiposAuditorias[6][1][1]['tipo'] = 1;
	$ParametroTiposAuditorias[6][2][2]['tipo'] = 6;
	$ParametroTiposAuditorias[6][6][1]['tipo'] = 1;
	$ParametroTiposAuditorias[6][6][2]['tipo'] = 6;
	
	$audi_tipo = array(
		0 => 'Certifica��o',
		1 => 'Manuten��o',
		2 => 'Certifica��o: Auditoria de Plano de A��o',
		3 => 'Manuten��o: Auditoria de Plano de A��o',
		4 => 'Suspens�o',
		5 => 'Recertifica��o',
		6 => 'Recertifica��o: Auditoria de Plano de A��o'
	);
		
	$audi_resultado = array(
		//"_"  => 'Aguardando Auditoria',
		"_"  => 'N�o Iniciado',
		"_0" => 'Reprovado',
		"_1" => 'Aprovado',
		"_2" => 'Pendente de plano de a��o',
		"_3" => 'Auditoria Agendada',
		"_4" => 'Aguardando Aprova��o',
		"_5" => 'Cancelado',
		"_6" => 'Suspenso'
	);
	
	$audi_certificacao = array(
		"_0" => 'N�o Certificado',
		"_1" => 'Certificado'
	);
	
	$audi_certificacao_cor = array(
		"_0" => '#D94F44',
		"_1" => '#54B026'
	);
	
	$audi_fornecedor_status = array(
		"_"  => 'Aguardando Auditoria',
		"_0" => 'Reprovado',
		"_1" => 'Aprovado',
		"_5" => 'Cancelado',
		"_6" => 'Suspenso'
	);
		
	$audi_resultado_cor = array(
		"_"  => '#B1B3AF',
		"_0" => '#D94F44',
		"_1" => '#54B026',
		"_2" => '#D1D100',
		"_3" => '#B1B3AF',
		"_4" => '#B1B3AF',
		"_5" => '#D94F44',
		"_6" => '#D94F44'
	);
	
	$audi_tipo_fornecedor = array(
		1 => 'Fornecedor',
		2 => 'Subcontratado'
	);
	
	//prazos para cada auditoria
		$prazo_audi_inicial      = 45;
		$prazo_audi_certificacao = 440;
		$prazo_audi_pendente     = 90;
		$prazo_audi_manutencao   = 440;
	
	
	$questaoRastreabilidade = '91';
?>