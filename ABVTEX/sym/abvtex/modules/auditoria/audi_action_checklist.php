<?PHP 
	/**
	* 
	* FA AS A��ES DE EDI��O DE CHECKLIST E MAIS USABILIDADES DE AUDITORIA
	*
	*/
	
	$modulo = intval(''.$_GET["mod"].$_POST["mod"]);
	
	$sql = "SELECT * 
			FROM Audi_Modulo m 
			JOIN Audi_Questao q ON m.modulo = q.modulo
			WHERE (aplicacao = 0 OR aplicacao = '".$auditoria["tipo_fornecedor"]."')
			".($modulo>0?" AND m.modulo = '".$modulo."' ":"")."
			ORDER BY m.ord, q.ord
	";
	$res_chklst = mysql_query($sql) or die(mysql_error());
	$checklist = array(); $quests_to_mods = array(); $num_questoes = 0;
	while($row = mysql_fetch_assoc($res_chklst))
	{
		$mod = $row["modulo"];
		$qst = $row["questao"];
		if(!isset($checklist[$mod])) 
			$checklist[$mod] = array(
				'nome'=>$row["nome"] , 
				'peso' => 0 , 
				'meta' => $row["meta"] , 
				'nao_atingida' => $row["nao_atingida"] , 
				'compile' => $row["compile"], 
				'questoes' => array(),
				'num_questoes' => 0,
				'num_questoes_preenchidas' => 0
				);
		
		$checklist[$mod]['peso'] += $row["peso"];
		$checklist[$mod]['num_questoes']++;
		$checklist[$mod]['questoes'][$qst] = array(
			'label' => $row["label"],
			'texto' => $row["texto"],
			'evidencia' => $row["evidencia"],
			'criterio' => $row["criterio"],
			'peso' => $row["peso"],
			'label_pai' => $row["label_pai"],
			'texto_pai' => $row["texto_pai"]
			);
		$quests_to_mods[$qst] = $mod;
		$num_questoes++;
	}
	
	if($editar)
	{
		if($action2=="save" && $modulo>0) // S A L V A R   M O D U L O - - S A L V A R   M O D U L O - - S A L V A R   M O D U L O - - S A L V A R   M O D U L O - - S A L V A R   M O D U L O - - 
		{
			//apaga o que tem nas pergunas anteriores
			$qsts = array_keys($checklist[$modulo]['questoes']);
			$sql_del = "DELETE FROM Audi_Resposta WHERE audi = '".$audi."' AND questao IN ('".implode("','",$qsts)."') LIMIT ".sizeof($qsts)." ";
			
			//query de inser��o
			$sql = "INSERT INTO Audi_Resposta (audi,questao,atende,observacao,baseLegal) VALUES ";
			$cont = 0;
			$pontuacao = 0;
			$pontuacao_maxima = 0;
			foreach($qsts as $qst) //pegando resultado dos forms
			{
				$atende = "";
				$sel = tratarVariaveis($_POST["qst_sel_".$qst]);
				$txt = tratarVariaveis($_POST["qst_txt_".$qst]);
				
				$baseLegal = tratarVariaveis($_POST["base_legal_".$qst]);
				if("".$sel!="") //se tiver selecionado uma resposta ele guarda o resultado
				{
					$atende = ($sel=="NA"?"NULL":"'".intval($sel)."'"); //NA = NULL no banco de dados
					$sql .= " ('".$audi."','".$qst."',".$atende.",'".$txt."', '".$baseLegal.$teste."') , ";
					$cont++;
				}
				if("".$sel!="NA") //se n�o for NA ele contabiliza a quest�o
				{
					$pontuacao_maxima += 2*$checklist[$modulo]['questoes'][$qst]['peso'];
					$pontuacao += intval($sel)*$checklist[$modulo]['questoes'][$qst]['peso'];
				}
			}
			if($cont>0) // se tiver alguma coisa para isnerir
			{
				mysql_query($sql_del) or die($sql_del.' '.mysql_error());
				mysql_query(substr($sql,0,-2)) or die($sql.' '.mysql_error());
				
				//calcula e salva a nota do modulo
				if($pontuacao_maxima>0) $nota = 100*$pontuacao/$pontuacao_maxima;
				else                    $nota = 100;
				
				if($nota>=$checklist[$modulo]['meta']) $resultado = 'Aprovado';
				else                                   $resultado = $checklist[$modulo]['nao_atingida'];
				
				$sql = "REPLACE INTO Audi_Resposta_Modulo (audi,modulo,pontuacao,nota,resultado)
						VALUES 
						('".$audi."','".$modulo."','".$pontuacao."','".$nota."','".$resultado."') ";
				mysql_query($sql) or die($sql.' '.mysql_error());
				
				addLog(102,"frid:".$frid.";audi:".$audi.";modulo:".$modulo.";", $frid);
			}
		}
		else if($action2=="encerrar")// E N C E R R A R   A U D I T O R I A - - E N C E R R A R   A U D I T O R I A - - E N C E R R A R   A U D I T O R I A - - 
		{
			
			$sqlForn = "SELECT * FROM CC_Fornecedor
						JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
					WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND CC_Fr.fornecedor = '".$frid."'";
			$queryForn = mysql_query($sqlForn);
			
			//pegando cada resposta
				$sql = "SELECT * 
						FROM Audi_Resposta 
						WHERE audi = '".$audi."'
						ORDER BY questao ";
				$res_audirsp = mysql_query($sql) or die(mysql_error());
				$respostas = array(); $num_questoes_preenchidas = 0;
				while($row = mysql_fetch_assoc($res_audirsp))
				{
					$qst = $row["questao"];
					$mod = $quests_to_mods[$qst];
					$respostas[$qst] = array('atende' => $row["atende"] , 'observacao' => $row["observacao"] , 'referencia' => $row["referencia"] );
					$checklist[$mod]['num_questoes_preenchidas']++;
					if("".$row["atende"]=="") // se atender for NULL (ou seja: N/A) entao removemos o peso do modulo
					{
						$respostas[$qst]['atende'] = 'NA';
						$checklist[$mod]['peso'] -= $checklist[$mod]['questoes'][$qst]['peso'];
					}
					$num_questoes_preenchidas++;
				}
				mysql_free_result($res_audirsp);
			
			//pegando cada resposta/modulo
				$sql = "SELECT * 
						FROM Audi_Resposta_Modulo 
						WHERE audi = '".$audi."'
						ORDER BY modulo
						";
				$res_audirsp = mysql_query($sql) or die(mysql_error());
				$respostas_modulo = array();
				while($row = mysql_fetch_assoc($res_audirsp))
				{
					$mod = $row["modulo"];
					$respostas_modulo[$mod] = array('pontuacao' => $row["pontuacao"] , 'nota' => $row["nota"] , 'resultado' => $row["resultado"] );
				}
				mysql_free_result($res_audirsp);
			
			//efetuando os calculos
				$i=0; $peso_total=0; $pontuacao_total=0; $resultado_final = "--";
				foreach($checklist as $mod => &$estrutura)
				{
					if("".$respostas_modulo[$mod]["resultado"]=="") { $resultado = "--"; }
					else
					{
						$resultado = $respostas_modulo[$mod]["resultado"];
						
						if($respostas_modulo[$mod]["resultado"]=="Aprovado")      { if($resultado_final=="--") $resultado_final = "Aprovado"; }
						elseif($respostas_modulo[$mod]["resultado"]=="Reprovado") $resultado_final = "Reprovado";
						elseif($resultado_final!="Reprovado"){
							//Se pendente de plano de a��o em Plano de a��o - reprovar
							/* if(($auditoria["tipo"] == 2) || ($auditoria["tipo"] == 3)){
		    					$resultado_final = "Reprovado";
		    				}else{ */
		    					$resultado_final = "Pendente de plano de a��o";
		    				//}
						}
					}
					
					if($estrutura["compile"]==1) { $peso_total += 2*$estrutura["peso"]; $pontuacao_total += $respostas_modulo[$mod]["pontuacao"]; }
				}
			
			//calculando o status e resultados
				$nota_final = 100*$pontuacao_total/$peso_total;
				
				if($nota_final<70 && $resultado_final=="Aprovado"){
					//Se pendente de plano de a��o em Plano de a��o - reprovar
					/* if(($auditoria["tipo"] == 2) || ($auditoria["tipo"] == 3)){
						$resultado_final = "Reprovado";
					}else{ */
						$resultado_final = "Pendente de plano de a��o";
					//}
				}
					
				
				$res = '';
				foreach($audi_resultado as $value => $str)
				{
					if(''.$str==''.$resultado_final) { $res = str_replace("_","",$value); break; }
				}
			
			//editando na tabela final de auditorias
				//status = 1 = Fechada
				$hoje_aprova = date("Y-m-d");
				$sql = "UPDATE Audi_Fornecedor 
						SET resultado = '".$res."', 
						nota = '".$nota_final."', 
						aprovacao = '".$hoje_aprova."', 
						status = 1                        
						WHERE empresa = '".$empresa."' 
						AND audi = '".$audi."' 
						AND certificadora = '".$cert."' 
						LIMIT 1 ";
				mysql_query($sql) or die(mysql_error());
			
			//adicionando no log
				//addLog(103,"frid:".$frid.";audi:".$audi.";");
				addLog(103,"frid:".$frid.";audi:".$audi.";status:".$audi_resultado['_'.$res].";", $frid);
				addLog(20,"frid:".$frid.";audi:".$audi.";status:".$audi_resultado['_'.$res].";", $frid);
				//addLogNew('103', $frid, 'audi:'. $audi .' - '.$audi_resultado['_'.$res], $frid);
				//addLogNew('20', $frid, 'audi:'. $audi .' - '.$audi_resultado['_'.$res], $frid);
			
			//editar o status do fornecedor
				$update_data_sql = "";
			$novo_certificado = 0;
			//se ele foi aprovado
				if("".$res=="1" || (("".$auditoria["tipo"]=="5") && "".$res=="2")) 
				{
					// Certificacao, Certificacao:Plano de a��o, Recertifica��o, Recertifica��o: plano de a��o
					if( ("".$auditoria["tipo"]=="0") || ("".$auditoria["tipo"]=="2") || ("".$auditoria["tipo"]=="5") || ("".$auditoria["tipo"]=="6") ){
						$update_data_sql = ", data_certificacao = '".$auditoria["data"]."' "; //se j� � a cetificacao
						$novo_certificado = 1;
					}
					// elseif("".$auditoria["tipo"]=="1") //se for a Manuten��o
					// {
						// $sql = "SELECT data FROM Audi_Fornecedor WHERE empresa = '".$empresa."' AND fornecedor = '".$frid."' AND status = 1 AND tipo in (0,2,5,6) ORDER BY data DESC LIMIT 1 ";
						// $res_aux_audi = mysql_query($sql) or die(mysql_error());
						// if(mysql_num_rows($res_aux_audi)>0) $update_data_sql = ", data_certificacao = '".mysql_result($res_aux_audi,0,"data")."' ";
					// }
				}
			//sen�o reprova e remove a certificacao
				elseif("".$res=="0") $update_data_sql = ", data_certificacao = NULL "; 
			
			
			//Atualiza status da certifica��o
			$sql = "UPDATE CC_Fr SET status_certificacao = '".$res."' ".$update_data_sql." WHERE empresa = '".$empresa."' AND fornecedor = '".$frid."' LIMIT 1 ";
			mysql_query($sql) or die(mysql_error());
			
			
			if($res == '1' || $res === '0' || $res == '6'){
				//Atualiza status da certifica��o
				$sql = "UPDATE CC_Fr SET status_fornecedor = '".$res."' WHERE empresa = '".$empresa."' AND fornecedor = '".$frid."' LIMIT 1 ";
				mysql_query($sql) or die( mysql_error() );
			}elseif((("".$auditoria["tipo"]=="5" || "".$auditoria["tipo"]=="6") && "".$res=="2")){
				//Atualiza status da certifica��o recertifica��o pendente de plano de a��o
				$sql = "UPDATE CC_Fr SET status_fornecedor = '1' WHERE empresa = '".$empresa."' AND fornecedor = '".$frid."' LIMIT 1 ";
				mysql_query($sql) or die( mysql_error() );
			}elseif((("".$auditoria["tipo"]=="1" || "".$auditoria["tipo"]=="3") && "".$res=="2")){
				//Atualiza status da certifica��o em caso de manuten��o pendente de plano de a��o
				$sql = "UPDATE CC_Fr SET status_fornecedor = '1' WHERE empresa = '".$empresa."' AND fornecedor = '".$frid."' LIMIT 1 ";
				mysql_query($sql) or die( mysql_error() );
			}
			
			//----------------------------------------------------------
			
			
			
			
			$sql = "SELECT * FROM CC_Fornecedor 
						JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
						JOIN Audi_Fornecedor ON Audi_Fornecedor.fornecedor = CC_Fornecedor.fornecedor AND Audi_Fornecedor.empresa = '".$empresa."' AND Audi_Fornecedor.audi = '".$audi."'
						JOIN CC_Empresa_Dados ON CC_Empresa_Dados.empresa = Audi_Fornecedor.certificadora
					WHERE CC_Fr.empresa = '".$empresa."'";
			$query = mysql_query($sql) or die(mysql_error());
			
			
			switch($auditoria["tipo"]){
				case '1': $anos = '1'; break;
				case '0': $anos = '2'; break;
				case '4': $anos = '2'; break;
			}
			
			
			//Cria Certificado --------------------------------------------------------
			
			if($novo_certificado == 1){ // Se for certificado
				$nomeEmpresa = mysql_result($query,0,'CC_Fornecedor.razaoSocial');
				$endereco = mysql_result($query,0,'CC_Fornecedor.rua')." - CEP: ".mysql_result($query,0,'CC_Fornecedor.cep9')." - ".mysql_result($query,0,'CC_Fornecedor.cidade')." - ".mysql_result($query,0,'CC_Fornecedor.estado')." - ".mysql_result($query,0,'CC_Fornecedor.pais');
				$nomeCertificadora = mysql_result($query,0,'CC_Empresa_Dados.razaoSocial');
				$dataAuditoria = mysql_result($query,0,'Audi_Fornecedor.data');
				$status = $audi_resultado['_'.mysql_result($query,0,'Audi_Fornecedor.resultado')];
				$dataCertificacao = mysql_result($query,0,'CC_Fr.data_certificacao');
				
				if($dataCertificacao == '') $dataCertificacao = $dataAuditoria;
				
				$sql = "INSERT INTO Audi_Certificado (empresa, certificadora, fornecedor, audi, nomeEmpresa, endereco, nomeCertificadora, dataAuditoria, status, dataCertificacao) VALUES ('".$empresa."','".$cert."','".$frid."','".$audi."', '".$nomeEmpresa."','".$endereco."','".$nomeCertificadora."','".$dataAuditoria."','".$status."','".$dataCertificacao."')";
				mysql_query($sql) or die('Audi_Certificado - '.mysql_error());
				
				
				mysql_query("UPDATE CC_Fr SET data_certificacao = '".$dataCertificacao."' WHERE empresa = '".$empresa."' AND fornecedor = '".$frid."'") or die( mysql_error() );
				
				
				//proximos passos: mandar email aos interessados
				
		    	$nome = mysql_result($query,0,'CC_Fornecedor.resp_nome');
		    	$email = mysql_result($query,0,'CC_Fornecedor.resp_email');
		    	
		    	$assunto = 'Fornecedor - '.$frid.': Certificado';
		    	$texto = '
		    	    	 	Parab�ns! Voc� foi certificado pelo organismo '.$nomeCertificadora.', no '.$systemName.'.
		    	    	 	<br/><br/>
		    	    	 	Voc� est� certificado, a partir de ('.convertSysDate($dataCertificacao).') e sua certifica��o tem validade de 2 ano(s).
		    	    	 	<br/><br/>
		    	    	 	Voc� poder� ter acesso ao seu certificado no sistema, e aos prazos das demais auditorias.
		    	    	 	<br><br>
							Acesso ao certificado:<br>
							<a href="https://www.techsocial.com.br/sym/abvtex/index.php?lc=crtfd&pg=auditoria&ac=resumo&frid='.$frid.'&audi='.$audi.'">Clique aqui para acessar a pesquisa.</a>
		    	    	 	
		    	    	 ';
		    	    	  
		    	$assinatura = 'Atenciosamente';
	
		    	$corpo = LoadLayout('padrao');
		    	$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		
		    	sendMail($nome, $email,$corpo, $assunto);
				sendMail($nome . "(" . $email . ")", "fabio@keyassociados.com.br", $corpo, $assunto);
		    	
		    	// ---------------------------------------------------------------------
		    	
		    	$i = 0;
		    	$sql = "
		    		SELECT * 
		    		FROM CC_Fr_Vinculo 
			    		JOIN CC_Empresa ON CC_Empresa.empresa = CC_Fr_Vinculo.empresa
			    		JOIN CC_Empresa_Dados ON CC_Empresa.empresa = CC_Empresa_Dados.empresa
			    		JOIN CC_Associacao ON CC_Associacao.associado = CC_Empresa.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
			    		JOIN CC_Fornecedor ON CC_Fr_Vinculo.fornecedor = CC_Fornecedor.fornecedor
		    		WHERE CC_Fr_Vinculo.fornecedor = '".$frid."'
					AND CC_Fr_Vinculo.ativo = 1 
		    		"; // fix para email enviado para certificadora antiga *** AND CC_Fr_Vinculo.ativo = 1 ***
		    	$query = mysql_query($sql) or die(mysql_error());
		    	while($values = mysql_fetch_array($query)){
		    		
		    		
		    		$sqlCont = "SELECT * FROM CC_Usuario WHERE empresa = '".mysql_result($query, $i, 'CC_Empresa_Dados.empresa')."' AND notificacoes = '1'";
		    		$queryCont = mysql_query($sqlCont) or die(mysql_error());
		    		if(mysql_num_rows($queryCont) > 0){
		    			$a = 0;
		    			while($valuesCont = mysql_fetch_array($queryCont)){
		    			
		    				if(!(strripos($valuesCont['tipos_notificacoes'], ';20;') === false)){
								//Envia email
								$assunto = 'Fornecedor - '.$frid.': Certificado';
							
								$texto = '
									O Fornecedor '.$nomeEmpresa.' ('.$frid.') foi certificado pelo organismo '.$nomeCertificadora.', no '.$systemName.'.
									<br/><br/>
									E est� certificado, a partir da data ('.convertSysDate($dataCertificacao).') e sua certifica��o tem validade de 2 ano(s).
								 ';
					
								$assinatura = 'Atenciosamente';
							
								$corpo = LoadLayout('padrao');
								$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
							
								$nome = mysql_result($queryCont, $a, 'CC_Usuario.nome');
								$email =mysql_result($queryCont, $a, 'CC_Usuario.email');
							
								sendMail($nome, $email,$corpo, $assunto);
								sendMail($nome . "(" . $email . ")", "fabio@keyassociados.com.br", $corpo, $assunto);

			    			}
			    			$a++;
			    		}
		    		}
		    		
		    		$i++;
		    	}
		    	
				
				// ----- Fim Emails -----------
				
				// -- Pesquisa -- 
					include_once("audi_action_pesquisa.php");
				// -- Pesquisa -- 
				
				
			}//Fim Aprovado
			else
			{ // Se for certificado
				$nomeEmpresa = mysql_result($query,0,'CC_Fornecedor.razaoSocial');
				$endereco = mysql_result($query,0,'CC_Fornecedor.rua')." - CEP: ".mysql_result($query,0,'CC_Fornecedor.cep9')." - ".mysql_result($query,0,'CC_Fornecedor.cidade')." - ".mysql_result($query,0,'CC_Fornecedor.estado')." - ".mysql_result($query,0,'CC_Fornecedor.pais');
				$nomeCertificadora = mysql_result($query,0,'CC_Empresa_Dados.razaoSocial');
				$dataAuditoria = mysql_result($query,0,'Audi_Fornecedor.data');
				$status = $audi_resultado['_'.$res];
				
				
				
				
				//proximos passos: mandar email aos interessados
				
		    	$nome = mysql_result($query,0,'CC_Fornecedor.resp_nome');
		    	$email = mysql_result($query,0,'CC_Fornecedor.resp_email');
		    	
		    	$assunto = 'Fornecedor - '.$frid.': '.$status;
		    	$texto = '
		    	    	 	Voc� teve seu status alterado pelo organismo '.$nomeCertificadora.', no '.$systemName.'.
		    	    	 	<br/><br/>
		    	    	 	Status: '.$status.'
		    	    	 ';
		    	    	  
		    	$assinatura = 'Atenciosamente';
	
		    	$corpo = LoadLayout('padrao');
		    	$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		
		    	sendMail($nome, $email,$corpo, $assunto);
				sendMail($nome . "(" . $email . ")", "fabio@keyassociados.com.br", $corpo, $assunto);
		    	
		    	// ---------------------------------------------------------------------
		    	
		    	$i = 0;
		    	$sql = "
		    		SELECT * 
		    		FROM CC_Fr_Vinculo 
			    		JOIN CC_Empresa ON CC_Empresa.empresa = CC_Fr_Vinculo.empresa
			    		JOIN CC_Empresa_Dados ON CC_Empresa.empresa = CC_Empresa_Dados.empresa
			    		JOIN CC_Associacao ON CC_Associacao.associado = CC_Empresa.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
			    		JOIN CC_Fornecedor ON CC_Fr_Vinculo.fornecedor = CC_Fornecedor.fornecedor
		    		WHERE CC_Fr_Vinculo.fornecedor = '".$frid."'
					AND CC_Fr_Vinculo.ativo = 1
		    		"; // fix para email enviado para certificadora antiga *** AND CC_Fr_Vinculo.ativo = 1 ***
		    	$query = mysql_query($sql) or die(mysql_error());
		    	while($values = mysql_fetch_array($query)){
		    		
		    		
		    		$sqlCont = "SELECT * FROM CC_Usuario WHERE empresa = '".mysql_result($query, $i, 'CC_Empresa_Dados.empresa')."' AND notificacoes = '1' ";
		    		$queryCont = mysql_query($sqlCont) or die(mysql_error());
		    		if(mysql_num_rows($queryCont) > 0){
		    			$a = 0;
		    			while($valuesCont = mysql_fetch_array($queryCont)){
		    				if(!(strripos($valuesCont['tipos_notificacoes'], ';20;') === false)){
								//Envia email
								$assunto = 'Fornecedor - '.$frid.': Certificado';
							
								$texto = '
									O Fornecedor '.$nomeEmpresa.' ('.$frid.') foi certificado pelo organismo '.$nomeCertificadora.', no '.$systemName.'.
									<br/><br/>
									E est� certificado, a partir da data ('.convertSysDate($dataCertificacao).') e sua certifica��o tem validade de 2 ano(s).
								 ';
					
								$assinatura = 'Atenciosamente';
							
								$corpo = LoadLayout('padrao');
								$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
							
								$nome = mysql_result($queryCont, $a, 'CC_Usuario.nome');
								$email =mysql_result($queryCont, $a, 'CC_Usuario.email');
							
								sendMail($nome, $email,$corpo, $assunto);
								sendMail($nome . "(" . $email . ")", "fabio@keyassociados.com.br", $corpo, $assunto);

			    			}
			    			$a++;
			    		}
		    		}
		    		
		    		$i++;
		    	}
		    	
				
				// ----- Fim Emails -----------
				
				
			}//Fim outros status
			
			 
			
			// -------------------------------------------------------------------------------------
			
		}
		elseif($action2=="aprovacao"){
			
			$sql = "UPDATE Audi_Fornecedor 
						SET resultado = '4' 
					WHERE empresa = '".$_COOKIE['empresa']."' 
					AND fornecedor = '".$frid."' 
					AND audi = '".$audi."' 
					AND certificadora = '".$_COOKIE['empr']."'
				";
			$query = mysql_query($sql) or die(mysql_error());
			
			
			
			$sql = "
				SELECT * FROM CC_Fornecedor 
					JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
    				JOIN Audi_Fornecedor ON Audi_Fornecedor.fornecedor = CC_Fornecedor.fornecedor AND Audi_Fornecedor.empresa = '".$empresa."' AND Audi_Fornecedor.audi = '".$audi."'
    				JOIN CC_Empresa_Dados ON CC_Empresa_Dados.empresa = Audi_Fornecedor.certificadora
    			WHERE CC_Fr.empresa = '".$empresa."'
    			";
			$query = mysql_query($sql);
			
			$nomeEmpresa = mysql_result($query,0,'CC_Fornecedor.razaoSocial');
			$fornecedor = mysql_result($query,0,'CC_Fornecedor.fornecedor');
			
	    	$i = 0;
	    	$sql = "
	    		SELECT * 
	    		FROM CC_Fr_Vinculo 
		    		JOIN CC_Empresa ON CC_Empresa.empresa = CC_Fr_Vinculo.empresa
		    		JOIN CC_Empresa_Dados ON CC_Empresa.empresa = CC_Empresa_Dados.empresa
		    		JOIN CC_Associacao ON CC_Associacao.associado = CC_Empresa.empresa AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
		    		JOIN CC_Fornecedor ON CC_Fr_Vinculo.fornecedor = CC_Fornecedor.fornecedor
	    		WHERE CC_Fr_Vinculo.fornecedor = '".$frid."' AND CC_Associacao.associado = '".$_COOKIE['empr']."' AND CC_Fr_Vinculo.ativo = 1"; 
				// fix para email enviado para certificadora antiga *** AND CC_Fr_Vinculo.ativo = 1 ***
	    	$query = mysql_query($sql) or die(mysql_error());
	    	while($values = mysql_fetch_array($query)){
	    		
	    		
	    		$sqlCont = "SELECT * FROM CC_Usuario WHERE empresa = '".mysql_result($query, $i, 'CC_Empresa_Dados.empresa')."' AND CC_Usuario.nivel = '1' AND notificacoes = '1' ";
	    		$queryCont = mysql_query($sqlCont) or die(mysql_error());
	    		if(mysql_num_rows($queryCont) > 0){
	    			$a = 0;
	    			while($valuesCont = mysql_fetch_array($queryCont)){
		    			//Envia email
		    			$assunto = 'Aguardando Aprova��o';
		    			
		    			$texto = '
		    				A auditoria do fornecedor - '.$frid.', est� aguardando aprova��o.
		    	    	 	
		    	    	 ';
				
		    			$assinatura = 'Atenciosamente';
						
		    			$corpo = LoadLayout('padrao');
		    			$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		    			
		    			$nome = mysql_result($queryCont, $a, 'CC_Usuario.nome');
		    			$email = mysql_result($queryCont, $a, 'CC_Usuario.email');
		    			
		    			sendMail($nome, $email,$corpo, $assunto);	
						sendMail($nome . "(" . $email . ")", "fabio@keyassociados.com.br", $corpo, $assunto);
		    			$a++;
		    		}
	    		}
	    		
	    		$i++;
	    	}
			
			// ----- Fim Emails -----------
			
			
			
		}
	}

	header('Location: ?ac='.$action.'&audi='.$audi.'&frid='.$frid.'&t='.time());
	exit(0);
?>