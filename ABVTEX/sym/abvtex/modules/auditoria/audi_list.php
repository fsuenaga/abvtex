<?PHP
$sql = "
	SELECT
		audi_fornecedor.empresa,
		audi_fornecedor.fornecedor,
		audi_fornecedor.audi,
		audi_fornecedor.checklist,
		audi_fornecedor.auditor,
		audi_fornecedor.`data`,
		audi_fornecedor.tipo,
		audi_fornecedor.tipo_fornecedor,
		audi_fornecedor.resultado,
		audi_fornecedor.nota,
		audi_fornecedor.`status`,
		audi_fornecedor.motivo,
		audi_fornecedor.num_certificado,
		audi_fornecedor.pesquisa,
		audi_fornecedor.num_hd,
		audi_fornecedor.criado,
		audi_fornecedor.aprovacao,
		cc_empresa_dados.razaoSocial as certificadora
	FROM
		audi_fornecedor
	JOIN cc_fr_vinculo ON cc_fr_vinculo.fornecedor = audi_fornecedor.fornecedor AND cc_fr_vinculo.empresaTipo = 'Certificadora' AND cc_fr_vinculo.ativo = '1'
	INNER JOIN cc_empresa_dados ON cc_empresa_dados.empresa = audi_fornecedor.certificadora
	WHERE
		Audi_Fornecedor.fornecedor = '{$frid}'
	AND Audi_Fornecedor.empresa = '{$_COOKIE['empresa']}'
	AND CC_Fr_Vinculo.passo > 0
	".(($_COOKIE['perm'] == 'fornecedor')?" AND Audi_Fornecedor.status = '1' ":"")."
	ORDER BY
		Audi_Fornecedor.`data` DESC
";
		
echo "<!-- sql: ". $sql ." -->" ;
$auditorias = mysql_query($sql) or die(mysql_error());

$abrir_contrato = false;
if(mysql_num_rows($auditorias)<=0) $abrir_contrato = true;
else
{
	$ultima_data = mysql_result($auditorias,0,"data");
	$ultimo_tipo = mysql_result($auditorias,0,"tipo");
	$ultimo_res  = mysql_result($auditorias,0,"resultado");
	$today = date("Y-m-d");
	//if($ultimo_tipo==2 && "".$ultimo_res !== "") $abrir_contrato = true;
}

// se precisa contratar uma certificadora (rever logica please)
if($abrir_contrato && $perm=="fornecedor") {
	
	$passo = $CC_Fr["passo_contrato"];
?>

<script type="text/javascript">
	function alertaPasso(){
		alert('Antes de seguir para o pr�ximo passo voc� ter� 1 dia, para entrar em contato com os organismos de Certifica��o.\nO pr�ximo passo estar� dispon�vel em 1 dia.');
		return(false)
	}
</script>

<table width="100%" style="font-size: 13px;" cellpadding="2" cellspacing="10" border="0">
<colgroup>
	<col width="50%">
	<col width="50%">
</colgroup>
<tr><td colspan="2" align="center">
	<img src="images/certificadora_passos_<?=$passo ?>.png">
</td></tr>
<tr>
	<?PHP if($passo==0) { /* N�O FEZ NADA AINDA, MENSAGEM DE ENTRADA */ ?>
		<tr>
			<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
				Ol�,
				<br><br>
				Para continuar o processo de certifica��o � necess�rio 
				selecionar e contratar um organismo de certifica��o. Este 
				organismo ir� realizar sua auditoria e de sua cadeia de 
				fornecimento, dependendo do escopo.
				<br><br>
				Esta etapa se divide em 3 partes:

				<ol>
				<li><b>Contatar; </b><br>
					Inicialmente voc� precisa contatar os 
					organismos de certifica��o credenciados pela ABVTex 
					para receber e analisar as propostas referentes aos 
					valores das auditorias;
					<br>&nbsp;</li>
				<li><b>Selecionar: </b><br>
					Ap�s ter recebido e avaliado as propostas, e acordado 
					com o organismo de certifica��o desejado, voc� dever� 
					selecion�-lo pelo sistema. Voc� poder� selecionar 
					apenas um organismo, e o plano de auditoria ser� vigente 
					por dois anos;
					<br>&nbsp;</li>
				<li><b>Confirmar: </b><br>
					Esta etapa apresenta um resumo dos 
					processos acima e solicita uma confirma��o sua como fornecedor, 
					referente ao contrato, para que sejam iniciadas as atividades 
					de auditoria pelo organismo selecionado.
					</li>
				<br><br>
				<div class="botao_green" style="width:200px;text-align:center;float:right;"><a href="?ac=chgPasso&opt=1"><b>Pr�ximo Passo &gt;</b></a></div>
			</td>
		</tr>
	<?PHP } else if($passo==3) { /* J� SELECIONOU A CERTIFICADORA, MENSAGEM PARA CONFIRMAR OU VOLTAR */ 
		$sql = "SELECT empresa, status FROM CC_Fr_Vinculo 
				WHERE fornecedor = '".$frid."' 
				AND empresa IN (SELECT associado FROM CC_Associacao WHERE empresa = '".$_COOKIE["empresa"]."' AND tipo = 'Certificadora') AND ativo = '1' LIMIT 1  ";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res)>0)
		{
			$row = mysql_fetch_assoc($res);
			$sql = "SELECT * FROM CC_Empresa WHERE empresa = '".$row["empresa"]."' LIMIT 1 ";
			$res_cert = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res_cert)>0) $row_cert = mysql_fetch_assoc($res_cert);
		}
		else
		{
			//alguma coisa aconteceu, vamos voltar ao planejamento
			$sql = "UPDATE CC_Fr SET passo_contrato = '0' WHERE empresa = '".$_COOKIE["empresa"]."' AND fornecedor = '".$frid."' LIMIT 1 ";
			mysql_query($sql) or die(mysql_error());
			echo('<script>window.open("?t='.time().'","_self"); </script>');
		}
		
		$logo_image = 'logos/'.$row['empresa'].'_l';
				 if(file_exists($logo_image.'.png')) $logo_image .= '.png';
			else if(file_exists($logo_image.'.jpg')) $logo_image .= '.jpg';
			else $logo_image = 'images/blank.png';
		if("".$row["status"]=="0"){ 
		?>
		<tr>
			<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
				Organismo de certifica��o selecionada:
				<br>
				<div align="center" style="background-color:#EEEEEE;padding:5px;"><img src="<?=$logo_image ?>" style="width:80px;" align="absmiddle"> &nbsp;<span style="font-size:16px;"><?=$row_cert["razaoSocial"] ?></span></div>
				<br><br>
					O organismo de certifica��o <b><?=$row_cert["razaoSocial"] ?></b> foi selecionado por voc� para prestar os servi�os de auditoria em suas instala��es. 
					Por favor, se for este o organismo contratado, confirme esta infoma��o utilizando o bot�o <b>Confirmar</b> abaixo. 
					Caso n�o seja este o organismo de certifica��o desejado e acordado, uilize o bot�o <b>Cancelar</b> para retornar � tela anterior.<br><br>
					<span style="color:red;">Aten��o: </span> Lembre-se que ap�s esta confirma��o o processo de auditoria n�o poder� ser cancelado 
					durante um per�odo de dois anos.
				<br><br>
				<div class="botao_green" style="width:200px;text-align:center;float:right;"><a href="?ac=chgPasso&opt=4&crt=<?=$row["empresa"] ?>"><b>Confirmar &gt;</b></a></div>
				<div class="botao_red"   style="width:200px;text-align:center;">            <a href="?ac=chgPasso&opt=2&mod=cancel"><b>&lt; Cancelar </b></a></div>
			</td>
		</tr>
		<?PHP } else if("".$row["status"]=="1") { ?>
		<tr>
			<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
				Aguardando o organismo de certifica��o aceitar o v�nculo e iniciar a auditoria
			</td>
		</tr>
		<?PHP } else { ?>
		<tr>
			<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
				V�nculo criado: Aguardando o orgamismo de certifica��o realizar a auditoria
			</td>
		</tr>
		<?PHP } ?>
		
	<?PHP } 
		/* PASSO 1 E 2 - CONTATAR A CERTIFICADORA OU ESCOLHER UMA DELAS */
		/* PASSO 1 E 2 - CONTATAR A CERTIFICADORA OU ESCOLHER UMA DELAS */
		/* PASSO 1 E 2 - CONTATAR A CERTIFICADORA OU ESCOLHER UMA DELAS */
		else 
		{ ?>

		<?PHP if($passo==1) { ?>
				<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
					Nesta etapa voc� precisar� contatar os 
					organismos de certifica��o credenciados pela ABVTex 
					para receber e analisar as propostas referentes aos 
					valores das auditorias.
				</td>
			</tr><tr>
		<?PHP } else if($passo==2) { ?>
				<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
					Nesta Etapa, ap�s ter recebido e avaliado as propostas voc� j�
					deve estar decidido sobre o organismo de certifica��o desejado para
					auditar a sua empresa. Por favor, selecione abaixo qual a certificadora
					que ir� realizar as auditorias em suas instala��es. <br><br>
					<span style="color:red;">Aten��o: </span> Lembre-se que s� � poss�vel selecionar 
					um organismo de certifica��o, e o plano de auditoria ser� vigente por dois anos.
				</td>
			</tr><tr>
		<?PHP } ?>
	
	
	<?PHP
			$sql = "SELECT e.empresa, e.razaoSocial, e.representante, e.telefone, e.fax, e.email FROM CC_Empresa e 
			JOIN CC_Associacao a ON e.empresa = a.associado AND a.tipo = 'Certificadora' 
			WHERE a.empresa = '".$_COOKIE["empresa"]."' ORDER BY e.razaoSocial ";
			$certificadoras = mysql_query($sql) or die(mysql_error());
	
			$i=0; while($row = mysql_fetch_assoc($certificadoras)) {
			if($i%2==0&&$i>0) echo('</tr><tr>'); 
			$logo_image = 'logos/'.$row['empresa'].'_l';
				 if(file_exists($logo_image.'.png')) $logo_image .= '.png';
			else if(file_exists($logo_image.'.jpg')) $logo_image .= '.jpg';
			else $logo_image = 'images/blank.png';
			?>
			<td valign="top" class="framed_gray">
			<table cellpadding="2" border="0" style="font-size: 12px;" width="100%">
			<colgroup>
				<col width="85">
				<col width="100">
				<col>
			</colgroup>
			<tr><td valign="top" rowspan="6" align="center">
				<img src="<?=$logo_image ?>" style="width:80px;">
			</td><td colspan="2"><b><?=$row["razaoSocial"] ?></b></td></tr>
			<tr><td>Representante:</td><td><?=$row["representante"] ?></td></tr>
			<tr><td>Telefone:     </td><td><?=$row["telefone"] ?></td></tr>
			<tr><td>Site:         </td><td><?=$row["fax"] ?></td></tr>
			<tr><td>E-mail:       </td><td><?=$row["email"] ?></td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<?PHP if($passo==1) {
				// evento 101 - Contato do Fornecedor
				// se existir esse evento e completo for < 100
				$sql = "SELECT * FROM CC_Workflow 
						WHERE evento = '101' 
						AND empresa = '".$_COOKIE["empresa"]."' 
						AND chave = '".$frid."~".$row["empresa"]."' 
						AND completo < 100 LIMIT 1 ";
				$res = mysql_query($sql) or die(mysql_error());
				
				if(mysql_num_rows($res)>0)
				{
					$class = "botao_green_down";
					$frase = "E-mail enviado � certificadora";
				}
				else
				{
					$class = "botao_green";
					$frase = "Enviar um e-mail para esta cerificadora<br>pedindo uma proposta de auditoria";
				}
				?>
				<tr><td colspan="3" class="<?=$class ?>" align="center" style="cursor:pointer;" onClick="javascript: if(confirm('tem certeza que deseja contatar a certificadora <?=$row["razaoSocial"] ?>?')) window.open('?ac=contatar&opt=2&crt=<?=$row["empresa"] ?>','_self');"><b>
				<?=$frase ?>
				</b></td></tr>
			<?PHP } else if($passo==2) { ?>
			<tr><td colspan="3" class="botao_green" align="center" style="cursor:pointer;" onClick="javascript: if(confirm('tem certeza que deseja selecionar a certificadora <?=$row["razaoSocial"] ?>?')) window.open('?ac=chgPasso&opt=3&crt=<?=$row["empresa"] ?>','_self');">
				<b>Selecionar este<br>organismo de certfica��o</b>
				</td></tr>
			<?PHP } ?>
			</table>
			</td>
			<?PHP $i++; } ?>
		</tr>
		
		<?PHP if($passo==1) { 
		
			$sql = "SELECT * FROM CC_Fr WHERE empresa = '".$_COOKIE["empresa"]."' AND fornecedor = '".$frid."' LIMIT 1 ";
			$query = mysql_query($sql);
			if(($passo==1) && (mysql_result($query,0,'data_passo_contrato') == date('Y-m-d'))){
				//$mensagem = "javascript: return alertaPasso();";
			}
		?>
		<tr>
			<td valign="top" class="framed_gray" colspan="2" style="padding:20px;">
				Ap�s contatar os organismos de certifica��o voc� poder� selecionar no passo a seguir qual deles melhor atende �s suas expectativas. 
				Para isso siga at� o pr�ximo passo, clicando no bot�o abaio.
				<br><br>
				<div class="botao_green" style="width:200px;text-align:center;float:right;"><a href="?ac=chgPasso&opt=2" onclick="<?php echo $mensagem; ?>"><b>Pr�ximo Passo &gt;</b></a></div>
			</td>
		</tr>
		<?PHP } ?>
	<?PHP } ?>
</table>
<?PHP
}else{

// listar auditorias
if( (mysql_num_rows($auditorias)>0) && ($frid != '') ) { mysql_data_seek($auditorias,0);
?>
<table width="100%" style="font-size: 13px;" cellpadding="4" cellspacing="0">
	<colgroup>
	<col>
	<col>
	<col>
	<col width="210">
	<col>
	</colgroup>
	<tr class="tabela_titulos">
		<td>Auditoria</td>
		<td>Certificadora</td>
		<td>Data da Auditoria</td>
		<td>Resultado</td>
		<td>Nota</td>
	</tr>
	
	<?php echo "<!--".$certificadoras."-->";?>
	
	<?PHP $i=0; while($row = mysql_fetch_assoc($auditorias)) { ?>
		<tr class="<?=($i%2==0?"":"zebra-dark") ?>">
			<td><a href="auditoria.php?ac=resumo&frid=<?=$frid ?>&audi=<?=$row["audi"] ?>"><?=$audi_tipo[$row["tipo"]] ?></a></td>
			<td><?=$row["certificadora"] ?></td>
			<td><?=convertDateDB($row["data"]) ?></td>
			<td width="210" align="center" style="background-color:<?=$audi_resultado_cor["_".$row["resultado"]] ?>;color:white;"><b><?=$audi_resultado["_".$row["resultado"]] ?></b></td>
			<td align="right"><?=("".$row["nota"]!=""?number_format($row["nota"],2,",","."):"--") ?></td>
		</tr>
	<?PHP $i++; } ?>
</table>
<?PHP } } ?>