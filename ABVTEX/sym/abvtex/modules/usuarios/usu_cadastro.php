<?php
	
	$tipos = array(0 => 'Comum', 1=> 'Administrador', 2=> 'Auditor');
	
	if($_POST['action'] == 's'){
	
		$empresa = $_COOKIE['empr'];
		$pass = geraSenha(8, false, true);
		$nome = tratarVariaveis($_POST['nome']);
		$email = tratarVariaveis($_POST['email']);
		$telefone = tratarVariaveis($_POST['telefone']);
		$celular = tratarVariaveis($_POST['celular']);
		$tipo = tratarVariaveis($_POST['tipo']);
		$notificacoes = tratarVariaveis($_POST['notificacoes']);
		
		$sql = "SELECT * FROM CC_Usuario WHERE email = '".$email."'";
   		$query = mysql_query($sql);
		
		if(mysql_num_rows($query) > 0){
			echo '<script type="text/javascript">
					alert(\'Email j� cadastrado na base de dados. Por favor escolha um email diferente.\');
					history.back(-1);
				 </script>';
			exit(0);
		}
		
		//Usuario
   			$user = explode('@', $email);
   			$usuario_original = str_replace(".","",$user[0]); 
   			$a=0;
   			$userFound = 1;
   			$user = $usuario_original;
   			//Verifica se usu�rio existe
   			while($userFound == 1){
   				$sql = "SELECT * FROM CC_Usuario WHERE user = '".$user."'";
   				$query = mysql_query($sql);
   				if(mysql_num_rows($query) == 0){
   					$userFound = 0;
   				}else{
   					$user = $usuario_original.$a++;
   				}
   			}
		
		
		
		$sql = "INSERT INTO CC_Usuario (empresa, user, pass, nome, email, telefone, celular, nivel, notificacoes) VALUES ('".$empresa."','".$user."','".$pass."','".$nome."', '".$email."', '".$telefone."', '".$celular."', '".$tipo."', '".$notificacoes."')";
		$query = mysql_query($sql) or die(mysql_error());
		
		
		//Envia email
    	$senha = $pass;
    	$assunto = 'Dados de Acesso';
    	
    	$texto = '
    	    	Bem-vindo ao '.$systemName.'!
    	    	<br /><br />
    	    	Por favor, altere sua senha ap�s o primeiro acesso.
    	    	<br /><br />
    	    	<b>Dados de acesso:</b><br />
    	    	  <ul>
    	    		<li><b>Usu�rio:</b> '.$email.'</li>
    	    		<li><b>Senha:</b> '.$senha.'</li>
    	    	  </ul>';
    	    	  
    	$assinatura = 'Atenciosamente';

    	$corpo = LoadLayout('padrao');
    	$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
    		
    	sendMail($nome, $email,$corpo, $assunto);
		
		
		header('Location: usuarios.php?ac=listagem');
	}
?>


	<script type="text/javascript">
		function celularSP(id){
			if(((($('#' + id).val()).substring(0,4) == '(11)')  && ($('#' + id).val()).substring(0,6) == '(11) _') && ($('#' + id).hasClass('celular'))){
    			$('#' + id).removeClass('celular').addClass('celularSP');
    		
    			$(".celularSP").mask("(11) 99999-9999");
    		}
		}
		
				
		function usuarioExiste(campo){
		    $.post('pesquisa.php?funcao=verificarUsuario', { 
		        campo: campo,
		        valor: $('#' + campo).val()
    	    },
    	    function(resposta2) {
    	        if(resposta2 == false){
    	        	$('#'+campo+'_existe').val('1');
    	        	$('#'+campo+'_status').html('Usu�rio dispon�vel');
    	        }else{
    	        	$('#'+campo+'_existe').val('');
    	        	$('#'+campo+'_status').html('Usu�rio j� existe');
    	        }
    	        
    	    });
		}
	</script>

			<!-- CONTENT --> 
			
			<div class="mainTitle">
				Usu&aacute;rios
		    	
		    	<a href="usuarios.php?ac=listagem" style="float:right;"><img src="images/voltar.png" alt="Voltar"></a>
		    </div>
			
			
			<form style="width: 500px; padding:10px; border: 1px solid #000;" method="post" action="usuarios.php?ac=cadastro">
				<input type="hidden" name="action" value="s">
			
			    <ul style="margin: 0;">
			    	<li>Cadastrar novo Usu&aacute;rio</li>
			    	<li>&nbsp;</li>
			    
			    	<li class="zebra-dark"><label for="nome"><b>Nome</b></label></li>
			    	<li><input type="text" name="nome" id="nome" class="required" alt="Nome" style="width: 320px;" /></li>
			    	
			    	<li class="zebra-dark"><label for="email"><b>E-mail</b></label></li>
			    	<li>
			    		<input type="hidden" name="email_existe" id="email_existe" value="" class="required" alt="Email deve ser diferente dos existentes e" />
			    		<input type="text" name="email" id="email" class="required email" alt="Email" onchange="javascript:usuarioExiste('email')" style="width: 320px;" />
			    		<label id="email_status"></label>
			    	</li>
			    	
			    	<li class="zebra-dark"><label for="telefone"><b>Telefone</b></label></li>
			    	<li><input type="text" name="telefone" id="telefone" class="required telefone" alt="Telefone" /></li>
			    	
			    	<li class="zebra-dark"><label for="celular"><b>Celular</b></label></li>
			    	<li><input type="text" name="celular" id="celular" class="celular" onkeydown="javascript:celularSP('celular')" alt="Celular" /></li>
			    	
			    	<li class="zebra-dark"><label for="tipo"><b>Tipo</b></label></li>
			    	<li>
			    		<select name="tipo" id="tipo" class="" alt="Tipo">
			    			<option value="0">Comum</option>
			    			<option value="1">Administrador</option>
			    			<?php if($_COOKIE['perm'] == 'Certificadora'){ ?><option value="2">Auditor</option><?php } ?>
			    			<?php if($_COOKIE['perm'] == 'Varejista'){ ?><option value="3">Auditor de Varejista</option><?php } ?>
			    		</select>
			    	</li>
			    	
			    	<li class="zebra-dark"><label for="notificacoes"><b>Receber notifica&ccedil;&otilde;es</b></label></li>
			    	<li>
			    		<input type="radio" name="notificacoes" class="" value="0" />
			    		<label>N&atilde;o</label>
			    		<input type="radio" name="notificacoes" class="" value="1" />
			    		<label>Sim</label>
			    	</li>
			    	
			    	
			    	
			    	
			    	
			    	
			    	
			    	
			    	
			    	
			    	<li><input type="submit" value="Salvar" /></li>
			    </ul>
			</form>
			
			
			
			<!-- FIM CONTENT --> 

