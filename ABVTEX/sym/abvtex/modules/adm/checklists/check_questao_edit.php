<?php
	$sql = "SELECT * FROM Audi_Checklist WHERE checklist = '".$chk_id."' ORDER BY ativo DESC, nome ASC ";
	$res_check = mysql_query($sql) or die(mysql_error()); if(mysql_num_rows($res_check)<=0) die('erro / n�o encontrada checklist');
	$row_check = mysql_fetch_assoc($res_check);
	
	$sql = "SELECT * FROM Audi_Modulo WHERE checklist = '".$chk_id."' AND modulo = '".$modl."' ORDER BY ord, nome ASC ";
	$res_modl = mysql_query($sql) or die(mysql_error()); if(mysql_num_rows($res_modl)<=0) die('erro / n�o encontrado m�dulo');
	$row_modl = mysql_fetch_assoc($res_modl);
	
	$sql = "SELECT * FROM Audi_Questao WHERE modulo = '".$modl."' AND questao = '".$qust."' ORDER BY ord, label, texto ASC ";
	$res_qust = mysql_query($sql) or die(mysql_error()); if(mysql_num_rows($res_qust)<=0) die('erro / n�o encontrada quest�o');
	$row_qust = mysql_fetch_assoc($res_qust); 
?>
	<div class="mainTitle"><a href="?t=<?=time() ?>">Checklists do Sistema</a>
	<span style="color:#CCCCCC;">&nbsp;|&nbsp;</span>
	<a href="?ac=chkedt&ckid=<?=$chk_id ?>"><?=$row_check["nome"] ?></a>
	<span style="color:#CCCCCC;">&nbsp;|&nbsp;</span>
	<a href="?ac=chkmdledt&ckid=<?=$chk_id ?>&modl=<?=$modl ?>"><?=$row_modl["nome"] ?></a>
	<span style="color:#CCCCCC;">&nbsp;|&nbsp;</span>
	Editar quest�o</div>
	
	<table width="100%" cellpadding="10" cellspacing="10" style="font-size:12px;">
	<tr>
	<td valign="top" width="600" style="border:1px solid #A39F9F;">
		<form method="post" action="?mac=chkqstupd&ckid=<?=$chk_id ?>&modl=<?=$modl ?>&qust=<?=$qust ?>" style="">
		<ul>
			<li class="zebra-dark"><label for="label">Item</label></li>
			<li><input type="text" name="label" id="label" class="" style="width: 100px;text-align:center;" value="<?=$row_qust["label"] ?>" /></li>
			<li class="zebra-dark"><label for="texto">Texto da Quest�o</label></li>
			<li><textarea name="texto" id="texto" rows="4" style="width:100%;"><?=$row_qust["texto"] ?></textarea></li>
			<li class="zebra-dark"><label for="evidencia">Evid�ncia</label></li>
			<li><textarea name="evidencia" id="evidencia" rows="4" style="width:100%;"><?=$row_qust["evidencia"] ?></textarea></li>
			<li class="zebra-dark"><label for="evidencia">Crit�rio / Op��es</label></li>
			<li>
			<?PHP $c=0; foreach($criterio_arr as $criterio) { ?>
				<div><input type="radio" name="criterio" id="criterio<?=$c ?>" class="" value="<?=$criterio ?>" <?=($row_qust["criterio"]==$criterio?' checked':'') ?> /><label for="criterio<?=$c ?>"><?=str_replace("/"," / ",$criterio) ?> </label> </div>
			<?PHP $c++; } ?>
			</li>
			<li class="zebra-dark"><label for="peso">Peso</label></li>
			<li><input type="text" name="peso" id="peso" class="" style="width:100px;text-align:right;" value="<?=$row_qust["peso"] ?>" /></li>
			
			<li class="menu_sec">Item Superior - Caso a quest�o seja um sub-item de um item maior</li>
			<li class="zebra-dark"><label for="label_pai">Item Superior</label></li>
			<li><input type="text" name="label_pai" id="label_pai" class="" style="width:100px;text-align:center;" value="<?=$row_qust["label_pai"] ?>" /></li>
			<li class="zebra-dark"><label for="texto_pai">Texto do Item</label></li>
			<li><input type="text" name="texto_pai" id="texto_pai" class="" style="width:100px;text-align:center;" value="<?=$row_qust["texto_pai"] ?>" /></li>
			<li class="zebra-dark">Editar</li>
			<li>
				<input type="radio" name="editar_pai" id="editar_pai1" class="" value="0" /><label for="editar_pai1">Editar somente o item superior dessa quest�o</label> &nbsp;|&nbsp;
				<input type="radio" name="editar_pai" id="editar_pai2" class="" value="1" /><label for="editar_pai2">Editar todos com o mesmo nome</label>
			</li>
			
			<li class="menu_sec">Quest�o calculada pelo resultado da rastreabilidade</li>
			<li class="zebra-dark">Recebe o resultado da restrabilidade?</li>
			<li>
				<input type="radio" name="especial" id="especial1" class="" value="0" <?=($row_qust["especial"]==0?' checked':'') ?> /><label for="especial1">N�o </label> &nbsp;|&nbsp;
				<input type="radio" name="especial" id="especial2" class="" value="1" <?=($row_qust["especial"]==1?' checked':'') ?> /><label for="especial2">Sim </label>
			</li>
			
			<li><input type="submit" value="Salvar" style="float:right;" /></li>
		</ul>
		</form>
	</td>
	<td valign="top" style="border:1px solid #A39F9F;">
		<?php if($row_qust["ativo"]==1) { ?>
		<div class="red-alert" id="inat_but" onClick="triggerVisibility('inat_frm');" style="font-weight:bold;cursor:pointer;padding:8px;width:200px;display:inline-block;">
		Inativar quest�o &gt;
		</div>
		<?php } else { ?>
		<div class="green-ok"  id="inat_but" onClick="triggerVisibility('inat_frm');" style="font-weight:bold;cursor:pointer;padding:8px;width:200px;display:inline-block;">
		Reativar quest�o &gt;
		</div>
		<?php } ?>
		<form method="post" action="?mac=chkqstatv&ckid=<?=$chk_id ?>&modl=<?=$modl ?>&qust=<?=$qust ?>" style="">
		<input type="hidden" name="ativo" value="<?=$row_qust["ativo"] ?>">
		<ul id="inat_frm" style="display:none;margin-top:0px;">
			<li class="zebra-dark"><label for="texto_inat">Motivo para <?=($row_qust["ativo"]==1?'inativar':'reativar') ?> a quest�o</label></li>
			<li><textarea name="observacoes" id="texto_inat" rows="4" style="width:100%;"></textarea></li>
			<li class="zebra-dark">Voc� confirma a opera��o?</li>
			<li><input type="checkbox" name="confirmar" id="confirmar" value="1"><label for="confirmar"> Confirmar</label></li>
			<li><input type="submit" value="Salvar" style="float:right;" />
				<input type="button" value="Cancelar" style="float:left;" onClick="triggerVisibility('inat_frm');" /></li>
		</ul>
		</form>
		<br>&nbsp;
		<div style="margin:4px;padding:8px;color:#A39F9F;" align="center">Hist�rico de Altera��es</div>
		<?php
		$sql = "SELECT * FROM Audi_Questao_Historico WHERE questao = '".$qust."' ORDER BY datahora DESC ";
		$res_hists = mysql_query($sql) or die(mysql_error());
		while($row_hist = mysql_fetch_assoc($res_hists))
		{
			$datahora = explode(" ",$row_hist["datahora"]);
		?>
			<div style="margin:4px;padding:8px;border-top:1px solid #A39F9F;">
			<?php if($row_hist["ativo"]==1) { ?>
			<div class="red-alert" style="padding:2px;width:200px;font-style:italic;">&nbsp;<?=convertDateDB($datahora[0])." ".$datahora[1] ?> - Inativo: </div>
			<?php } else { ?>
			<div class="green-ok" style="padding:2px;width:200px;font-style:italic;">&nbsp;<?=convertDateDB($datahora[0])." ".$datahora[1] ?> - Ativo: </div>
			<?php } 
			echo(str_replace("\n","<br>",$row_hist["observacoes"])); ?>
			</div>
		<?php 
		} 
		?>
		<div style="margin:4px;padding:8px;border-top:1px solid #A39F9F;">
			<div class="blue-ok" style="padding:2px;width:200px;font-style:italic;">&nbsp;<?=$row_qust["criacao"] ?> - Entrada no sistema </div>
		</div>
	</td>
	</tr>
	</table>