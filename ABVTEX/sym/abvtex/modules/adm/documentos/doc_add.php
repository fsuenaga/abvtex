<?php
	$row_doc    = array();
	$arr_doccat = array();
?>
	<div class="mainTitle"><a href="?t=<?=time() ?>">Documentos do Sistema</a>
	<span style="color:#CCCCCC;">&nbsp;|&nbsp;</span>
	Inserir novo documento</div>
	
	
	<form method="post" action="?mac=docadd&doc=<?=$doc ?>" style="">
	<div style="font-size:12px;border:1px solid #A39F9F; padding:10px; width:600px; margin-bottom: 20px; display:inline-block;">
	<ul>
		<li class="zebra-dark"><label for="documento">Nome do Documento</label></li>
		<li><input type="text" name="documento" id="documento" style="width: 300px;" value="<?=$row_doc["documento"] ?>" /></li>
		<li class="zebra-dark"><label for="nome_arquivo">Nome do Arquivo (quando � feito download)</label></li>
		<li><input type="text" name="nome_arquivo" id="nome_arquivo" style="width: 300px;" value="<?=$row_doc["nome_arquivo"] ?>" /></li>
		<li class="zebra-dark">Documento Essencial</li>
		<li>
			<input type="radio" name="essencial" id="essencial1" value="1" <?=($row_doc["essencial"]==1?'checked':'') ?> /><label for="essencial1"> Sim</label> &nbsp;|&nbsp;
			<input type="radio" name="essencial" id="essencial2" value="0" <?=($row_doc["essencial"]==0?'checked':'') ?> /><label for="essencial2"> N�o</label>
		</li>
		<li class="zebra-dark">Documento Estrutural</li>
		<li>
			<input type="radio" name="estrutural" id="estrutural1" value="1" <?=($row_doc["estrutural"]==1?'checked':'') ?> /><label for="estrutural1"> Sim</label> &nbsp;|&nbsp;
			<input type="radio" name="estrutural" id="estrutural2" value="0" <?=($row_doc["estrutural"]==0?'checked':'') ?> /><label for="estrutural2"> N�o</label>
		</li>
		<li class="zebra-dark">Documento de Auditoria</li>
		<li>
			<input type="radio" name="auditoria" id="auditoria1" value="1" <?=($row_doc["auditoria"]==1?'checked':'') ?> /><label for="auditoria1"> Sim</label> &nbsp;|&nbsp;
			<input type="radio" name="auditoria" id="auditoria2" value="0" <?=($row_doc["auditoria"]==0?'checked':'') ?> /><label for="auditoria2"> N�o</label>
		</li>
		<li class="menu_sec">Documento vinculado �s categorias</li>
		<?php $c=0; foreach($arr_categorias as $auc => $nome) { ?>
			<li class="zebra-dark" style="background:<?=$default_colors[$c++] ?>;"><?=$nome ?></li>
			<li>
				<input type="radio" name="cat<?=$auc ?>" id="cat<?=$auc ?>_1" <?=($arr_doccat[$auc."_".$doc]==1?'checked':'') ?> value="1" /><label for="cat<?=$auc ?>_1"> Sim</label> &nbsp;|&nbsp;
				<input type="radio" name="cat<?=$auc ?>" id="cat<?=$auc ?>_2" <?=($arr_doccat[$auc."_".$doc]==0?'checked':'') ?> value="0" /><label for="cat<?=$auc ?>_2"> N�o</label>
			</li>
		<?php } ?>
		<li><input type="submit" value="Salvar" style="float:right;" /></li>
	</ul>
	</div>
	</form>