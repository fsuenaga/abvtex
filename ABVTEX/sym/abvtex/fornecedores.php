<?php
	require_once("connect.inc");
	include_once("functions.php");
	include_once("modules/auditoria/audi_libs.php");
    require_once("../date.inc");
	require_once("libs/lib.log.php");
	require_once("libs/lib.arrays.php");
	
	require_once("libs/class.symUploader.php");
	require_once("libs/lib.token.php");
	
	require_once('libs/lib.sendmail.php');
    require_once('libs/mailLayout/lib.mailLayout.php');

	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			
			<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js"></script>
			<link href="js/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
			
			<script type="text/javascript" src="js/formValidation.js"></script>
			
			<script type="text/javascript" src="js/jquery.meio.mask.js"></script>
			
			<script type="text/javascript">
				function exibir(fornecedor){
					//Se atributo expandir estiver setado como 0
					if($('#expandir_'+fornecedor).attr('expandir') == 0){
						$('#expandir_'+fornecedor).attr('expandir', '1');
						$('#expandir_'+fornecedor).css('display', '');
						//$('#link_'+fornecedor).html('Esconder');
					}else{
						$('#expandir_'+fornecedor).attr('expandir', '0');
						$('#expandir_'+fornecedor).css('display', 'none');
						//$('#link_'+fornecedor).html('Exibir');
					}
				}
				
				
				function exibirTabela(id){
					if($('#'+id).attr('expandir') == 0){
						$('#'+id).attr('expandir', '1');
						$('#'+id).css('display', '');
					}else{
						$('#'+id).attr('expandir', '0');
						$('#'+id).css('display', 'none');
					}
				}
				
		
				$(document).ready(function(){
					$('#campo').change(function(){
						if($('#campo').val() == 'CNPJ'){
							$('#valor').removeClass('cnpj').addClass('cnpj');
							$(".cnpj").mask("99.999.999/9999-99");
							$('#valor').focus();
						} else {
							$('#valor').removeClass('cnpj');
							$('#valor').unmask();
							$('#valor').focus();
						}
						$('#pesquisa').focus();
					});
				}); 
				

						
						
					
					
				function exibir(fornecedor){
					//Se atributo expandir estiver setado como 0
					if($('#expandir_'+fornecedor).attr('expandir') == 0){
						$('#expandir_'+fornecedor).attr('expandir', '1');
						$('#expandir_'+fornecedor).css('display', '');
					}else{
						$('#expandir_'+fornecedor).attr('expandir', '0');
						$('#expandir_'+fornecedor).css('display', 'none');
					}
				}
		</script>

			
			
			
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
			
			<!-- HEADER -->
				<?php include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			<?php
				$acao = $_GET['ac'];
				
				switch($acao){
					case 'fornecedores': 
						if($_COOKIE['perm'] == 'Administradora'){
    						include_once('modules/fornecedores/fornecedores_admin.php');
    					}else{
    						include_once('modules/fornecedores/fornecedores.php');
    					};
					break;
					case 'fornecedores2': 
    						include_once('modules/fornecedores/fornecedores-antigo.php');
					break;
					case 'aceitar':  
						if($_COOKIE['perm'] == 'Varejista'){
    						include_once('modules/fornecedores/fornecedores.php');
    					}else{
    						include_once('modules/fornecedores/aceitar.php');
    					}; break;
    				case 'recusar':  
    						include_once('modules/fornecedores/fornecedores.php');
    				break;
				}
				
				
			
			?>
			
			
		</div>	
		
	</body>

</html>	
