<?PHP
    require_once("../date.inc");
    include_once("functions.php");
    require_once("connect.inc");
    
    require_once('libs/lib.lists.php');
    require_once('libs/lib.sendmail.php');
    require_once('libs/mailLayout/lib.mailLayout.php');
    
    
    if($_POST['action']){
    	switch($_COOKIE['perm'])
		{			  
			case 'fornecedor' : { require_once('modules/cadastro/cadastroatualizar_for.php'); }
				break;
			case 'Certificadora' : { require_once('modules/cadastro/cadastroatualizar_cer.php'); }
				break;
			case 'Varejista' : { require_once('modules/cadastro/cadastroatualizar_var.php'); }
				break;
		}
		exit(0);
    }
    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			
			<script type="text/javascript" src="js/formValidation.js"></script>
	
			<script type="text/javascript" src="js/jquery.maskedinput-1.3.js"></script>
			
			<script type="text/javascript">
				//Mascaras de entrada
				jQuery(function($){
					$(".data").mask("99/99/9999");
					$(".telefone").mask("(99) 9999-9999");
					$(".celular").mask("(99) 9999-9999");
					$(".celularSP").mask("(99) 99999-9999");
					$(".cpf").mask("999.999.999-99");
					$(".cep").mask("99999-999");
					$(".cnpj").mask("99.999.999/9999-99");
					$(".placa").mask("aaa-9999");
				}); 
				
				function celularSP(id){
					if(((($('#' + id).val()).substring(0,4) == '(11)')  && ($('#' + id).val()).substring(0,6) == '(11) _') && ($('#' + id).hasClass('celular'))){
		    			$('#' + id).removeClass('celular').addClass('celularSP');
		    		
		    			$(".celularSP").mask("(11) 99999-9999");
		    		} 
				}
				
				function verificarDados(){
					//Verifica se � o mesmo valor pesquisado
				    if($('#cnpjEmpresa').val() != ''){
				    	if(checkCNPJ($('#CNPJ').val())){
				    		//Verifica se CNPJ est� cadastrado na base
				    		$.post('pesquisa.php?funcao=checkFornecedor&pg=cad', { 
				    			cnpj: $("#CNPJ").val()
				    		},
				    		function(resposta2) {
				    			if(resposta2 == false){
				    				//alert('Continuar cadastro.');
				    			}else{
				    			}
				    		});
				    	}else{
				    		alert('CNPJ deve ser valido.');
				    		$('#nao_encontrado').css('display','none');
				    		return false;
				    	}
				    }else{
				    	return false;
				    }
				    return true;
				}
				
				
				function checkCNPJ(cnpj){
						  var i = 0;
						  var l = 0;
						  var strNum = "";
						  var strMul = "6543298765432";
						  var character = "";
						  var iValido = 1;
						  var iSoma = 0;
						  var strNum_base = "";
						  var iLenNum_base = 0;
						  var iLenMul = 0;
						  var iSoma = 0;
						  var strNum_base = 0;
						  var iLenNum_base = 0;
						
						  if (cnpj == "")
								return (false);
						
						  l = cnpj.length;
						  for (i = 0; i < l; i++) {
								caracter = cnpj.substring(i,i+1)
								if ((caracter >= '0') && (caracter <= '9'))
								   strNum = strNum + caracter;
						  };
						
						  if(strNum.length != 14)
								return (false);
						
						  strNum_base = strNum.substring(0,12);
						  iLenNum_base = strNum_base.length - 1;
						  iLenMul = strMul.length - 1;
						  for(i = 0;i < 12; i++)
								iSoma = iSoma +
												parseInt(strNum_base.substring((iLenNum_base-i),(iLenNum_base-i)+1),10) *
												parseInt(strMul.substring((iLenMul-i),(iLenMul-i)+1),10);
						
						  iSoma = 11 - (iSoma - Math.floor(iSoma/11) * 11);
						  if(iSoma == 11 || iSoma == 10)
								iSoma = 0;
						
						  strNum_base = strNum_base + iSoma;
						  iSoma = 0;
						  iLenNum_base = strNum_base.length - 1
						  for(i = 0; i < 13; i++)
								iSoma = iSoma +
												parseInt(strNum_base.substring((iLenNum_base-i),(iLenNum_base-i)+1),10) *
												parseInt(strMul.substring((iLenMul-i),(iLenMul-i)+1),10)
						
						  iSoma = 11 - (iSoma - Math.floor(iSoma/11) * 11);
						  if(iSoma == 11 || iSoma == 10)
								iSoma = 0;
						  strNum_base = strNum_base + iSoma;
						  if(strNum != strNum_base)
								return (false);
						
						  return (true);
						}
						
						
						function buscarEndereco(){
							$.post('pesquisa.php?funcao=buscarEndereco', { 
				    			CEP: $("#cep").val()
				    		},
				    		function(resposta2) {
				    		
				    			var campos = Array();
				    			
				    			var quebra=resposta2.split("||");
				    			for(i=0;i<quebra.length;i++){
			    					quebra2 = quebra[i].split("==");
		        					campos[quebra2[0]] = quebra2[1]; 
				    			}
				    			
				    			$("#rua").val(campos['rua'].replace(/^\s+|\s+$/g,""));
				    			$("#bairro").val(campos['bairro'].replace(/^\s+|\s+$/g,""));
				    			$("#cidade").val(campos['cidade'].replace(/^\s+|\s+$/g,""));
				    			$("#estado").val(campos['estado'].replace(/^\s+|\s+$/g,""));
				    			
				    		});
						}
						
						//Remonta mascaras
						$(function(){
							
							$('#CNPJ').focus(function() {
								$('#CNPJ').addClass('cnpj');
								$(".cnpj").mask("99.999.999/9999-99");
							});
							
							$('#cep').focus(function() {
								$('#cep').addClass('cep');
								$(".cep").mask("99999-999");
							});
						});
				
			</script>	
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
	</head>
		
	<body>
	    <div class="content">
	    
	    <!-- HEADER -->
	    		<?php include('topo.php'); ?>
	    	<!-- FIM HEADER --> 
	    	
	    	
	    	<!-- CONTENT -->
	    	
	    	<?php

				switch($_COOKIE['perm'])
				{			  
					case 'fornecedor' : { include('modules/cadastro/cadastroatualizar_for.php'); }
						break;
					case 'Certificadora' : { include('modules/cadastro/cadastroatualizar_cer.php'); }
						break;
					case 'Varejista' : { include('modules/cadastro/cadastroatualizar_var.php'); }
						break;
				}
			?>
	    	
	    	<!-- FIM CONTENT --> 
	    </div>	
		
	</body>

</html>	