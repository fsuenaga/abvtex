<?php
require_once("conn.php");
class RotinasAutomaticas extends ConexaoDb {
	var $rotinasHabilitadas = array("atualiza_data_validade","atualiza_vinculo","validade_certificacao","vencimento_plano_acao","vencimento_manutencao");
	var $empresa = "abvtex";
	var $certificadoras = array();
	var $varejistas = array();
	var $admis = array();
	var $fornecedores = array();
	var $motivos = array();
	
	var $certificadorasEmail = array();
	var $varejistasEmail = array();
	var $fornecedoresEmail = array();
	var $AdminEmail;
	var $dataRotina;
	function __construct($data){
		$this->dataRotina = $data;
		$this->ConectaDb();
		$this->GetContatosCertificadoras();
		$this->GetContatosVarejistas();
		$this->GetContatosAdms();
		$this->GetMotivos();
		$this->Rotinas();
	}
	private function Rotinas(){
		foreach($this->rotinasHabilitadas as $rotina){
			$this->$rotina();
		}
	}
	function Executa($tipo,$mail_previa = false){
		foreach($this->fornecedores as $forn => $motivo){
			$getF = $this->SelecionaBanco("SELECT *,CC_Fr.classe as class from CC_Fornecedor inner join CC_Fr on CC_Fornecedor.fornecedor = CC_Fr.fornecedor WHERE CC_Fr.empresa = 'abvtex' and CC_Fr.fornecedor = '{$forn}'");
			$this->fornecedores[$forn]['dados'] = $getF[0];
			$this->fornecedores[$forn]['audi'] = 0;
			$this->fornecedores[$forn]['log'] = 1;
			if($tipo != 'only-log'){
				$this->fornecedores[$forn]['log'] = 0;
				$this->fornecedores[$forn]['audi'] = $this->IncluirAudi($this->fornecedores[$forn]);
				$this->UpdateFornecedor($this->fornecedores[$forn]);
			}
			$emailLinha = "
			<tr>
			<td>
				<a href='https://www.techsocial.com.br/sym/abvtex/fornecedor_dados.php?fr={$this->fornecedores[$forn]['dados']['fornecedor']}&ac=auditoria'>{$this->fornecedores[$forn]['dados']['fornecedor']}</a>
			</td>
			<td>
				<a href='https://www.techsocial.com.br/sym/abvtex/fornecedor_dados.php?fr={$this->fornecedores[$forn]['dados']['fornecedor']}&ac=auditoria'>{$this->fornecedores[$forn]['dados']['razaoSocial']}</a>
			</td>
			<td>{$this->motivos[$this->fornecedores[$forn]['motivo']]}</td></tr>";
			$this->certificadorasEmail[$this->fornecedores[$forn]['dados']['certificadora']] .= $emailLinha;
			$varejistas = $this->SelecionaBanco("SELECT empresa from CC_Fr_Vinculo WHERE empresaTipo = 'Varejista' and status >= 2 and fornecedor = '{$forn}' and ativo = 1");
			if(is_array($varejistas)){
				foreach($varejistas as $varejista){
					$this->varejistasEmail[$varejista['empresa']] .= $emailLinha;
				}
			}
			$fornecedores = $this->SelecionaBanco("SELECT CC_Fr.empresa,CC_Fornecedor.resp_email,CC_Fornecedor.razaoSocial as nome from CC_Fr inner join CC_Fornecedor on CC_Fr.empresa = CC_Fornecedor.fornecedor WHERE CC_Fr.empresa <> 'abvtex' and CC_Fr.fornecedor = '{$forn}' and CC_Fr.ativo = 1");
			if(is_array($fornecedores)){
				foreach($fornecedores as $fornecedor){
					$this->fornecedoresEmail[$fornecedor['empresa']]['linhas'] .= $emailLinha;
					$this->fornecedoresEmail[$fornecedor['empresa']]['email'] = $fornecedor['resp_email'];
					$this->fornecedoresEmail[$fornecedor['empresa']]['nome'] = $fornecedor['nome'];
				}
			}
			$this->AdminEmail .= $emailLinha;
			$this->IncluiLog($this->fornecedores[$forn]);
		}
		if($tipo != 'only-log'){
			$this->ExecutaEmail();
		}
		if($mail_previa === true){
			$this->ExecutaPrevia();
		}
	}
	function ExecutaPrevia(){
		require_once('../libs/lib.sendMail.php');
		$rotina = 1;
		$qtds_email = 0;
		foreach($this->certificadoras as $key => $Adados){
			if(!empty($this->certificadorasEmail[$key])){
				foreach($Adados as $dados){
						$dadosEmail = $this->LayoutEmailPreviaFornecedores();
						sendMail($dados['nome'], $dados['email'],$dadosEmail[0], $dadosEmail[1]." - ".strtoupper($key));
						$qtds_email++;
				}
			}
		}
		echo "Foram enviados ".$qtds_email." email.";
	}
	function ExecutaEmail(){
		
		require_once('../libs/lib.sendMail.php');
		$rotina = 1;
		$qtds_email = 0;
		
		foreach($this->varejistas as $key => $Adados){
			if(!empty($this->varejistasEmail[$key])){
				foreach($Adados as $dados){
					$dadosEmail = $this->LayoutEmailVariosFornecedores();
					$dadosEmail[0] = str_ireplace("{LISTAFORNECEDOR}",$this->varejistasEmail[$key],$dadosEmail[0]);
					if(!sendMail($dados['nome'], $dados['email'],$dadosEmail[0], $dadosEmail[1]." - ".strtoupper($key))){
						echo "Erro E-mail";
						file_put_contents("erro_exec.txt",date('d/m/Y H:i:s'));
						exit;
					}
					$qtds_email++;
			
				}
			}
		}
		
		foreach($this->certificadoras as $key => $Adados){
			if(!empty($this->certificadorasEmail[$key])){
				foreach($Adados as $dados){
						$dadosEmail = $this->LayoutEmailVariosFornecedores();
						$dadosEmail[0] = str_ireplace("{LISTAFORNECEDOR}",$this->certificadorasEmail[$key],$dadosEmail[0]);
						sendMail($dados['nome'], $dados['email'],$dadosEmail[0], $dadosEmail[1]." - ".strtoupper($key));
						$qtds_email++;
				}
			}
		}
		
		foreach($this->admis as $key => $dados){
					if(!empty($this->AdminEmail)){
						$dadosEmail = $this->LayoutEmailVariosFornecedores();
						$dadosEmail[0] = str_ireplace("{LISTAFORNECEDOR}",$this->AdminEmail,$dadosEmail[0]);
						sendMail($dados['nome'], $dados['email'],$dadosEmail[0], $dadosEmail[1]);
						$qtds_email++;
					}
		}
		
		foreach($this->fornecedoresEmail as $key => $dados){
				$dadosEmail = $this->LayoutEmailFornecedores();
				$dadosEmail[0] = str_ireplace("{LISTAFORNECEDOR}",$dados['linhas'],$dadosEmail[0]);
				sendMail($dados['nome'], $dados['email'],$dadosEmail[0], $dadosEmail[1]);
				$qtds_email++;
		}
		foreach($this->fornecedores as $forn => $motivo){
				
				$dadosEmail = $this->LayoutEmailFornecedor($this->fornecedores[$forn]['dados']['razaoSocial'],$this->fornecedores[$forn]['motivo']);
				sendMail($this->fornecedores[$forn]['dados']['razaoSocial'], $this->fornecedores[$forn]['dados']['resp_email'],$dadosEmail[0], $dadosEmail[1]);
				$qtds_email++;
		}
		echo "Foram enviados ".$qtds_email." email.";
	}
	private function atualiza_data_validade(){
		$sql = "UPDATE CC_Fr
				SET CC_Fr.data_validade = ADDDATE(
				CC_Fr.data_certificacao,
				INTERVAL 2 YEAR
				)
				WHERE
				(CC_Fr.data_validade IS NULL  OR CC_Fr.data_validade < ADDDATE(CC_Fr.data_certificacao,INTERVAL 2 YEAR)) 
				AND CC_Fr.data_certificacao IS NOT NULL
				AND CC_Fr.empresa = '{$this->empresa}'";
		$this->AlteraBanco($sql);
	}
	private function atualiza_vinculo(){
		$sql = "UPDATE CC_Fr
				INNER JOIN CC_Fr_Vinculo ON CC_Fr_Vinculo.fornecedor = CC_Fr.fornecedor
				SET CC_Fr.certificadora = CC_Fr_Vinculo.empresa
				WHERE
					CC_Fr_Vinculo.empresaTipo = 'Certificadora'
				AND (CC_Fr_Vinculo.empresa <> CC_Fr.certificadora or CC_Fr.certificadora is null)
				AND CC_Fr_Vinculo.ativo = 1
				AND CC_Fr.empresa = '{$this->empresa}'";
		$this->AlteraBanco($sql);
	}
	private function validade_certificacao(){
		$sql = "select *, ADDDATE(CC_Fr.data_validade,INTERVAL 75 DAY) as previa from CC_Fr
				WHERE
				CC_Fr.status_certificacao <> '2'
				AND ADDDATE(CC_Fr.data_validade,INTERVAL 75 DAY) < '{$this->dataRotina}'
				AND CC_Fr.status_fornecedor = 1
				AND CC_Fr.data_certificacao IS NOT NULL
				AND CC_Fr.data_validade IS NOT NULL
				AND CC_Fr.empresa = '{$this->empresa}'";
		$forne = $this->SelecionaBanco($sql);
		foreach($forne as $fornecedores){
			if(isset($this->fornecedores[$fornecedores['fornecedor']])){
				continue;
			}
			$this->fornecedores[$fornecedores['fornecedor']]['motivo'] = "Fora_Validade";
			$this->fornecedores[$fornecedores['fornecedor']]['previa'] = $fornecedores['previa'];
		}
	}
	private function vencimento_plano_acao(){
    
        //$excessoes = " AND fornecedor NOT IN ('1371660835747','1376309417775')"; //quebra galho at� fixar a regra das suspens�es - d�bora - 06/04/2015

    
		// $sql = "select *,ADDDATE((select audit.data from Audi_Fornecedor as audit where audit.resultado = 2 and audit.status = 1 and audit.empresa = '{$this->empresa}' and audit.fornecedor = CC_Fr.fornecedor order by data DESC limit 1),INTERVAL 105 DAY) as previa from CC_Fr
				// WHERE
				// CC_Fr.status_certificacao = 2
				// AND CC_Fr.data_certificacao IS NOT NULL
				// AND CC_Fr.data_validade IS NOT NULL
				// AND ADDDATE((select audit.data from Audi_Fornecedor as audit where audit.resultado = 2 and audit.status = 1 and audit.empresa = '{$this->empresa}' and audit.fornecedor = CC_Fr.fornecedor order by data DESC limit 1),INTERVAL 105 DAY) < '{$this->dataRotina}'
				// AND CC_Fr.empresa = '{$this->empresa}'";
		$sql = "
				select *,ADDDATE((
				SELECT audit.data
				FROM  `Audi_Fornecedor` AS audit
				WHERE audit.status =1
				AND audit.empresa =  '{$this->empresa}'
				AND audit.resultado =2
				AND IF( (

				SELECT auditt.data
				FROM Audi_Fornecedor AS auditt
				WHERE auditt.resultado <>2
				AND auditt.status =1
				AND auditt.empresa =  '{$this->empresa}'
				AND auditt.fornecedor = audit.fornecedor
				ORDER BY auditt.data DESC 
				LIMIT 1
				) IS NULL ,  '0000-00-00', (

				SELECT auditt.data
				FROM Audi_Fornecedor AS auditt
				WHERE auditt.resultado <>2
				AND auditt.status =1
				AND auditt.empresa =  '{$this->empresa}'
				AND auditt.fornecedor = audit.fornecedor
				ORDER BY auditt.data DESC 
				LIMIT 1
				) ) < audit.data
				AND audit.fornecedor =  CC_Fr.fornecedor
				ORDER BY audit.data ASC 
				LIMIT 1
				),INTERVAL 105 DAY) as previa from CC_Fr
								WHERE
								CC_Fr.status_certificacao = 2
								AND CC_Fr.data_certificacao IS NOT NULL
								AND CC_Fr.data_validade IS NOT NULL
								AND ADDDATE((
				SELECT audit.data
				FROM  `Audi_Fornecedor` AS audit
				WHERE audit.status =1
				AND audit.empresa =  '{$this->empresa}'
				AND audit.resultado =2
				AND IF( (

				SELECT auditt.data
				FROM Audi_Fornecedor AS auditt
				WHERE auditt.resultado <>2
				AND auditt.status =1
				AND auditt.empresa =  '{$this->empresa}'
				AND auditt.fornecedor = audit.fornecedor
				ORDER BY auditt.data DESC 
				LIMIT 1
				) IS NULL ,  '0000-00-00', (

				SELECT auditt.data
				FROM Audi_Fornecedor AS auditt
				WHERE auditt.resultado <>2
				AND auditt.status =1
				AND auditt.empresa =  '{$this->empresa}'
				AND auditt.fornecedor = audit.fornecedor
				ORDER BY auditt.data DESC 
				LIMIT 1
				) ) < audit.data
				AND audit.fornecedor =  CC_Fr.fornecedor
				ORDER BY audit.data ASC 
				LIMIT 1

				),INTERVAL 105 DAY) < '{$this->dataRotina}'
				AND CC_Fr.empresa = '{$this->empresa}';
                ";
		$forne = $this->SelecionaBanco($sql);
		
		foreach($forne as $fornecedores){
			if(isset($this->fornecedores[$fornecedores['fornecedor']])){
				continue;
			}
			$this->fornecedores[$fornecedores['fornecedor']]['motivo'] = "Plano_Acao";
			$this->fornecedores[$fornecedores['fornecedor']]['previa'] = $fornecedores['previa'];
		}
	}
	private function vencimento_manutencao(){
		$sql = "select *,ADDDATE(ADDDATE(CC_Fr.data_certificacao,INTERVAL 1 YEAR),INTERVAL 75 DAY) as previa from CC_Fr
				WHERE
				CC_Fr.status_certificacao not in (0,2,5,6)
				AND CC_Fr.status_fornecedor = 1
				AND CC_Fr.data_certificacao IS NOT NULL
				AND CC_Fr.data_validade IS NOT NULL
				AND ADDDATE(ADDDATE(CC_Fr.data_certificacao,INTERVAL 1 YEAR),INTERVAL 75 DAY) < '{$this->dataRotina}'
				AND (select audit.data from Audi_Fornecedor as audit where audit.status = 1 and audit.resultado = 1 and audit.tipo in (1,3) and audit.empresa = '{$this->empresa}' and audit.fornecedor = CC_Fr.fornecedor order by data DESC limit 1) is null
				AND CC_Fr.empresa = '{$this->empresa}'";
		
		$forne = $this->SelecionaBanco($sql);
		foreach($forne as $fornecedores){
			if(isset($this->fornecedores[$fornecedores['fornecedor']])){
				continue;
			}
			$this->fornecedores[$fornecedores['fornecedor']]['motivo'] = "Manutencao";
			$this->fornecedores[$fornecedores['fornecedor']]['previa'] = $fornecedores['previa'];
		}
	}
	private function SqlUsuariosEmail($tipo){
		return "SELECT
					CC_Usuario.empresa,CC_Usuario.nome,CC_Usuario.email
				FROM
					CC_Usuario
				INNER JOIN
					CC_Associacao
				ON
					CC_Associacao.associado = CC_Usuario.empresa
				WHERE
					CC_Associacao.tipo = '$tipo'
				AND
					CC_Usuario.nivel = 1";
	}
	private function GetContatosCertificadoras(){
		$sql = $this->SqlUsuariosEmail("Certificadora");
		$this->certificadoras = $this->SelecionaBanco($sql);
		foreach($this->certificadoras as $key => $values){
			$this->certificadoras[$values['empresa']][] = $values;
			unset($this->certificadoras[$key]);
		}
	}
	private function GetContatosVarejistas(){
		$sql = $this->SqlUsuariosEmail("Varejista");
		$this->varejistas = $this->SelecionaBanco($sql);
		foreach($this->varejistas as $key => $values){
			$this->varejistas[$values['empresa']][] = $values;
			unset($this->varejistas[$key]);
		}
	}
	private function GetContatosAdms(){
		$sql = $this->SqlUsuariosEmail("Administradora");
		$this->admis = $this->SelecionaBanco($sql);
	}
	function GetMotivos(){
		$this->motivos = array(
			"Fora_Validade" => "Fora da Validade de Certifica��o. Car�ncia de 75 Dias.",
			"Plano_Acao" => "Plano de A��o n�o respondido no prazo determinado. Prazo de 105 dias.",
			"Manutencao" => "Manuten��o n�o realizada no prazo determinado. Prazo de 1 ano e 75 dias ap�s a certifica��o."
		);
	}
	private function IncluiLog($forn){
		if(empty($forn['dados']['status_fornecedor'])){
			$forn['dados']['status_fornecedor'] = 0;
		}
		$values = "('{$this->dataRotina}','{$forn['previa']}','{$forn['dados']['fornecedor']}','{$forn['audi']}','{$forn['dados']['status_certificacao']}','{$forn['dados']['status_fornecedor']}','{$forn['motivo']}','{$forn['log']}')";
		$insertLog = "Insert into Log_Rotina (data,data_previa,fornecedor,audi,status_certificacao,status_fornecedor,motivo,apenas_log) values {$values}";
		$this->InsereBanco($insertLog);
	}
	private function IncluirAudi($forn){
		$values = "('{$this->empresa}','{$forn['dados']['certificadora']}','{$forn['dados']['fornecedor']}',1,'{$forn['previa']}',4,{$forn['dados']['class']},6,1,'{$this->motivos[$forn['motivo']]}')";
		$insertAudi = "insert into Audi_Fornecedor (empresa,certificadora,fornecedor,checklist,data,tipo,tipo_fornecedor,resultado,status,motivo) values {$values}";
		return $this->InsereBanco($insertAudi);
	}
	private function UpdateFornecedor($forn){
		//if($forn['motivo'] == 'Fora_Validade'){
			$status_fornecedor = ", status_fornecedor = '6'";
		//}
		$up_forn = "UPDATE CC_Fr set data_status= '{$forn['previa']}',status_certificacao = '6', motivo = '{$this->motivos[$forn['motivo']]}' {$status_fornecedor} where fornecedor = '{$forn['dados']['fornecedor']}' and empresa = '{$this->empresa}'";
		$this->AlteraBanco($up_forn);
		$up_audis = "UPDATE Audi_Fornecedor set status = '1' where fornecedor = '{$forn['dados']['fornecedor']}' and empresa = '{$this->empresa}' and data <= '{$forn['previa']}'";
		$this->AlteraBanco($up_audis);
	}
	private function LayoutEmailPreviaFornecedores(){
		$assunto = '[ABVTEX] Pr�via de Suspens�o de Empresas - '.date('d/m/Y');
		$titulo  = $assunto;
		$texto   = '
			Prezado(a),
			<br><br>
			Na p�gina inicial do sistema de gest�o de fornecedores ABVTEX est� dispon�vel uma rela��o das empresas que ser�o suspensas nos pr�ximos 7 dias, gentileza verificar.<br><br> 
		';
		require_once("../libs/mailLayout/lib.mailLayout.php");
		$assinatura = 'Atenciosamente';
		$corpo = LoadLayout('padrao');
		$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		return array($corpo,$assunto);
	}
	private function LayoutEmailVariosFornecedores(){
		$assunto = '[ABVTEX] Suspens�o de Empresas - '.date('d/m/Y');
		$titulo  = $assunto;
		$texto   = '
			Prezado(a),
			<br><br>
			Segue abaixo rela��o das empresas suspensas na sistema de gest�o de forcedores ABVTEX:<br><br> 
			<table border="1" width="100%">
			{LISTAFORNECEDOR}
			</table>
		';
		require_once("../libs/mailLayout/lib.mailLayout.php");
		$assinatura = 'Atenciosamente';
		$corpo = LoadLayout('padrao');
		$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		return array($corpo,$assunto);
	}
	private function LayoutEmailFornecedores(){
		$assunto = '[ABVTEX] Suspens�o de Subcontratados - '.date('d/m/Y');
		$titulo  = $assunto;
		$texto   = '
			Prezado(a),
			<br><br>
			Segue abaixo rela��o dos subcontratados suspensos na sistema de gest�o de forcedores ABVTEX:<br><br> 
			<table border="1" width="100%">
			{LISTAFORNECEDOR}
			</table>
		';
		require_once("../libs/mailLayout/lib.mailLayout.php");
		$assinatura = 'Atenciosamente';
		$corpo = LoadLayout('padrao');
		$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		return array($corpo,$assunto);
	}
	private function LayoutEmailFornecedor($nome,$motivo){
		$assunto = '[ABVTEX] Suspens�o de Empresa - '.date('d/m/Y');
		$titulo  = $assunto;
		$texto   = '
			Prezado Fornecedor,
			<br><br>
			Voc� foi suspenso no programa Associa��o Brasileira do Varejo T�xtil - ABVTEX<br><br>
			<b>Motivo:</b><br>
			'.$this->motivos[$motivo].'<br> 
		';
		require_once("../libs/mailLayout/lib.mailLayout.php");
		$assinatura = 'Atenciosamente';
		$corpo = LoadLayout('padrao');
		$corpo = replaceContent($corpo,$texto,$titulo,$assinatura);
		return array($corpo,$assunto);
	}
}

class ReverseRotinasAutomaticas extends ConexaoDb {
	var $data = "";
	var $motivo = "";
	var $apenas_log = "";
	var $log = "";
	var $empresa = "abvtex";
	function __construct($data = '',$motivo='',$apenas_log=''){
		$this->data = $data;
		$this->motivo = $motivo;
		$this->apenas_log = $apenas_log;
		$this->ConectaDb();
	}
	public function Reverse(){
		$filtro_motivo = "";
		$filtro_apenas_log = "";
		$filtro_data = "";
		if($this->motivo != ""){
			$filtro_motivo = " AND motivo = '{$this->motivo}'";
		}
		if($this->apenas_log != ""){
			$filtro_apenas_log = " AND apenas_log = {$this->apenas_log}";
		}
		if($this->data != ""){
			$filtro_data = " AND data = '{$this->data}'";
		}
		if($this->motivo == "" && $this->apenas_log == "" && $this->data == ""){
			return true;
		}
		$sql_Log = "SELECT * from  Log_Rotina where 1=1 $filtro_data $filtro_motivo $filtro_apenas_log";
		$this->log = $this->SelecionaBanco($sql_Log);
		if(is_array($this->log)){
			foreach($this->log as $log){
				if($log['audi'] > 0){
					$this->DeletaAudi($log['audi']);
					$this->UpdateFornecedor($log);
				}
				$this->DeleteLog($log['id_rotina']);
			}
		}
	}
	private function DeleteLog($id_rotina){
		$del_log = "Delete from  Log_Rotina where id_rotina = '{$id_rotina}'";
		$this->AlteraBanco($del_log);
	}
	private function UpdateFornecedor($forn){
		$up_forn = "UPDATE CC_Fr set status_certificacao = '{$forn['status_certificacao']}', status_fornecedor = '{$forn['status_fornecedor']}', motivo = NULL where fornecedor = '{$forn['fornecedor']}' and empresa = '{$this->empresa}'";
		$this->AlteraBanco($up_forn);
	}
	private function DeletaAudi($audi){
		$del_audi = "Delete from  Audi_Fornecedor where audi = '{$audi}'";
		$this->AlteraBanco($del_audi);
	}
}
?>