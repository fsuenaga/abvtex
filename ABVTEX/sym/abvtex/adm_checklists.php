<?php
	if($_COOKIE["perm"]!="Administradora") { echo('<script> history.back(); </script>'); exit(0); }
	
	require_once("connect.inc");
	include_once("functions.php");
    require_once("libs/date.inc");
	require_once("libs/lib.log.php");
	
	$acao       = ''.$_GET['ac'];
	$acao_model = ''.$_GET['mac'];
	$chk_id     = intval(0+$_GET['ckid']);
	$modl       = intval(0+$_GET['modl']);
	$qust       = intval(0+$_GET['qust']);
	
	if($acao_model!='')
	{
		switch($acao_model){
			case 'chkadd'   : include_once('modules/adm/checklists/check_model_chkadd.php');     break;
			case 'chkupd'   : include_once('modules/adm/checklists/check_model_chkedit.php');    break;
			case 'chkmdladd': include_once('modules/adm/checklists/check_model_mdladd.php');     break;
			case 'chkmdlupd': include_once('modules/adm/checklists/check_model_mdledit.php');    break;
			case 'chkmdlord': include_once('modules/adm/checklists/check_model_mdlreorder.php'); break;
			case 'chkqstadd': include_once('modules/adm/checklists/check_model_qstadd.php');     break;
			case 'chkqstupd': include_once('modules/adm/checklists/check_model_qstedit.php');    break;
			case 'chkqstord': include_once('modules/adm/checklists/check_model_qstreorder.php'); break;
			case 'chkqstatv': include_once('modules/adm/checklists/check_model_qst_ativar.php'); break;
			case 'chkcatadd': include_once('modules/adm/checklists/check_model_cat_add.php');    break;
			case 'chkcatrmv': include_once('modules/adm/checklists/check_model_cat_remove.php'); break;
			case 'chkduplic': include_once('modules/adm/checklists/check_model_duplicate.php');  break;
		}
	}
	
	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	
	include_once('modules/adm/checklists/check_params.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/visibility.js"></script>
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
			<!-- HEADER -->
				<?php include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			<?php
				switch($acao){
					case '':          include_once('modules/adm/checklists/check_list.php');         break;
					case 'chknew':    include_once('modules/adm/checklists/check_new.php');          break;
					case 'chkedt':    include_once('modules/adm/checklists/check_edit.php');         break;
					case 'chkmdlnew': include_once('modules/adm/checklists/check_modulo_new.php');   break;
					case 'chkmdledt': include_once('modules/adm/checklists/check_modulo_edit.php');  break;
					case 'chkqstnew': include_once('modules/adm/checklists/check_questao_new.php');  break;
					case 'chkqstedt': include_once('modules/adm/checklists/check_questao_edit.php'); break;
					case 'chkduplic': include_once('modules/adm/checklists/check_duplicate.php');    break;
				}
			?>
		</div>
	</body>
</html>