<?php
	require_once("connect.inc");
	include_once("functions.php");
	include_once("modules/auditoria/audi_libs.php");
    require_once("../date.inc");
	require_once("libs/lib.log.php");
	
	if($_COOKIE['perm'] == 'fornecedor' && $_GET['tipo'] != '2'){
		$frid = $_COOKIE['empr'];
	}else{
		$frid = $_GET['frid'];
	}
	
	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	
	$sql = "SELECT 
				* 
			FROM Audi_Certificado 
				JOIN CC_Fr ON CC_Fr.fornecedor = Audi_Certificado.fornecedor AND CC_Fr.empresa = '".$_COOKIE['empresa']."'
			WHERE 
				Audi_Certificado.empresa = '".$_COOKIE['empresa']."' 
				AND Audi_Certificado.fornecedor = '".$frid."' 
			ORDER BY Audi_Certificado.id DESC 
			LIMIT 1";
			
			echo "<!-- " . $sql . " -->" ;
			
	$query = mysql_query($sql) or die(mysql_error());
	
	if(mysql_num_rows($query)<=0) { echo('<table width="100%" height="100%"><tr><td align="center"><em> -- Certificado n�o encontrado ou suspenso -- </em></td></tr></table>'); exit(0); }
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/formValidation.js"></script>
			
			<script type="text/javascript" src="js/jquery.maskedinput-1.3.js"></script>
			
			<script type="text/javascript">
				//Mascaras de entrada
				jQuery(function($){
				    $(".data").mask("99/99/9999");
				    $(".telefone").mask("(99) 9999-9999");
				    $(".celular").mask("(99) 9999-9999");
				    $(".celularSP").mask("(99) 99999-9999");
				    $(".cpf").mask("999.999.999-99");
				    $(".cep").mask("99999-999");
				    $(".cnpj").mask("99.999.999/9999-99");
				    $(".placa").mask("aaa-9999");
				});
		</script>
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			<?php
			
			$empresa = mysql_result($query,0,'Audi_Certificado.nomeEmpresa');
			$endereco = mysql_result($query,0,'Audi_Certificado.endereco');
			$certificadora = mysql_result($query,0,'Audi_Certificado.nomeCertificadora');
			$data_auditoria = convertSysDate(mysql_result($query,0,'Audi_Certificado.dataAuditoria'));
			$classificacao = mysql_result($query,0,'Audi_Certificado.status');
			$ID = mysql_result($query,0,'Audi_Certificado.fornecedor');
			// $data_certificacao = convertSysDate(mysql_result($query,0,'Audi_Certificado.dataCertificacao'));
			$data_certificacao = convertSysDate(mysql_result($query,0,'CC_Fr.data_certificacao'));
			$criacao = convertSysDate(mysql_result($query,0,'Audi_Certificado.data'));
			$validade = convertSysDate(mysql_result($query,0,'CC_Fr.data_validade'));
			
			$val = explode('-', mysql_result($query,0,'CC_Fr.data_certificacao'));
			$validade = convertSysDate(($val[0]+2).'-'.$val[1].'-'.$val[2]);

			// if(mysql_result($query,0,'Audi_Certificado.dataCertificacao') != '' and $validade == ''){
				// $val = explode('-', mysql_result($query,0,'Audi_Certificado.dataCertificacao'));
				// $validade = convertSysDate(($val[0]+2).'-'.$val[1].'-'.$val[2]);
			// }
			
			// if( $validade < $data_certificacao ) {
				// $val = explode( '-', mysql_result( $query, 0, 'Audi_Certificado.dataCertificacao' ) ) ;
				// $validade = convertSysDate( ( $val[0] + 2 ) . '-' . $val[1] . '-' . $val[2] ) ;
			// }
				
			?>
			
			
			
			<img src="logos/<?php echo mysql_result($query,0,'Audi_Certificado.certificadora'); ?>_l.png" alt="logo" style="float:left;" />
			<div style="text-align: center; font-weight: bold; margin-top: 50px; font-size: 22px;">
				Certificado
			</div>
			<br clear="both" />
			<br/>
			
			<div style="margin-top: 19px;">
				<?php echo $empresa; ?><br />
				<?php echo $endereco; ?>
			</div>
			<br/><br/>
			
			<div>
				O Organismo de Qualifica��o <?php echo $certificadora; ?> formaliza que a empresa acima citada participou do <?php echo $systemName; ?> e que, em auditoria de qualifica��o independente, realizada em <?php echo $data_auditoria; ?>, seus auditores avaliaram que a planta acima citada obteve o seguinte resultado no Programa de Certifica��o:
			</div>
			<br/><br/>
			
			<span>Classifica��o obtida: <?php echo $classificacao; ?></span>
			<br /><br />
			<span>Organismo de qualifica��o: <?php echo $certificadora; ?></span>
			
			<br /><br />
			<span>N�mero do processo: <?php echo $ID; ?></span>
			
			<br /><br />
			<span>Validade: <?php echo $validade; ?></span>
			
			<br /><br />
			<span>A validade destas informa��es pode ser confirmada no portal do Programa (www.abvtex.org.br/certificacao), por meio do N�mero do Processo, informado acima.</span>
			
			<br /><br />
			<span>� obrigat�ria a realiza��o de Auditoria de Manuten��o, ap�s 1 (um) ano da obten��o da qualifica��o.</span>
			
			<br /><br /><br /><br />
			<span>S�o Paulo,</span><br/>
			<span><?php echo $criacao; ?></span>
			
			
			
		</div>	
		
	</body>

</html>	
