<?php
	require_once("connect.inc");
	include_once("functions.php");
	include_once("modules/auditoria/audi_libs.php");
    require_once("../date.inc");
	require_once("libs/lib.log.php");
	require_once("libs/lib.arrays.php");
	
	//Formato
	if($_GET['fm'] == 'exc'){
    	include('libs/excel_header.php');
    	
    	exportSetHTMLHeaders('relatorio_'.$_GET['ac']);
    }
	
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		

		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<?php if((($_GET['ac'] == 'dinamico') && ($_GET['ft'] == '1')) || ($_GET['ac'] == 'fornecedores') || ($_GET['ac'] == 'subcontratados')){ ?><link href="css/style_fs.css" rel="stylesheet" type="text/css" /> <?php } ?>
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->		
			<script type="text/javascript" src="js/jquery.js"></script>
		<!-- JS -->
	</head>

	<body>
	
		<!-- HEADER -->
			<?php include('topo.php'); ?>
		<!-- FIM HEADER --> 
	
		<div class="content">

			<div class="mainTitle">
				Manuais
				<a style="float:right;" href="javascript:history.back(-1)">
				<img alt="Voltar" src="images/voltar.png">
				</a>
			</div>
			
			<ul>
		
				<?php if($_COOKIE['perm'] != 'Fornecedor'){ ?>
					<li><a href="manuais/Manual-Certificadora.pdf" target="_blank">Manual Certificadora<a/></li>
					<li><a href="manuais/Manual-Varejista.pdf" target="_blank">Manual Varejista<a/></li>
				<?php }?>
		
				<li><a href="manuais/Manual-Fornecedores.pdf" target="_blank">Manual Fornecedor<a/></li>
		
			</ul>
			
			<!-- FIM CONTENT -->
			
		</div>	
		
	</body>

</html>	
