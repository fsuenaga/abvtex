<?php
	function ConsistenciaTiposAuditoria($tipo_ultimo,$res_ultimo,$status_certificacao_audi,$frid,$prev_tipo_ultimo,$prev_resultado_ultimo){
		global $ParametroTiposAuditorias;
		$retorna = array();
		if("".$tipo_ultimo=="nenhum"){
			$retorna['tipo'] = 0;
			$retorna['prazo'] = 105;
		}elseif("".$tipo_ultimo == "4" && $prev_tipo_ultimo == "4"){
			$retorna['tipo'] = 5;
			$retorna['prazo'] = 105;
		}elseif("".$status_certificacao_audi == "6"){
			$retorna = $ParametroTiposAuditorias[$prev_tipo_ultimo][6][$prev_resultado_ultimo];
		}else{
			$retorna = $ParametroTiposAuditorias[$tipo_ultimo][$status_certificacao_audi][$res_ultimo];
		}
		return $retorna;
	}
	function tratarVariaveis($variavel){
		
		$search = 	array("'"); 
		$replace = 	array("`");
			
		$result = trim(str_replace($search, $replace, $variavel)).'';
		
		return $result;
	}
	
	function remover_caracter($string) {
	    $string = preg_replace("/[�����]/", "a", $string);
	    $string = preg_replace("/[�����]/", "A", $string);
	    $string = preg_replace("/[���]/", "e", $string);
	    $string = preg_replace("/[���]/", "E", $string);
	    $string = preg_replace("/[��]/", "i", $string);
	    $string = preg_replace("/[��]/", "I", $string);
	    $string = preg_replace("/[�����]/", "o", $string);
	    $string = preg_replace("/[�����]/", "O", $string);
	    $string = preg_replace("/[���]/", "u", $string);
	    $string = preg_replace("/[���]/", "U", $string);
	    $string = preg_replace("/�/", "c", $string);
	    $string = preg_replace("/�/", "C", $string);
	 // $string = preg_replace("/[][><}{)(:;,!?*%~^`&#@]/", "", $string);
	 //   $string = preg_replace("/ /", "_", $string);
	    return $string;
	}
	
	
	
	
	/*
	Exemplo de uso:
	$cnpj = "11222333000199";
	$cpf = "00100200300";
	$cep = "08665110";
	$data = "10102010";
	 
	echo mascaras($cnpj,'##.###.###/####-##');
	echo mascaras($cpf,'###.###.###-##');
	echo mascaras($cep,'#####-###');
	echo mascaras($data,'##/##/####');	
	*/
	function mascaras($val, $mask)
	{
		$maskared = '';
		$k = 0;
	 	for($i = 0; $i<=strlen($mask)-1; $i++){
	 	if($mask[$i] == '#'){
	 	if(isset($val[$k]))
	 		$maskared .= $val[$k++];
	 	}
	 	else{
	 		if(isset($mask[$i]))
	 			$maskared .= $mask[$i];
	 		}
	 	}
	 	return $maskared;
	}
	
	
	

/**
* Fun��o para gerar senhas aleat�rias
*/

function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
{
$lmin = 'abcdefghijklmnopqrstuvwxyz';
$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$num = '1234567890';
$simb = '!@#$%*-';
$retorno = '';
$caracteres = '';

$caracteres .= $lmin;
if ($maiusculas) $caracteres .= $lmai;
if ($numeros) $caracteres .= $num;
if ($simbolos) $caracteres .= $simb;

$len = strlen($caracteres);
for ($n = 1; $n <= $tamanho; $n++) {
$rand = mt_rand(1, $len);
$retorno .= $caracteres[$rand-1];
}
return $retorno;
}

?>