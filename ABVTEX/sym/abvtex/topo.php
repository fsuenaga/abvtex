<?php
	require_once('modules/auditoria/audi_libs.php');
?>

<div id="header">
	
	<div id="header-left"> 
		<div style="float:right; padding-top:4px;">
			<a style="float: left; margin-right: 23px;" href="config.php" title="Configura&ccedil;&otilde;es"><img style="width: 18px;" alt="Configura&ccedil;&otilde;es" src="images/config.png"></a>
			<div style="width: 200px;float:left">
				<?php echo date('d/m/Y H:i'); ?>
				<a href="logout.php" style="margin: 0 10px 0 30px;">Sair</a>
			</div>
		</div>
	</div>
	
	<div id="header-middle">
		<?php  
			if(($_COOKIE['perm'] != '') && isset($_COOKIE['empr'])){
				if($_COOKIE['perm'] == 'fornecedor'){
					echo  mysql_result($fornecedorResult, 0, "CC_Fornecedor.razaoSocial");
				}else{
					echo  mysql_result($fornecedorResult, 0, "CC_Empresa_Dados.razaoSocial");
				}
				
				
				if($_COOKIE['perm'] == 'fornecedor'){ 
					if($_COOKIE['status_certificacao'] != ""){ 
						echo '<br/><span id="status" style="font-weight:bold; color:'.$audi_resultado_cor['_'.$_COOKIE['status_certificacao']].'">'.$audi_resultado['_'.$_COOKIE['status_certificacao']].'</span>';
					}else{
						echo '<br/><span id="status" style="font-weight:bold; color:#666;">N&atilde;o Certificado</span>';
					}
				}
			}
		?>
	</div>
					
	<div id="header-menu">
		<ul>
			
		
		<?php 
		
		if(isset($_COOKIE['empr']))
		{ // Se for cadastrado
			if($_COOKIE['perm'] == 'fornecedor')
			{
				$sql_subcontratados_forn = "Select fornecedor from CC_Fr where empresa = '{$_COOKIE['empr']}'";
		
				$query_subcontratados_forn = mysql_query($sql_subcontratados_forn);
				$fornecedores_permitidos = array($_COOKIE['empr']);
				while($rst_subcontratados_forn = mysql_fetch_assoc($query_subcontratados_forn)){
					$fornecedores_permitidos[] = $rst_subcontratados_forn['fornecedor'];
				}
				if(!isset($frid) || !in_array($frid,$fornecedores_permitidos)){
					$frid = $_COOKIE['empr'];
				}
				if(mysql_result($fornecedorResult, 0, 'pAcesso') != '1')//Se j� tiver trocado a senha
				{
					echo '<li><a href="home.php">In&iacute;cio</a></li>';
				
					if($_COOKIE['passoSistema'] >= '1'){
						echo '<li><a href="cadastroatualizar.php">Cadastro</a></li>';
					}
					
					if($_COOKIE['tipoFR']==='1'){
						if($_COOKIE['passoSistema'] >= '2'){
							echo '<li><a href="subcontratado.php?ac=listagem">Subcontratados</a></li>';
						}
					}
					if($_COOKIE['passoSistema'] >= '3'){
						echo '<li><a href="documentos.php" class="dpop" title="Insira os documentos solicitados para ter acesso � escolha de certificadoras e auditorias.">Documentos</a></li>';
					}
					
					//Verifica se possui vinculo com varejista
					$sql = "SELECT * FROM CC_Fr_Vinculo WHERE fornecedor = '".$_COOKIE['empr']."' AND empresaTipo = 'Varejista' AND ativo = '1' AND ( status = '2' OR  status = '3')";
					$query = mysql_query($sql);
					
					if((($_COOKIE['passoSistema'] >= '4') && ((mysql_num_rows($query) > 0) || ($_COOKIE['tipoFR'] === '2'))) || ($_COOKIE['passoSistema'] >= '3' && $_COOKIE['tipoFR'] === '2'))
					{
						echo '<li><a href="auditoria.php" class="dpop" title="Acesso �s auditorias e certificado">Auditorias</a></li>';
						
						$sql = "SELECT Audi_Fornecedor.audi, Audi_Fornecedor.pesquisa
								FROM Audi_Certificado 
									JOIN Audi_Fornecedor ON Audi_Certificado.audi = Audi_Fornecedor.audi
									JOIN CC_Fr ON CC_Fr.fornecedor = Audi_Certificado.fornecedor AND CC_Fr.empresa = '".$_COOKIE['empresa']."'
								WHERE 
									Audi_Certificado.empresa = '".$_COOKIE['empresa']."' AND Audi_Certificado.fornecedor = '".$_COOKIE['empr']."' 
								ORDER BY Audi_Certificado.data DESC 
								LIMIT 1";
						$result = mysql_query($sql) or die(mysql_error());
						
						if(mysql_num_rows($result)>0)
						{
							$audi = mysql_result($result,0,"Audi_Fornecedor.audi");
							if(''.mysql_result($result,0,"Audi_Fornecedor.pesquisa")=='1')
							{
								echo '<li><a href="certificado.php" target="_blank">Certificado</a></li>';
							}
							else
							{
								echo '<li><a href="auditoria.php?ac=pesquisa&audi='.$audi.'&frid='.$_COOKIE['empr'].'&tipo=" class="dpop" title="Para ter acesso ao certificado, por favor, responda � pesquisa">Pesquisa</a></li>';
							}
						}
						mysql_free_result($result);
					}
					
					echo '<li><a href="fornecimento.php">Fornecimento</a></li>';
				}//Fim pAcesso
			}
			elseif($_COOKIE['perm'] == 'Administradora')
			{
				
				echo '<li><a href="home.php">In&iacute;cio</a></li>';
				
				echo '<li><a href="fornecedores.php?ac=fornecedores">Fornecedores</a></li>';
				
				echo '<li><a href="usuarios.php">Usu&aacute;rios</a></li>';
				
				echo '<li><a href="usuarios.php?ac=listagem_for_adm">Usu&aacute;rios de Fornecedores</a></li>';
				
				echo '<li><a href="usuarios.php?ac=listagem_adm">Usu&aacute;rios Associados</a></li>';
				
				echo '<li><a href="relatorios.php">Relat&oacute;rios</a></li>';
			
			}
			else
			{   // Se usu�rio diferente de Auditor de Varejista
				if($_COOKIE['nivel'] != '3')
				{
				
					echo '<li><a href="home.php">In&iacute;cio</a></li>';
					
					// Se igual � Administrador
					if($_COOKIE['nivel'] == '1'){
						echo '<li><a href="cadastroatualizar.php">Cadastro</a></li>';
					}
					
					echo '<li><a href="fornecedores.php?ac=fornecedores">Fornecedores</a></li>';
					
					// Se igual � Administrador
					if($_COOKIE['nivel'] == '1'){
						echo '<li><a href="usuarios.php">Usu&aacute;rios</a></li>';
					}
					
					if($_COOKIE['perm'] == 'Varejista'){
						echo '<li><a href="consultar.php">Consultar</a></li>';
					}
				}
				
				echo '<li><a href="relatorios.php">Relat&oacute;rios</a></li>';
			}
		}
		else
		{ // Se não for cadastrado
				echo '<li><a href="cadastro.php">Cadastro</a></li>';
		}
		
		?>
		</ul>
	</div>

</div>