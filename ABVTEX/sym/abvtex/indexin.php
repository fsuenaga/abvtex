<?PHP
/*
	Dividir Login em 4 partes -> CC_Usuario
	
		- Administrador (ABVTEX) -> CC_Empresa -> Acesso por email
		
		- Fornecedor/Subcontratado (mesmo esquema de login) -> CC_Fornecedor/CC_Fr -> Acesso por CNPJ/N� de Identifica��o/Email
		
		- Certificadora (Compara��o Empresa/Empresa) -> CC_Associacao/CC_empresa -> Acesso por email
		
		- Varejista (Compara��o Empresa/Empresa) -> CC_Associacao/CC_empresa -> Acesso por email
	
*/

//remove o cache
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

# CODIGO DE VALIDA��O DO USU�RIO
require_once("key.inc");
require_once("../functions.inc");
$empresa = 'abvtex';

//tratar entrada
$not_allowed = array("DROP ","ONCLICK","ONMOUSEOVER","ONMOUSEOUT","ONLOAD","<SCRIPT","JAVASCRIPT","\\","'","\"");

$permitido = 0;

if(($_COOKIE["user"] == "")||($_COOKIE["pass"] == "")||($_COOKIE["empr"] == "")||("".$_POST["user"]!="")||("".$_POST["pass"]!=""))
{
	// Destroi COOKIES
	setcookie("user","",time()+1,'/sym/abvtex/');
	setcookie("pass","",time()+1,'/sym/abvtex/');
	setcookie("empr","",time()+1,'/sym/abvtex/');
	setcookie("empresa","",time()+1,'/sym/abvtex/');
	
	$separator = '.';
	$user = trim($_POST["user"]);
	$user = str_replace($not_allowed,"",$user);
	$pass = trim($_POST["pass"]);
	$pass = str_replace($not_allowed,"",$pass);
	$ppss = "".passCodefy($pass);
	
	
	if(($_POST["user"] == "")||($_POST["pass"] == "")) { header("Location: index.php?ern=3"); exit(0); }

	
	
	// Usu�rios das Certificadoras, Varejistas, 
	if(strpos($user,"@")!==false) { // Login por Email
		$query = "SELECT * FROM CC_Usuario 
					LEFT JOIN CC_Associacao ON CC_Associacao.associado = CC_Usuario.empresa
				WHERE 
					CC_Usuario.email = '".$user."' 
					AND CC_Usuario.pass = '".$pass."' 
				LIMIT 1";
		$result = mysql_query($query) or die ("Erro na busca de usu�rio - ".mysql_error());
		if(mysql_num_rows($result) > 0){
			
			$empresa  = mysql_result($result,0,'CC_Associacao.empresa');
			$user     = mysql_result($result,0,'CC_Usuario.user');
			$nomeUser = mysql_result($result,0,'CC_Usuario.nome');
			$perm     = mysql_result($result,0,'CC_Associacao.tipo');
			$empr     = mysql_result($result,0,'CC_Usuario.empresa');
			$nivel     = mysql_result($result,0,'CC_Usuario.nivel');
			$permitido = 1;
		}
	}
	else // Login por CNPJ ou por ID do fornecedor
	{
		$cnpj_sem_pont = str_replace(array('.','/', '-'),"", $user);
		
		
		
		$query = "SELECT 
					* 
				  FROM CC_Fornecedor 
					JOIN CC_Fr         ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor AND CC_Fr.empresa = '".$empresa."'
					JOIN CC_Usuario_Fr ON CC_Usuario_Fr.fornecedor = CC_Fr.fornecedor
				  WHERE 
					( (CC_Fornecedor.CNPJ = '".$user."' OR CC_Fornecedor.CNPJ = '".$cnpj_sem_pont."') OR CC_Fornecedor.fornecedor = '".$user."')
					AND CC_Usuario_Fr.pass = '".$pass."' 
				  LIMIT 1";
		$result = mysql_query($query) or die ("Erro na busca de usu�rio - ".mysql_error());
		if(mysql_num_rows($result) > 0){
			$empresa  = mysql_result($result,0,'CC_Fr.empresa');
			$empr     = mysql_result($result,0,'CC_Fr.fornecedor');
			$user     = mysql_result($result,0,'CC_Usuario_Fr.user');
			$nomeUser = mysql_result($result,0,'CC_Usuario_Fr.nome');
			$passoSistema   = mysql_result($result,0,'CC_Fr.passo_sistema');
			$status_certificacao   = mysql_result($result,0,'CC_Fr.status_certificacao');
			$perm     = 'fornecedor';
			$permitido = 1;
			setcookie("status_certificacao" , mysql_result($result,0,'CC_Fr.status_certificacao'), time()+72000, $cookie_path);
			setcookie("passoSistema"        , mysql_result($result,0,'CC_Fr.passo_sistema')      , time()+72000, $cookie_path);
			setcookie("tipoFR"              , mysql_result($result,0,'CC_Fr.classe')             , time()+72000, $cookie_path);
		}
	}
	
	//Se for validada e permitida a entrada do usu�rio
	if($permitido == 1)
	{
		setcookie("empr"    			,$empr         				,time()+7200,'/sym/abvtex/');
		setcookie("empresa" 			,$empresa      				,time()+7200,'/sym/abvtex/');
		setcookie("pass"    			,$ppss         				,time()+7200,'/sym/abvtex/');
		setcookie("user"    			,$user         				,time()+7200,'/sym/abvtex/');
		setcookie("perm"    			,$perm         				,time()+7200,'/sym/abvtex/');
		setcookie("nomeUser"			,$nomeUser     				,time()+7200,'/sym/abvtex/');
		setcookie("passoSistema"		,$passoSistema    			,time()+7200,'/sym/abvtex/');
		setcookie("status_certificacao"	,$status_certificacao  		,time()+7200,'/sym/abvtex/');
		setcookie("sess"				,sessionCodefy()    		,time()+7200,'/sym/abvtex/');
		
		if($perm != 'fornecedor'){
			setcookie("nivel",$nivel,time()+72000, '/sym/abvtex/');
		}
		
		if($_GET['lc'] == 'crtfd'){
			header("Location: ".$_GET['pg'].".php?ac=resumo&frid=".$empr."&audi=".$_GET['audi']); exit(0);
		}elseif($_GET['lc'] == 'psqs'){
			header("Location: ".$_GET['pg'].".php?ac=resumo&frid=".$empr."&audi=".$_GET['audi']); exit(0);
		}else{
			header("Location: home.php"); exit(0);
		}
		
	} else {
		header("Location: index.php?ern=3"); exit(0);
	}
}
?>