<?php
	if($_COOKIE["perm"]!="Administradora") { echo('<script> history.back(); </script>'); exit(0); }
	
	require_once("connect.inc");
	include_once("functions.php");
    require_once("libs/date.inc");
	require_once("libs/lib.log.php");
	require_once("libs/lib.default_colors.php");
	
	$acao       = ''.$_GET['ac'];
	$acao_model = ''.$_GET['mac'];
	$doc        = intval(0+$_GET['doc']);
	$cat        = intval(0+$_GET['cat']);
	
	$sql = "SELECT * FROM CC_Fr_Categoria WHERE 1 ORDER BY nome ";
	$res_cat = mysql_query($sql) or die(mysql_error()); $arr_categorias = array();
	while($row_cat = mysql_fetch_assoc($res_cat)) $arr_categorias[$row_cat["categoria"]] = $row_cat["nome"];
	$num_categorias = sizeof($arr_categorias);
	$yn = array('N�o','Sim');
	
	if($acao_model!='')
	{
		switch($acao_model){
			case 'docadd'   : include_once('modules/adm/documentos/doc_model_add.php');        break;
			case 'docupd'   : include_once('modules/adm/documentos/doc_model_edit.php');       break;
			case 'doccatcng': include_once('modules/adm/documentos/doc_model_cat_change.php'); break;
		}
	}
	
	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/visibility.js"></script>
		<!-- FIM JS -->
	</head>

	<body>
		<div class="content">
			
			<!-- HEADER -->
				<?php include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			<?php
				switch($acao){
					case '':          include_once('modules/adm/documentos/doc_list.php'); break;
					case 'docnew':    include_once('modules/adm/documentos/doc_add.php');  break;
					case 'docedt':    include_once('modules/adm/documentos/doc_edit.php'); break;
				}
			?>
		</div>
	</body>
</html>