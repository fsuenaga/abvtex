<?PHP
	/**
	*
	*  LIB: symUploader
	*
	*  Pode ser instanciado em qualquer parte do c�digo
	*  
	*  @Author: Gustavo M. Monteiro
	*  
	*/
require_once($syspath.'libs/class.symFilesystem.php');

class symUploader {
	
	/** $_COOKIE["empresa"] */
	var $empresa			= "";
	/** $_COOKIE["norma"] */
	var $norma				= "";
	/** o separador utilizado no sistema operacional */
	var $OS_separator		= "/";
	/** As pastas padr�o do sistema e de onde s�o armazenados os arquivos */
	var $sys_driver			= "E:";
	var $sys_folder			= "/sym/abvtex";
	var $sys_files_folder	= "/uploads";
	var $create_folder_lock	= false;
	var $input_verify_lock	= false;
	/** A tabela do sistema que ir� trabalhar */
	var $table				= "SYM_Documento";
	/** m�ximo de bytes permitido (1MB) */
	var $max_size			= 1291456;
	/** o nome da vari�vel do $_FILES */
	var $post				= "";
	/** o tipo do documento / opera��o / armazenamento dele */
	var $operation_type		= "";
	var $operation_subtype	= "";
	/** o diret�rio onde ele ir� */
	var $upload_dir 		= "";
	/** o ID do documento */
	var $id					= "";
	var $table_key			= "";
	var $table_subkey		= "";
	/** o diret�rio do arquivo */
	var $file_dir 			= "";
	/** o nome do arquivo */
	var $file_name			= "";
	/** a extens�o do arquivo */
	var $file_extension		= "";
	/** o tipo MIME do arquivo */
	var $file_type			= "";
	/** se o arquivo for uma imagem */
	var $img_width			= 0;
	var $img_height			= 0;
	var $img_type			= "";
	/** um array com as extens�es permitidas */
	var $allowedExtensions	= array();
	/** cont�m todos os erros concatenados */
	var $erros        		= "";
	var $erros_count        = 0;
	
	/** 0=>BD , 1=>FILE SYSTEM */
	var $upload_style		= 1;
	var $data				= '';
	
	/** Data de Validade */
	var $validade			= 'NULL';
	
	/** o status da opera��o da classe */
	var $status             = "Em espera";
	
	/** cont�m as cnfigura��es por operacao */
	var $operation  = "";
	var $operations = array(
		"_doc"			=> "DOC", //DOC para documentos padr�o do sistema
		"_docs"			=> "DOC", 
		"_audi"			=> "AUDI",//AUDI para arquivos de auditoria
		"_auditoria"	=> "AUDI", 
		"_check"		=> "CK",  //CK para Checklists do sistema
		"_checklist"	=> "CK", 
		"_fr"			=> "FR",  //FR para pastas de fornecedores
		"_fornecedor"	=> "FR", 
		"_fornecedores"	=> "FR", 
		"_oc"			=> "OC",  //OC para pastas de uploads de ocorr�ncias e evid�ncias relacionadas
		"_ocorrencia"	=> "OC",
		"_ocorrencias"	=> "OC"
		);
	/** cont�m as cnfigura��es por sub-operacao */
	var $sub_operation  = "";
	var $sub_operations = array();
	
	/**
	* Fun��o symUploader
	* 
	* cria a classe symUploader
	* 
	* @param $table_key 		= a chave da tabela para recupera��o / ID do fornecedor
	* @param $table_subkey 		= a sub-chave da tabela para recupera��o
	* @param $post 				= o nome da vari�vel do $_FILES
	* @param $operation_type 	= o tipo de documento que ser� processado
	* @param $operation_subtype = o sub-tipo de documento que ser� processado
	* @param $allowedExtensions = uma string com as extens�es permitidas, separadas por ","
	*/
	
	function symUploader($table_key, $table_subkey, $post, $allowedExtensions="", $operation_type="", $operation_subtype="", $validade = "NULL")
	{
		//carregando sistema de arquivos
		$file_system = new symFilesystem();
		$this->OS_separator     = $file_system->getSeparator();
		$this->sys_driver       = $file_system->getSysDriver();
		$this->sys_folder       = $file_system->getSysFolder();
		$this->sys_files_folder = $file_system->getSysFilesFolder();
		
		//estabelecendo vari�veis
		$this->empresa			 = strtolower($_COOKIE["empresa"]);
		$this->norma			 = strtolower($_COOKIE["hall"]);
		$this->post				 = $post;
		$this->table_key		 = "".$table_key;
		$this->table_subkey		 = "".$table_subkey;
		$this->operation_type	 = "_".strtolower($operation_type);
		$this->operation_subtype = "_".strtolower($operation_subtype);
		if(($validade != 'NULL') && ($validade != "")){
			$this->validade 	 = $validade;
		}
		
		
		//estabelecendo c�lculos iniciais
		$this->operation     = $this->operations[$this->operation_type];
		if(isset($this->sub_operations[$this->operation_subtype]))
			$this->sub_operation = $this->sub_operations[$this->operation_subtype];
		else
			$this->sub_operation = substr($this->operation_subtype,1);
		
		if($allowedExtensions!="")
		{
			$this->allowedExtensions = explode(",",$allowedExtensions);
			for($i=0;$i<sizeof($this->allowedExtensions);$i++)
				$this->allowedExtensions[$i] = strtolower(trim(str_replace(".","",$this->allowedExtensions[$i])));
			sort($this->allowedExtensions);
		}
	}
	
	/**
	* Fun��o addExtension
	* 
	* adiciona uma extens�o de arquivo aos arquivos permitidos
	*/
	function addExtension($ext)
	{
		$aus = strtolower(trim(str_replace(".","",$ext)));
		if(!in_array($aus))
			array_push($this->allowedExtensions,$aus); sort($this->allowedExtensions);
	}
	
	/** Fun��o setID -  atribui o valor da variavel */
	function setEmpresa($empr)    { $this->empresa = $empr; }
	
	/** Fun��o setID -  atribui o valor da variavel */
	function setFornecedor($frnc) { $this->table_key = $frnc; }
	
	/** Fun��o setID -  atribui o valor da variavel */
	function setNorma($norm)      { $this->norma   = $norm; }
	
	/** Fun��o setID -  atribui o valor da variavel */
	function setID($id)           { $this->id      = $id;   }
	
	/** Fun��o setOperation -  atribui o valor da operan��o */
	function setOperation($op) { $this->operation_type = "_".strtolower($op); }
	
	/** Fun��o setID -  atribui o valor da variavel */
	function setSuboperation($op) { $this->operation_subtype = "_".strtolower($op); }
	
	function addError($str)
	{
		$this->status = "Falha";
		$this->erros_count++; 
		$this->erros .= $str.'
';
	}
	
	/**
	* Fun��o valuesVerify
	* 
	* Verifica os valores de entrada antes de come�ar a procssar os dados
	*/
	function valuesVerify()
	{
		//verifica o arquivo / post / entrada
		if(!$this->input_verify_lock)
		{
			if (isset($_FILES[$this->post]) && $_FILES[$this->post]['error'] != 4)
			{
				$max = intval($_POST["MAX_FILE_SIZE"])/1024;
				$uperrors=array(1 => 'Erro: O tamanho m�ximo do arquivo foi excedido\n(Tamanho m�ximo '.$max.'KB).',
								2 => 'Erro: O tamanho m�ximo do arquivo foi excedido\n(Tamanho m�ximo '.$max.'KB).',
								3 => 'Erro: O arquivo foi transferido apenas parcialmente.',
								4 => 'Erro: N�o h� arquivo anexo.');
				if ($_FILES[$this->post]['error'] != 0) { $this->addError($uperrors[$_FILES[$this->post]['error']]); return false; } //erro
				
				// pega atibutos se for imagem
				@list($this->img_width, $this->img_height, $this->img_type, ) = getimagesize($_FILES[$this->post]['tmp_name']);
				
				// pega atributos do arquivo
				$this->file_name 	  = $_FILES[$this->post]['name'];
				$this->file_extension = strtolower(substr($_FILES[$this->post]['name'],strrpos($_FILES[$this->post]['name'],".")+1));
				$this->file_type 	  = $_FILES[$this->post]['type'];
				
				//ve se a extensao est� nos permitidos
				if(!in_array($this->file_extension,$this->allowedExtensions)) { $this->addError('Erro: Extens�o n�o permitida.'); return false; } //erro
			}
			else { $this->addError('Erro: N�o h� arquivo anexo ou n�o encontrada vari�vel POST.'); return false; } //erro
		}
		
		//verificando a opera��o e criando as pastas necess�rias
		//$upload_dir = dirname(__FILE__); //pega o path
		//$pos = strpos($upload_dir,$this->sys_folder); //remove o que tiver na frente do '/sym' em caso de ser chamado em subpastas
		//$upload_dir = substr($upload_dir,0,$pos+strlen($this->sys_folder)).$this->sys_files_folder;
		//$file_dir   = str_replace($this->OS_separator,"",$this->sys_files_folder);
		
		$upload_dir = $this->sys_driver.$this->sys_folder.$this->sys_files_folder;
		$file_dir   = $upload_dir;
		if($this->OS_separator!="/") $file_dir = str_replace($this->OS_separator,"/",$file_dir);
		
		//criando diret�rio - empresa
		$upload_dir.= $this->OS_separator.$this->empresa;
		$file_dir  .= '/'.$this->empresa;
		if($this->upload_style==1 && !is_dir($upload_dir) && !$this->create_folder_lock) mkdir($upload_dir,0777);
		
		//criando sub-diret�rio sob a chave, se necess�rio
		if("".$this->table_key!="")
		{
			// faz uma pasta com a primeira letra (n�mero) do fornecedor
			$first_letter = $this->table_key[0];
			$upload_dir .= $this->OS_separator.$first_letter;
			$file_dir   .= '/'.$first_letter;
			if($this->upload_style==1 && !is_dir($upload_dir) && !$this->create_folder_lock) mkdir($upload_dir,0777);
			
			// faz a pasta do fornecedor
			$upload_dir .= $this->OS_separator.$this->table_key;
			$file_dir   .= '/'.$this->table_key;
			if($this->upload_style==1 && !is_dir($upload_dir) && !$this->create_folder_lock) mkdir($upload_dir,0777);
		}
		
		//criando diret�rio - opera��o
		if("".$this->operation_type!="" && "".$this->operation_type!="_")
		{
			$upload_dir .= $this->OS_separator.$this->operations[$this->operation_type];
			$file_dir   .= '/'.$this->operations[$this->operation_type];
			if($this->upload_style==1 && !is_dir($upload_dir) && !$this->create_folder_lock) mkdir($upload_dir,0777);
		}
		
		//criando sub-diret�rio, se necess�rio
		if("".$this->operation_subtype!="" && "".$this->operation_subtype!="_")
		{
			if(isset($this->sub_operations[$this->operation_subtype]))
			{
				$upload_dir .= $this->OS_separator.$this->sub_operations[$this->operation_subtype];
				$file_dir   .= '/'.$this->sub_operations[$this->operation_subtype];
			}
			else
			{
				$upload_dir .= $this->OS_separator.substr($this->operation_subtype,1);
				$file_dir   .= '/'.$this->sub_operations[$this->operation_subtype];
			}
			if($this->upload_style==1 && !is_dir($upload_dir) && !$this->create_folder_lock) mkdir($upload_dir,0777);
		}
		
		$this->upload_dir = $upload_dir.$this->OS_separator;
		$this->file_dir   = $file_dir."/";
		
		if(!$this->input_verify_lock)
		{
			// verificando o tamanho
			$data = file_get_contents($_FILES[$this->post]['tmp_name']); 
			if(sizeof($data)>$this->max_size) { $this->erros .= 'Erro: limite de arquivo excedido ('.$this->max_size.' bytes).\n'; return false; } //erro
			if($this->upload_style==1) unset($data); //se for upload para pasta, limpa a variavel (pq ser� usado no processo a seguir)
			else                       $this->data = addslashes($data); //senao salva para colocar na tabela
		}
		return true;
	}
	
	/**
	* Fun��o upload
	* 
	* Sobe o arquivo sob os par�metros especificados
	*/
	function upload()
	{
		//verifica os valores de entrada
		$test = $this->valuesVerify();
		if(!$test) { $this->addError('-- Ocorreram '.$this->erros_count.' erros, opera��o abortada --'); return false; } //erro
		
		if("".$this->id=="")
		{
			$max = 15; $prefix = ''; $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789'; $sep = '';
			do {
				$i=0; $chave = "";
				for ($j = strlen($prefix); $j < $max; $j++) { $pos = rand(0, 34); $chave .= $chars[$pos]; }
				$chave = $prefix.$sep.$chave;
				$linhas = mysql_num_rows(mysql_query("SELECT DOC_ID FROM ".$this->table." WHERE DOC_ID = '".$chave."' LIMIT 1 "));
			} while($linhas > 0);
			$this->id = $chave;
		}
		
		//upload
		if($this->upload_style==1)
		{
			if(move_uploaded_file($_FILES[$this->post]['tmp_name'],$this->upload_dir.$this->id.'.dat'))
			{
				$sql = "INSERT INTO SYM_Documento (`empresa`,`norma`,`DOC_ID`,`chave`,`subchave`,`arquivo`,`extensao`,`caminho`,`width`,`height`,`operation`,`sub_operation`, `validade`) VALUES (	
						'".$this->empresa."',
						'".$this->norma."',
						'".$this->id."',
						'".$this->table_key."',
						'".$this->table_subkey."',
						'".$this->file_name."',
						'".$this->file_extension."',
						'".addslashes($this->upload_dir)."',
						'".(0+intval($this->img_width))."',
						'".(0+intval($this->img_height))."',
						'".$this->operation."',
						'".$this->sub_operation."',
						".(($this->validade != 'NULL')?" '".$this->validade."' ":"NULL")."
						)";
				mysql_query($sql) or $this->erros .= mysql_error().'\n';
				
				return true;
			}
			else //deu erro no upload
			{
				switch ($_FILES[$this->post]['error']) {
					case 1 : $this->erros .= 'O arquivo � maior do que a instala��o do PHP permite'; break;
					case 2 : $this->erros .= 'O arquivo � maior do que a aplica��o POST permite'; break;
					case 3 : $this->erros .= 'Apenas parte do arquivo foi transferida'; break;
					case 4 : $this->erros .= 'Nenhum arquivo foi anexado ou carregado'; break;
					default: $this->erros .= 'Erro desconhecido';
				}
				return false;
			}
		}
		else if($this->upload_style==0)
		{
			$sql = "INSERT INTO SYM_Documento (`empresa`,`norma`,`DOC_ID`,`chave`,`subchave`,`arquivo`,`extensao`,`caminho`,`width`,`height`,`operation`,`sub_operation`,`file`, `validade`) VALUES (
			'".$this->empresa."',
			'".$this->norma."',
			'".$this->id."',
			'".$this->table_key."',
			'".$this->table_subkey."',
			'".$this->file_name."',
			'".$this->file_extension."',
			'".addslashes($this->upload_dir)."',
			'".(0+intval($this->img_width))."',
			'".(0+intval($this->img_height))."',
			'".$this->operation."',
			'".$this->sub_operation."',
			'".$this->data."',
			".(($this->validade != 'NULL')?" '".$this->validade."' ":"NULL")."
			)";
			mysql_query($sql) or $this->erros .= 'Arquivo - '.mysql_error().'\n';
			
			return true;
		}
		
		return true; /* TUDO CERTO */
	}
	
	/**
	* Fun��o update
	* 
	* Sobrescreve o arquivo sob os par�metros especificados
	*/
	function update()
	{
		//� necess�rio o ID estar atribu�do
		if("".$this->id=="") { $this->addError('Erro: o ID n�o est� atribu�do, n�o h� o que alterar.'); return false; } //erro
		
		$test = $this->valuesVerify();
		if(!$test) { $this->addError('-- Ocorreram '.$this->erros_count.' erros, opera��o abortada --'); return false; } //erro
		
		$sql = "SELECT * 
				FROM SYM_Documento 
				WHERE empresa = '".$this->empresa."' 
				AND    DOC_ID = '".$this->id."' 
				LIMIT 1 ";
		$result = mysql_query($sql) or die(mysql_error());
		$existe = (mysql_num_rows($result)>0);
		
		if($existe)
		{
			if($this->upload_style==1)
			{
				@unlink($this->upload_dir.$this->id.'.dat');
				
				//update
				if(move_uploaded_file($_FILES[$this->post]['tmp_name'],$this->upload_dir.$this->id.'.dat'))
				{
					$sql = "REPLACE INTO SYM_Documento (`empresa`,`norma`,`DOC_ID`,`chave`,`subchave`,`arquivo`,`extensao`,`caminho`,`width`,`height`,`operation`,`sub_operation`, `validade`) VALUES (	
							'".$this->empresa."',
							'".$this->norma."',
							'".$this->id."',
							'".$this->table_key."',
							'".$this->table_subkey."',
							'".$this->file_name."',
							'".$this->file_extension."',
							'".addslashes($this->upload_dir)."',
							'".(0+intval($this->img_width))."',
							'".(0+intval($this->img_height))."',
							'".$this->operation."',
							'".$this->sub_operation."',
							".(($this->validade != 'NULL')?" '".$this->validade."' ":"NULL")."
						)";
					mysql_query($sql) or die(mysql_error());
					
					return true;
				}
				else //deu erro no upload
				{
					switch ($_FILES[$this->post]['error']) {
						case 1 : $this->erros .= 'O arquivo � maior do que a instala��o do PHP permite'; break;
						case 2 : $this->erros .= 'O arquivo � maior do que a aplica��o POST permite'; break;
						case 3 : $this->erros .= 'Apenas parte do arquivo foi transferida'; break;
						case 4 : $this->erros .= 'Nenhum arquivo foi anexado ou carregado'; break;
						default: $this->erros .= 'Erro desconhecido';
					}
					return false;
				}
			}
			else if($this->upload_style==0)
			{
				$sql = "REPLACE INTO SYM_Documento (`empresa`,`norma`,`DOC_ID`,`chave`,`subchave`,`arquivo`,`extensao`,`caminho`,`width`,`height`,`operation`,`sub_operation`,`file`,`validade`) VALUES (	
						'".$this->empresa."',
						'".$this->norma."',
						'".$this->id."',
						'".$this->table_key."',
						'".$this->table_subkey."',
						'".$this->file_name."',
						'".$this->file_extension."',
						'".addslashes($this->upload_dir)."',
						'".(0+intval($this->img_width))."',
						'".(0+intval($this->img_height))."',
						'".$this->operation."',
						'".$this->sub_operation."',
						'".$this->data."',
						".(($this->validade != 'NULL')?" '".$this->validade."' ":"NULL")."
						)";
				mysql_query($sql) or die(mysql_error());
				
				return true;
			
			}
		}
		else return false;
		
		return true; /* TUDO CERTO */
	}
	
	/**
	* Fun��o delete
	* 
	* Apaga o arquivo sob os par�metros especificados
	*/
	function delete()
	{
		//� necess�rio o ID estar atribu�do
		if("".$this->id=="") { $this->addError('Erro: o ID n�o est� atribu�do, n�o h� o que alterar.'); return false; } //erro
		
		$this->input_verify_lock  = true; //trava para nao analisar os inputs
		$this->create_folder_lock = true; //trava para nao criar as pastas
		$test = $this->valuesVerify();
		if(!$test) { $this->addError('-- Ocorreram '.$this->erros_count.' erros, opera��o abortada --'); return false; } //erro
		
		$sql = "SELECT * 
				FROM SYM_Documento 
				WHERE empresa = '".$this->empresa."' 
				AND    DOC_ID = '".$this->id."' 
				LIMIT 1 ";
		$result = mysql_query($sql) or die(mysql_error());
		$existe = (mysql_num_rows($result)>0);
		
		if($existe)
		{
			if($this->upload_style==1)
			{
				unlink($this->upload_dir.$this->id.'.dat');
			}
			$sql = "DELETE
					FROM SYM_Documento 
					WHERE empresa = '".$this->empresa."' 
					AND    DOC_ID = '".$this->id."' 
					LIMIT 1 ";
			mysql_query($sql) or die(mysql_error());
			return true;
		}
		else return false;
		
		return true; /* TUDO CERTO */
	}
}
?>