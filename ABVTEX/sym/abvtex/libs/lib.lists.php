<?PHP
function createHTMLSelect($my_array,$option = "") {
	$aus = "";
	$option_arr = array();
	if(is_array($option)) $option_arr          = $option;
	else                  $option_arr[strtolower($option)] = 1;
	
	foreach($my_array as $key => $value) {$aus .= '<option value="'.$key.'"'.($option!=""&&isset($option_arr[strtolower($key)])?' selected':'').'>'.$value.'</option>
';}
	return $aus;
}
?>