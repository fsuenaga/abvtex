<?PHP
	/**
	*
	*  LIB: symFilesystem
	*
	*  Pode ser instanciado em qualquer parte do c�digo
	*  
	*  @Author: Gustavo M. Monteiro
	*  
	*/

class symFilesystem {
	
	/** o separador utilizado no sistema operacional */
	private $OS_separator		= "\\";
	private $sys_driver			= "E:";
	private $sys_folder			= "\\sym\\abvtex";
	/** As pastas padr�o do sistema e de onde s�o armazenados os arquivos */
	private $sys_files_folder	= "\\uploads";
	
	private $status				= "Em espera";
	
	/**
	* Fun��o symFilesystem
	* 
	* cria a classe symFilesystem
	* 
	*/
	function symFilesystem()
	{
		$this->sys_driver .= $this->OS_separator."inetpub".$this->OS_separator."wwwroot";
		$this->status = 'Carregado';
	}
	
	function getSeparator()      { return $this->OS_separator;     }
	function getSysDriver()      { return $this->sys_driver;       }
	function getSysFolder()      { return $this->sys_folder;       }
	function getSysFilesFolder() { return $this->sys_files_folder; }
	function getStatus()         { return $this->status;           }
}
?>