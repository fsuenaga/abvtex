<?PHP
	/**
	*
	*  LIB: WORKFLOW
	*
	*  Requer connect.inc previamente carregado
	*  Pode ser instanciado em qualquer parte do c�digo
	*  
	*  @Author: Gustavo M. Monteiro
	*  @Version: 1.0.0
	*  @Date: 27/04/2012
	*  
	*/
if(!isset($syspath)) $syspath = '../../';
	
//require_once($syspath.'mail/class.phpmailer.php'); // j� � carregado pelo sendMail
require_once($syspath.'libs/workflow/class.workflow_evento.php');
require_once($syspath.'libs/lib.sendMail.php');

class Workflow {
	
	/** $_COOKIE["empresa"] */
	var $empresa       = "";
	
	/** ID do Workflow */
	var $id            = "";
	
	/** ID do Evento */
	var $evento_id     = 0;
	/** Objeto do Evento */
	var $evento        = null;
	
	/** chave prim�ria do Workflow */
	/** chave prim�ria quebra em arrays, separada pelo separator */
	var $chave         = "";
	var $chave_array   = array();
	var $chave_separator = "~";
	/** referencia com informa��es pro email */
	/** referencia quebra em arrays */
	var $referencia       = "";
	var $referencia_array = array();
	
	/** porcentagem de completude do Workflow, de 0 a 100 */
	var $completo      = 0;
	
	/** datas */
	var $data_inicio   = "";
	var $data_fim      = "";
	var $data_c1       = "";
	var $data_c2       = "";
	var $data_c3       = "";
	
	/** endere�o de email para enviar */
	var $enviar_email  = "";
	/** nome do dono do email para enviar */
	var $enviar_nome   = "";
	/** assunto do email */
	var $email_assunto = "";
	/** corpo do email */
	var $email_corpo   = "";
	
	/** se o workflow est� carregado */
	var $load          = false;
	/** cont�m todos os erros concatenados */
	var $erros         = "";
	
	/**
	* Fun��o Workflow
	* 
	* Construtor da Classe
	*/
	function Workflow()
	{
		$this->empresa = strtolower($_COOKIE["empresa"]);
	}
	
	/**
	* Fun��o carregarEvento
	* 
	* Cria o objeto de Evento e o instancia ao workflow
	*
	* @param $evento_id = o ID do evento de workflow, se quiser, caso contr�rio ele pega o carregado da pr�pria classe
	* @return true on success
	*/
	function carregarEvento($evento_id=0)
	{
		if("".$evento_id!="" && "".$evento_id!="0") $this->evento_id = $this->procurarEvento($evento_id);
		else if($evento_id!=0)                      $this->evento_id = $evento_id;
		
		$this->evento = new Workflow_Evento($this->evento_id);
		$this->chave_separator = $this->evento->chave_separator;
		
		return $this->evento->carregado();
	}
	
	/**
	* Fun��o carregar
	* 
	* Pega o �ltimo workflow com o evento e chave designados
	* 
	* @param $evento_id = o ID do evento de workflow
	* @param $chave     = a chave prim�ria do evento
	* @param $aberto    = se true s� carrega se for o ultimo aberto
	* @return true on success
	*/
	function carregar($evento_id,$chave,$aberto=false)
	{
		$this->erros = "";
		if(!$this->carregarEvento($evento_id)) { $this->erros .= "N�o conseguiu carregar o objeto evento. "; return false; }
		$this->chave     = $chave;
		$sql = "SELECT * FROM CC_Workflow 
				WHERE evento = '".$this->evento_id."' 
				AND  empresa = '".$this->empresa."' 
				AND   chave  = '".$this->chave."' 
				".($aberto?" AND completo < 100 ":"")."
				ORDER BY data_inicio DESC 
				LIMIT 1 ";
		$res = mysql_query($sql) or die(mysql_error());
		
		if(mysql_num_rows($res)<=0) { $this->erros .= "N�o pode carregar o evento. "; return false; }
		
		$this->id          = intval($id);
		if(strpos($this->chave,$this->chave_separator)!==false) $this->chave_array = explode($this->chave_separator,$this->chave);
		$this->referencia  = mysql_result($res,0,"referencia");
		$this->carregarReferencia();
		$this->completo    = mysql_result($res,0,"completo");
		$this->data_inicio = mysql_result($res,0,"data_inicio");
		$this->data_fim    = mysql_result($res,0,"data_fim");
		$this->data_c1     = mysql_result($res,0,"data_c1");
		$this->data_c2     = mysql_result($res,0,"data_c2");
		$this->data_c3     = mysql_result($res,0,"data_c3");
		$this->load        = true;
		return true;
	}
	
	/**
	* Fun��o carregar
	* 
	* Pega o workflow pelo ID passado
	* 
	* @param $id  = o D do CC_Workflow
	* @param $all = true por padr�o, j� carrega dos as informa��es do Workflow na classe
	* @return true on success
	*/
	function carregarID($id,$all=true)
	{
		$this->id = intval($id);
		
		if($all)
		{
			$this->erros = "";
			$sql = "SELECT * FROM CC_Workflow WHERE id = '".$this->id."' LIMIT 1 ";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res)<=0) { $this->erros .= "N�o pode carregar o evento. "; return false; }
			
			$this->evento_id   = mysql_result($res,0,"evento");
			if(!$this->carregarEvento()) { $this->erros .= "N�o conseguiu carregar o objeto evento. "; return false; }
			$this->chave       = mysql_result($res,0,"chave");
			if(strpos($this->chave,$this->chave_separator)!==false) $this->chave_array = explode($this->chave_separator,$this->chave);
			$this->referencia  = mysql_result($res,0,"referencia");
			$this->carregarReferencia();
			$this->completo    = mysql_result($res,0,"completo");
			$this->data_inicio = mysql_result($res,0,"data_inicio");
			$this->data_fim    = mysql_result($res,0,"data_fim");
			$this->data_c1     = mysql_result($res,0,"data_c1");
			$this->data_c2     = mysql_result($res,0,"data_c2");
			$this->data_c3     = mysql_result($res,0,"data_c3");
		}
		$this->load = true;
		return true;
	}
	
	/**
	* Fun��o procurarEvento
	* 
	* Procura pelo Evento no banco dw dados
	*
	* @param $evento_nome = o nome do evento, ser� achado se o Nome ou Codigo coincidir com o par�metro passado
	* @return o ID do evento
	*/
	function procurarEvento($evento_nome)
	{
		$aus = trim(str_replace("'","",str_replace("\\","",$evento_nome)));
		$sql = "SELECT evento FROM CC_Workflow_Evento 
				WHERE nome = '".$aus."' OR codigo = '".$aus."' LIMIT 1 ";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res)<=0) { $this->erros .= "N�o conseguiu encontrar o workflow_evento. "; return false; }
		else                        return mysql_result($res,0,"evento");
	}
	
	
	/**
	* Fun��o ciar
	* 
	* Criar um novo Workflow para o devido evento e chave
	* 
	* @param $evento = mixed, pode ser o Objeto do Evento, o ID do Evento ou o Nome/C�digo
	* @param $chave  = a chave prim�ria do workflow
	* @param $criar_novo_com_antigo_aberto = false por padr�o, n�o permite criar um novo se o antigo com o mesmo evento-chave estiver aberto
	* @return true on success
	*/
	function criar($evento="",$chave="",$criar_novo_com_antigo_aberto=false)
	{
		//colocando evento
		if($this->evento_id==0 || $this->evento==null)
		{
			if(is_object($evento))
			{
				$this->evento    = $evento;
				$this->evento_id = $evento->evento_id;
				$this->chave_separator = $this->evento->chave_separator;
			}
			else
			{
				if(0+intval($evento)<=0) $this->evento_id = $this->procurarEvento($evento);
				else 					 $this->evento_id = intval($evento);
				if(0+intval($this->evento_id)==0) { $this->erros .= "N�o existe evento '0', n�o foi poss�vel carregar. "; return false; }
				
				if(!$this->carregarEvento()) { $this->erros .= "N�o conseguiu carregar o objeto evento. "; return false; }
			}
		}
		
		
		if("".$chave!="")
			$this->chave = trim(str_replace("'","",str_replace("\\","",$chave)));
		if("".$this->chave=="")
		{ $this->erros .= "A chave principal do Workflow n�o foi designada. "; return false; }
		
		if(!$criar_novo_com_antigo_aberto)
		{
			$sql = "SELECT * FROM CC_Workflow 
					WHERE evento = '".$this->evento_id."' 
					AND  empresa = '".$this->empresa."'
					AND   chave  = '".$this->chave."'
					AND completo < 100 
					LIMIT 1 ";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res)>0) { $this->erros .= "Existe um workflow anterior aberto. "; return false; }
		}
		
		$sql = "INSERT INTO CC_Workflow "; 
		$sql_fields = "evento,empresa,chave";
		$sql_values = "'".$this->evento_id."','".$this->empresa."','".$this->chave."'";
		if("".$this->data_inicio!="")
		{
			$sql_fields .=",data_inicio";
			$sql_values .=",'".$this->data_inicio."'";
		}
		if("".$this->referencia!="")
		{
			$sql_fields .=",referencia";
			$sql_values .=",'".$this->referencia."'";
		}
		$sql .= "(".$sql_fields.") VALUES (".$sql_values.") ";
		
		$res = mysql_query($sql) or die(mysql_error());
		$this->id = mysql_insert_id();
		$this->load = true;
		
		return true;
	}
	
	/**
	* Fun��o enviarEmail
	* 
	* Envia o email segundo os parametros montados
	* 
	* @return true on success
	*/
	function enviarEmail($email,$nome,$chamada=0,$send_without_loading=false)
	{
		if(!$send_without_loading && !$this->load) { $this->erros .= "N�o pode enviar sem carregar o evento. "; return false; }
		
		if(is_null($this->evento))
		{
			if(!$this->carregarEvento()) { $this->erros .= "N�o pode enviar sem carregar o evento. "; return false; } 
		}
		
		$this->enviar_email = trim(str_replace(array(";",","),"",$email));
		$this->enviar_nome  = trim($nome);
		
		if($this->email_corpo=="" && $this->email_assunto=="")
		{
			if($chamada<=1)
			{ $Subject = $this->evento->assunto1; $Body = $this->evento->email1; }
			else if($chamada==2)
			{ $Subject = $this->evento->assunto2; $Body = $this->evento->email2; }
			else if($chamada==3)
			{ $Subject = $this->evento->assunto3; $Body = $this->evento->email3; }
		}
		else
		{ $Subject = $this->email_assunto; $Body = $this->email_corpo; }
		
		foreach($this->referencia_array as $key => $value)
		{
			$Body = str_replace("[key".$key."]",$value,$Body);
		}
		
		if(!sendMail($this->enviar_nome, $this->enviar_email, $Body, $Subject))
		{ 
			$this->erros .= "N�o pode enviar o email. ";
			return false;
		}
		
		/*//fazendo o email
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host = "mail.keyassociados.com.br";
		$mail->IsHTML(true);
		$mail->SMTPAuth = true;
		$mail->Username = "sym@keyassociados.com.br";
		$mail->Password = "rNs55oLL";
		$mail->From     = "sym@keyassociados.com.br";
		$mail->FromName = "Sym";
		
		$mail->Subject = $Subject; $mail->Body = $Body;
		
		$mail->AltBody  = ""; // optional, comment out and test
		
		$mail->AddAddress($this->enviar_email,$this->enviar_nome);
		
		if(!$mail->Send()) { $this->erros .= "N�o pode enviar o email. "; return false; }
		//*/
		
		return true;
	}
	
	/**
	* Fun��o customizarMail
	* 
	* Altera os textos do assunto e corpo do email para algo personalizado
	* 
	* @return true on success
	*/
	function customizarMail($assunto,$corpo)
	{
		$this->email_assunto = $assunto;
		$this->email_corpo   = $corpo;
		return true;
	}
	
	/**
	* Fun��o alterarReferencia
	* 
	* Altera a referencia do workflow em questao
	* 
	* @return true on success
	*/
	function alterarReferencia($referencia="")
	{
		$sql = "UPDATE CC_Workflow 
				SET referencia = '".($referencia==""?$this->referencia:$referencia)."'  
				WHERE id = '".$this->id."' LIMIT 1 ";
		$res = mysql_query($sql) or die(mysql_error());
		return true;
	}
	
	/**
	* Fun��o alterarStatus
	* 
	* Altera o status do workflow em questao
	* 
	* @return true on success
	*/
	function alterarStatus($status)
	{
		$sql = "UPDATE CC_Workflow 
				SET completo = '".$status."' 
				".($status>=100?", data_fim = '".date("Y-m-d H:i:s")."'":"")." 
				WHERE id = '".$this->id."' LIMIT 1 ";
		$res = mysql_query($sql) or die(mysql_error());
		
		return true;
	}
	
	/**
	* Fun��o adicionarChave
	* 
	* adiciona uma chave ao vetor e forma��o da chave final do Workflow
	* 
	* @return true on success
	*/
	function adicionarChave($string)
	{
		$size = sizeof($this->chave_array);
		$string = trim(str_replace("'","",str_replace("\\","",$string)));
		if("".$string=="") return false;
		
		array_push($this->chave_array,$string);
		$this->chave = trim(implode($this->chave_separator,$this->chave_array));
		return true;
	}
	
	/**
	* Fun��o adicionarReferencia
	* 
	* adiciona uma referencia ao vetor e campo de referencias
	* 
	* @return true on success
	*/
	function adicionarReferencia($string,$pos=-1)
	{
		// limpa caracteres n�o permitidos
		$string = trim(str_replace("'","",str_replace("\\","",str_replace(":","",str_replace(";","",$string)))));
		if("".$string=="") $string = "--";
		
		if($pos>=0) $this->referencia_array[$pos] = $string;
		else        array_push($this->referencia_array,$string);
		
		$aus = "";
		foreach($this->referencia_array as $key => $value)
		{
			$aus .= $key.":".$value.";";
		}
		
		$this->referencia = $aus;
		return true;
	}
	
	/**
	* Fun��o carregarReferencia
	* 
	* carrega a string de referencias para o array
	* 
	* @return true on success
	*/
	function carregarReferencia($string="")
	{
		$string = trim(str_replace("'","",str_replace("\\","",$string)));
		if("".$string=="") $string = $this->referencia;
		
		$auv = explode(";",$string);
		
		foreach($auv as $aus)
		{
			$aus = trim($aus);
			if("".$aus!="")
			{
				$auv2  = explode(":",$aus);
				$pos   = intval(trim($auv2[0]));
				$value = trim($auv2[1]);
				$this->referencia_array[$pos] = $value;
			}
		}
		return true;
	}
	
	/**
	* Fun��o editarData
	* 
	* Altera uma data do objeto Workflow (n�o altera o banco de dados)
	* 
	* @return true on success
	*/
	function editarData($data,$tipo=0)
	{
		switch($tipo)
		{
			case 0 : { $this->data_inicio = $data; return true; }
			case 1 : { $this->data_fim    = $data; return true; }
			case 2 : { $this->data_c1     = $data; return true; }
			case 3 : { $this->data_c2     = $data; return true; }
			case 4 : { $this->data_c3     = $data; return true; }
		}
		return false;
	}
}
?>