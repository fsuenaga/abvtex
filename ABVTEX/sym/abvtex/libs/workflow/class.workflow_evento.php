<?PHP
	/**
	*
	*  LIB: WORKFLOW_EVENTO
	*
	*  Requer connect.inc previamente carregado
	*  Pode ser instanciado em qualquer parte do c�digo
	*  
	*  @Author: Gustavo M. Monteiro
	*  @Version: 1.0.0
	*  @Date: 27/04/2012
	*  
	*/

class Workflow_Evento {
	
	/** $_COOKIE["empresa"] */
	var $empresa   = "";
	
	/** Dados do Evento */
	var $evento_id = "";
	var $nome      = "";
	var $descricao = "";
	var $codigo    = "";
	var $tipo      = "";
	
	/** Dados do Mail */
	var $descricao_cliente = "";
	var $chave_separator = "~";
	var $email1   = "";
	var $email2   = "";
	var $email3   = "";
	var $assunto1 = "";
	var $assunto2 = "";
	var $assunto3 = "";
	var $resp1    = "";
	var $resp2    = "";
	var $resp3    = "";
	
	/** Se foi carregado com sucesso */
	var $load          = false;
	
	/** Referencia ao banco de dados, serve para conseguir recuperar a partir da chave primaria o nome/conte�do do dono do Workflow */
	var $db_reference_str = "";
	/** Array no formato 
	[0] => { 
		[table]=>str , //(a tabela do BD) 
		[field]=>str , //(o campo que a chave se refere) 
		[label]=>str   //(o campo que cont�m o nome) 
		} 
	[1] -> ... */
	var $db_reference  = array();
	/** cont�m todos os erros concatenados */
	var $erros         = "";
	
	function Workflow_Evento($evento)
	{
		$this->empresa = $_COOKIE["empresa"];
		
		if(0+intval($evento)<=0) $this->evento_id = $this->procurarEvento($evento);
		else 					 $this->evento_id = intval($evento);
		if(0+intval($this->evento_id)==0) { $this->erros .= "N�o existe evento '0', n�o foi poss�vel carregar. "; return false; }
		$this->evento_id = $evento;
		
		$sql = "SELECT * FROM CC_Workflow_Evento WHERE evento = '".$this->evento_id."' LIMIT 1 ";
		$res = mysql_query($sql) or die(mysql_error());
		
		if(mysql_num_rows($res)<=0) { $this->load = false; $this->erros .= 'N�o conseguiu encontrar CC_Workflow_Evento. '; }
		else
		{
			$this->nome      = mysql_result($res,0,"nome");
			$this->descricao = mysql_result($res,0,"descricao");
			$this->codigo    = mysql_result($res,0,"codigo");
			$this->tipo      = mysql_result($res,0,"tipo");
			$this->db_reference_str = mysql_result($res,0,"db_reference");
			
			$sql = "SELECT * FROM CC_Workflow_Mail WHERE empresa = '".$this->empresa."' AND evento = '".$this->evento_id."' LIMIT 1 ";
			$res = mysql_query($sql) or die(mysql_error());
			
			if(mysql_num_rows($res)<=0) { $this->load = false; $this->erros .= 'N�o conseguiu encontrar CC_Workflow_Mail. '; }
			else
			{
				$this->descricao_cliente = mysql_result($res,0,"descricao_cliente");
				$this->email1   = mysql_result($res,0,"email1");
				$this->email2   = mysql_result($res,0,"email2");
				$this->email3   = mysql_result($res,0,"email3");
				$this->assunto1 = mysql_result($res,0,"assunto1");
				$this->assunto2 = mysql_result($res,0,"assunto2");
				$this->assunto3 = mysql_result($res,0,"assunto3");
				$this->resp1    = mysql_result($res,0,"resp1");
				$this->resp2    = mysql_result($res,0,"resp2");
				$this->resp3    = mysql_result($res,0,"resp3");
				
				$this->load = true;
			}
			
			//estrutra a parte de refer�ncia ao banco de dados para conseguir transformar a chame primaria em texto novamente
			$referencia = explode(";",$this->db_reference_str);
			
			foreach($referencia as $regra)
			{
				$regra = trim($regra);
				if("".$regra!="")
				{
					$pos1 = strpos($regra,":");
					$pos2 = strpos($regra,".");
					$pos3 = strpos($regra,"->");
					
					$numero = intval(substr($regra,0,$pos1));
					$tabela = trim(substr($regra,$pos1+1,$pos2-$pos1-1));
					$campo  = "`".trim(substr($regra,$pos2+1,$pos3-$pos2-1))."`";
					$nome   = "`".trim(substr($regra,$pos3+2))."`";
					$this->db_reference[$numero] = array('table' => $tabela, 'field' => $campo, 'label' => $nome);
				}
			}
		}
	}
	
	function procurarEvento($evento)
	{
		$aus = str_replace("'","",str_replace("\\","",$evento));
		$sql = "SELECT evento FROM CC_Workflow_Evento 
				WHERE nome = '".$aus."' OR codigo = '".$aus."' LIMIT 1";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res)>0) return false;
		else                       return mysql_result($res,0,"evento");
	}
	
	function carregado() { return $this->load; }
}
?>