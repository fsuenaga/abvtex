<?PHP
	/**
	*	fun��o: addLog
	*
	*	Adiciona um log no sistema referente ao �ndice. Requer estar conectado no BD para funcionar
	*
	*	@param $indice o indice no C_Log_Indice ao qual se refere o log;
	*	@param $params parametros adicionais a serem adicionados (default = "");
	*	@return true on success
	*/
	function addLog($indice, $params=null, $fornecedor="NULL")
	{
		$str = 'empresa:'.$_COOKIE['empresa'].';perm:'.$_COOKIE['perm'].';';
		if(!is_null($params))
		{
			if(is_string($params)) $str .= $params;
			else if(is_array($params)) { foreach($params as $key => $value) $str .= $key.':'.$value.';'; }
		}
		$sql = "INSERT INTO CC_Log (empresa,fornecedor,user,indice,log) VALUES ('".$_COOKIE["empr"]."','".$fornecedor."','".$_COOKIE["user"]."','".$indice."','".$str."') ";
		mysql_query($sql) or die('Log - '.mysql_error());
		
		return (mysql_affected_rows()>0);
	}
	
	/**
	*	Nova fun��o para adicionar logs, com notifica��o
	*	
	*
	*
	*	@param $indice - N�mero de refer�ncia do Log - Tabela BD -> CC_Log_Indice
	*	@param $empresa - Nome ou n�mero da empresa que efetua a a��o (fornecedor, Varejista, Certificadora, Administradora)
	*/
	
	function addLogNew($indice, $empresa, $log='NULL', $fornecedor='NULL'){
		$sql = "INSERT INTO CC_Log (empresa,user,indice, log, fornecedor) VALUES ('".$empresa."','".$_COOKIE["user"]."','".$indice."', ".(($log != 'NULL')?"'".$log."'":$log).", ".(($fornecedor != 'NULL')?"'".$fornecedor."'":$fornecedor).") ";
		mysql_query($sql) or die('Log - '.mysql_error());
	}
	
?>