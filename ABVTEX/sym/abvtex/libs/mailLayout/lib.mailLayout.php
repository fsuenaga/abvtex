<?PHP
	function LoadLayout($layout)
	{
		global $syspath;
		
		$letter = file_get_contents($syspath.'libs/mailLayout/'.$layout.'.htm');
		
		return $letter;
	}
	
	function replaceContent($letter,$body="",$title="",$sign="")
	{
		$str = $letter;
		if("".$body !="") $str = str_replace("<!-- CORPO //-->"     ,$body ,$str);
		if("".$title!="") $str = str_replace("<!-- TITULO //-->"    ,$title,$str);
		if("".$sign !="") $str = str_replace("<!-- ASSINATURA //-->",$sign ,$str);
		
		return $str;
	}
?>