<?php

	require_once("key.inc");
	require_once("modules/auditoria/audi_libs.php");
	require_once("functions.php");
	
	//VERIFICA SE FORNECEDOR EXISTE
				
	$funcao = str_replace("'", "", $_GET['funcao']);
	
	
	if($funcao == 'checkFornecedor'){
		if($_POST['cnpj']){
			
			//remove pontuação
			$array1 = array( ".", "/","-", "'");
			$array2 = array( "", "", "","");
			$cnpj = str_replace($array1, $array2, $_POST['cnpj']);
			$cnpj_original = str_replace("'", "", $_POST['cnpj']);
			
			$sql = "SELECT * FROM CC_Fornecedor 
					JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
					WHERE (CC_Fornecedor.CNPJ LIKE '".$cnpj."' OR CC_Fornecedor.CNPJ LIKE '".$cnpj_original."') LIMIT 1";
			$query = mysql_query($sql) or die(mysql_error());
			
			if(mysql_num_rows($query) > 0){
				echo '1';
			}else{
				echo '0';
			}
			exit();
		}
	}
	if($funcao == 'checkFornecedorFilial'){
		if($_POST['cnpj']){
			
			//remove pontuação
			$array1 = array( ".", "/","-", "'");
			$array2 = array( "", "", "","");
			$cnpj = str_replace($array1, $array2, $_POST['cnpj']);
			$cnpj = substr($cnpj,0,8)."0001";
			$sql = "SELECT * FROM CC_Fornecedor 
					JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
					WHERE (CC_Fornecedor.CNPJ LIKE '".$cnpj."%') LIMIT 1";
			$query = mysql_query($sql) or die(mysql_error());
			
			if(mysql_num_rows($query) > 0){
				echo '1';
			}else{
				echo '0';
			}
			exit();
		}
	}
	if($funcao == 'checkIDFornecedor'){
		if($_POST['ID']){
			
			//remove pontuação
			$array1 = array( ".", "/","-", "'");
			$array2 = array( "", "", "","");
			$ID = str_replace($array1, $array2, $_POST['ID']);
			
			$sql = "SELECT * FROM CC_Fornecedor 
					JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
					WHERE CC_Fornecedor.fornecedor = '".$ID."' AND empresa = '".$_COOKIE['empresa']."' LIMIT 1";
			$query = mysql_query($sql) or die(mysql_error());
			
			if(mysql_num_rows($query) > 0){
				echo '1';
			}else{
				echo '0';
			}
			exit();
		}
	}
	
	if($funcao == 'checkSubcontratado'){
		if($_POST['cnpj']){
			
			//remove pontuação
			$array1 = array( ".", "/","-", "'","\\");
			$array2 = array(  "",  "", "",  "",  "");
			$cnpj = str_replace($array1, $array2, $_POST['cnpj']);
			$cnpj_original = str_replace("'", "", $_POST['cnpj']);
			
			$frid = trim(str_replace(" ","",str_replace("'","",$_POST['frn'])));
			
			$sql = "SELECT * FROM CC_Fornecedor 
					JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
					WHERE CC_Fr.empresa = '".$frid."' AND (CC_Fornecedor.CNPJ = '".$cnpj."' OR CC_Fornecedor.CNPJ = '".$cnpj_original."') LIMIT 1 ";
			$query = mysql_query($sql) or die(mysql_error());
			
			if(mysql_num_rows($query) > 0){
				//Caso estiver já vinculado
				echo '1';
			}else{
				//Caso não encontrar subcontratado
				
				$sql = "SELECT * FROM CC_Fornecedor 
					JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
					WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND (CC_Fornecedor.CNPJ = '".$cnpj."' OR CC_Fornecedor.CNPJ = '".$cnpj_original."') LIMIT 1 ";
				$query = mysql_query($sql) or die(mysql_error());
				
				if(mysql_num_rows($query) > 0){
					
					echo '
						<form action="subcontratado.php?ac=cadastro" method="post">
						<input type="hidden" name="action" value="fornecedor_encontrado" />
						<input type="hidden" name="ID" value="'.mysql_result($query,0,'CC_Fornecedor.fornecedor').'" />
						<table cellspacing="0" cellpadding="2" style="font-size:13px;" width="600px">
							<tr class="titulo-escuro">
								<td colspan="2"><b>Fornecedor encontrado:</b></td>
							</tr>
							<tr>
								<td width="150px"><b>N&ordm; ID</b></td>
								<td>'.mysql_result($query,0,'CC_Fornecedor.fornecedor').'</td>
							</tr>
							<tr class="zebra-dark">
								<td><b>CNPJ</b></td>
								<td>'.mascaras(mysql_result($query,0,'CC_Fornecedor.CNPJ'),'##.###.###/####-##').'</td>
							</tr>
							<tr>
								<td><b>Razão Social</b></td>
								<td>'.htmlentities(mysql_result($query,0,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1").'</td>
							</tr>
							<tr>
								<td><b>Nome Fantasia</b></td>
								<td>'.htmlentities(mysql_result($query,0,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1").'</td>
							</tr>
							<tr>
								<td><b>Status</b></td>
								<td>'.htmlentities($audi_resultado['_'.mysql_result($query,0,'CC_Fr.status_fornecedor')], ENT_QUOTES, "ISO-8859-1").'</td>
							</tr>
							'.(
							( (mysql_result($query,0,'CC_Fr.status_fornecedor')  !== '1') )?
							'
							<tr class="zebra-dark">
								<td colspan="2">Subcontratado, n&atilde;o pode ser vinculado. Status diferente de Aprovado.</td>
							</tr>'
							:'<tr class="zebra-dark">
								<td><input type="submit" value="Vincular"></td>
								<td></td>
							</tr>'
							
							).'
						</table>
						</form>
					';
					
					
				}else{
					echo '0';
				}
				
			}
			
			
			
			exit();
		}
	}
	
	
	if($funcao == 'checkSubcontratadoID'){
		if($_POST['id']){
			
			//remove pontuação
			$id = str_replace("'", "", $_POST['id']);
			
			$frid = $_COOKIE['empr'];
			
			//bloco de verificação da categoria do fornecedor para bloqueio de inserção caso já tenha passado a data de conformidade
			$row_fornecedor   = @mysql_fetch_assoc(mysql_query("SELECT sub FROM CC_Fornecedor WHERE fornecedor = '".$frid."' LIMIT 1 "));
			$row_subcategoria = @mysql_fetch_assoc(mysql_query("SELECT data_conforme FROM CC_Fr_Subcategoria WHERE sub = '".$row_fornecedor["sub"]."' LIMIT 1 "));
			$today = date("Y-m-d"); $data_conforme = "".$row_subcategoria["data_conforme"];
			$teste_pode_vincular = false;
			if("".$data_conforme=="") $teste_pode_vincular = true;
			elseif(strcmp($today,$data_conforme)<0) $teste_pode_vincular = true;
			
			if($frid != $id){
				
				$sql = "SELECT * FROM CC_Fornecedor 
						JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
						WHERE CC_Fr.empresa = '".$frid."' AND (CC_Fornecedor.fornecedor = '".$id."') LIMIT 1 ";
				$query = mysql_query($sql) or die(mysql_error());
				
				if(mysql_num_rows($query) > 0){
					//Caso estiver já vinculado
					echo '1';
				}else{
					//Caso não encontrar subcontratado
					
					$sql = "SELECT * FROM CC_Fornecedor 
						JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor
						WHERE CC_Fr.empresa = '".$_COOKIE['empresa']."' AND (CC_Fornecedor.fornecedor = '".$id."') LIMIT 1 ";
					$query = mysql_query($sql) or die(mysql_error());
					
					if(mysql_num_rows($query) > 0){
						
						echo '
							<form action="subcontratado.php?ac=cadastro" method="post">
							<input type="hidden" name="action" value="fornecedor_encontrado" />
							<input type="hidden" name="ID" value="'.mysql_result($query,0,'CC_Fornecedor.fornecedor').'" />
							<table cellspacing="0" cellpadding="2" style="font-size:13px;" width="600px">
								<tr class="titulo-escuro">
									<td colspan="2"><b>Fornecedor encontrado:</b></td>
								</tr>
								<tr>
									<td width="150px"><b>N&ordm; ID</b></td>
									<td>'.mysql_result($query,0,'CC_Fornecedor.fornecedor').'</td>
								</tr>
								<tr class="zebra-dark">
									<td><b>CNPJ</b></td>
									<td>'.mascaras(mysql_result($query,0,'CC_Fornecedor.CNPJ'),'##.###.###/####-##').'</td>
								</tr>
								<tr>
									<td><b>Razão Social</b></td>
									<td>'.htmlentities(mysql_result($query,0,'CC_Fornecedor.razaoSocial'), ENT_QUOTES, "ISO-8859-1").'</td>
								</tr>
								<tr>
									<td><b>Nome Fantasia</b></td>
									<td>'.htmlentities(mysql_result($query,0,'CC_Fornecedor.nomeFantasia'), ENT_QUOTES, "ISO-8859-1").'</td>
								</tr>
								<tr>
									<td><b>Status</b></td>
									<td>'.htmlentities($audi_resultado['_'.mysql_result($query,0,'CC_Fr.status_fornecedor')], ENT_QUOTES, "ISO-8859-1").'</td>
								</tr>
								'.(
								( (''.mysql_result($query,0,'CC_Fr.status_fornecedor') == '1') || $teste_pode_vincular )?
								'
								<tr class="zebra-dark">
									<td><input type="submit" value="Vincular"></td>
									<td></td>
								</tr>
								':'
								<tr class="zebra-dark">
									<td colspan="2">Subcontratado, n&atilde;o pode ser vinculado. Status diferente de Aprovado.</td>
								</tr>
								'
								).'
							</table>
							</form>
						';
						
						
					}else{
						echo '0';
					}
				}
			}else{
				echo 'ID deve ser diverente do seu.';
			}
			
			exit();
		}
	}
	
	
	
	if($funcao == 'buscarEndereco'){
		$cep = $funcao = str_replace("'", "", $_POST["CEP"]);
		$action="http://www.buscacep.correios.com.br/servicos/dnec/consultaLogradouroAction.do" ;	
		$ch = curl_init($action);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt($ch, CURLOPT_POST, true );
		curl_setopt($ch, CURLOPT_POSTFIELDS, "CEP=".$cep."&Metodo=listaLogradouro&TipoConsulta=cep&StartRow=1&EndRow=10");
		
		$r=curl_exec($ch);
		curl_close($ch);
		
		
		var_dump($r); exit();
		
		//EXTRAINDO VALORES
		if($pos   = strpos($r, '<table border="0" cellspacing="1" cellpadding="5" bgcolor="gray">')){
			$table = substr($r,$pos,500);
			//Desmonta página
			list($rua,$bairro,$cidade,$estado) = explode("    ",trim(strip_tags($table)));
			
			echo utf8_encode("rua==".$rua."||bairro==".$bairro."||cidade==".$cidade."||estado==".$estado);
					
		}else echo "CEP Nao encontrado";
	}
	
	
	
	if($funcao == 'verificarUsuario'){
	
		$campo = $_POST['campo'];
		$valor = $_POST['valor'];
	
		$sql = "SELECT * FROM CC_Usuario 
				WHERE 
					".$campo." = '".$valor."'
				ORDER BY nome ASC";
		$query = mysql_query($sql) or die(mysql_error());
		
		if(mysql_num_rows($query) > 0)
			echo '1';
		else
			echo '0';
		
	}

	if($funcao == 'encontrarServicoSub'){
		
    	$frid = $_POST['frid'];
    	
	  $sql = "SELECT * FROM CC_Servico 
		    		JOIN CC_ServicoTipo ON CC_Servico.servico = CC_ServicoTipo.id
	    		WHERE CC_ServicoTipo.empresa = '".$_COOKIE['empresa']."'
	    		AND CC_Servico.fornecedor = '".$frid."' ";
		$query = mysql_query($sql) or die(mysql_error());
    	$tiposServicos = '';
    	if(mysql_num_rows($query) > 0){
    		while($values = mysql_fetch_array($query)){
    		 $tiposServicos .= '<option value="'.$values['id'].'">'.$values['tipo'].'</option>';
    		}
    	}
	    
	    echo $tiposServicos;
    	
    	exit(0);
    }
	


?>