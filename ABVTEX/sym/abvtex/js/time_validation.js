// JavaScript Document

var moduleValidation = true;
var flag = new Array();

function ValidDate(oform,campo)
{
    var message = 'Por favor insira uma data v�lida. Formato correto: DD/MM/AAAA';
	var temp = new Array();
	var i = 0;

	if((''+oform[campo].value).length > 0)
	{
		if((''+oform[campo].value).length != 10)
		{
			alert('11: '+message);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		if((''+oform[campo].value).charAt(2) != '/')
		{
			alert('12: '+message);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		if((''+oform[campo].value).charAt(5) != '/')
		{
			alert('13: '+message);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		temp = (''+oform[campo].value).split('/');
		if(isNaN(parseInt(temp[0],10))||isNaN(parseInt(temp[1],10))||isNaN(parseInt(temp[2],10))||(parseInt(temp[2],10)==0))
		{
			alert('14: '+message);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		if(parseInt(temp[1],10) > 12 || parseInt(temp[1],10) < 1)
		{
			alert('15: '+message);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		if((parseInt(temp[1],10) == 4 || 
		    parseInt(temp[1],10) == 6 || 
		    parseInt(temp[1],10) == 9 || 
		    parseInt(temp[1],10) == 11 ) &&
		   (parseInt(temp[0],10) > 30 ||
		    parseInt(temp[0],10) < 1) )
		{
			alert('16: '+message);
			flag = true;
			return;
		}
		else
		if(parseInt(temp[1],10) == 2)
		{
			if(parseInt(temp[2],10)%4 > 0 && 
		   (parseInt(temp[0],10) > 28 ||
		    parseInt(temp[0],10) < 1) )
			{
				alert('17: '+message);
				for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
				if(i>=flag.length) flag.push(campo);
				return;
			}
			else if
			(parseInt(temp[0],10) > 29 ||
		     parseInt(temp[0],10) < 1)
			{
				alert('18: '+message);
				for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
				if(i>=flag.length) flag.push(campo);
				return;
			}
		}
		else
		if(parseInt(temp[0],10) > 31 ||
		   parseInt(temp[0],10) < 1  ||
		   parseInt(temp[1],10) > 12 ||
		   parseInt(temp[1],10) < 1)
		{
			alert('19: '+message);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
	}
	for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
	if(i<flag.length) flag.splice(i,1);
}

function HelpDate(oform,campo)
{
	if((''+oform[campo].value).length == 2) oform[campo].value = oform[campo].value+'/';
	if((''+oform[campo].value).length == 5) oform[campo].value = oform[campo].value+'/';
}

function ValidHour(oform,campo)
{
    var message2 = 'Por favor insira uma hora v�lida. Formato correto: HH:MM ';
	var temp2 = new Array();
	var i = 0;

	if((''+oform[campo].value).length > 0)
	{
		if((''+oform[campo].value).length != 5)
		{
			alert(message2);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		if((''+oform[campo].value).charAt(2) != ':')
		{
			alert(message2);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		temp2 = (''+oform[campo].value).split(':');
		if(isNaN(parseInt(temp2[0]))||isNaN(parseInt(temp2[1])))
		{
			alert(message2);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
		if(parseInt(temp2[0]) > 23 || parseInt(temp2[0]) < 0 ||
		   parseInt(temp2[1]) > 59 || parseInt(temp2[1]) < 0)
		{
			alert(message2);
			for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
			if(i>=flag.length) flag.push(campo);
			return;
		}
	}
	for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
	if(i<flag.length) flag.splice(i,1);
}

function HelpHour(oform,campo)
{
	if((''+oform[campo].value).length == 2) oform[campo].value = ''+oform[campo].value+':';
}

function fSend(oform)
{
    //DEBUG
	//document.getElementById('dbg').innerHTML='';
	//for(i=0;i<flag.length;i++) document.getElementById('dbg').innerHTML=document.getElementById('dbg').innerHTML+flag[i]+', ';
	if(flag.length == 0) {oform.submit();}
	else {alert('Existem datas ou horas que n�o est�o no formato adequado.');}
}

function fSendReturn()
{
    //DEBUG
	//document.getElementById('dbg').innerHTML='';
	//for(i=0;i<flag.length;i++) document.getElementById('dbg').innerHTML=document.getElementById('dbg').innerHTML+flag[i]+', ';
	if(flag.length == 0) {return true;}
	else {alert('Existem datas ou horas que n�o est�o no formato adequado.');}
	return false;
}

function addField(campo)
{
	flag.push(campo);
}

function removeField(campo)
{
	for(i=0;i<flag.length;i++) if(flag[i] == campo) break;
	if(i<flag.length) flag.splice(i,1);
}