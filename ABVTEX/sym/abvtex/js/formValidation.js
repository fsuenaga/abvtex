/*
\u00e1 -> á
\u00e9 -> é
\u00ed -> í
\u00f3 -> ó
\u00fa -> ú
\u00c1 -> Á
\u00c9 -> É
\u00cd -> Í
\u00d3 -> Ó
\u00da -> Ú
\u00f1 -> ñ
\u00d1 -> Ñ 
*/

$(function(){	
	$("form").submit(function() {
		//Define Submit verdadeiro
		var retorno = true;
		
		//Pega todos os radios
		var radios = new Array();
		
		$(".required", this).each(function(){
			//Verifica tipos select, textarea, text
			if(($(this).is("select")) || ($(this).is("textarea")) || ($(this).attr('type') == undefined) || ($(this).attr('type') == 'text') || ($(this).attr('type') == 'hidden')){
				if($(this).val() == ""){
					alert("O campo " + $(this).attr("alt") + ", \u00e9 obrigat\u00f3rio.");
					$(this).focus();
					//Define Submit falso
					retorno = false;
					return false;
				}else{
					if($(this).hasClass('email')){
						if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val()))) {
							alert("Email incorreto.");
							$(this).focus();
							retorno = false;
							return (false);
						}
					}
				}
			//Verifica tipo checkbox
			}else if($(this).attr('type') == 'checkbox'){
				if(!($(this).is(':checked'))){
					alert("O campo " + $(this).attr("alt") + ", \u00e9 obrigat\u00f3rio.");
					//Define Submit falso
					retorno = false;
					return false;
				}
			//Verifica tipo radio
			}else if($(this).attr('type') == 'radio'){
				if(!$("input[name^='" + $(this).attr('name') + "']").is(':checked')){
					alert("O campo " + $(this).attr("alt") + ", \u00e9 obrigat\u00f3rio.");
					//Define Submit falso
					retorno = false;
					return false;
				}
			}
			
		});
		return retorno;
	});
});