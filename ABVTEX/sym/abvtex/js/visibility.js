function setVisibility(id,opt)
{
	var obj = document.getElementById(id);
	if(obj) obj.style.display=opt;
}

function triggerVisibility(id)
{
	var obj = document.getElementById(id);
	if(obj.style.display=='none') obj.style.display='';
	else 						  obj.style.display='none';
}