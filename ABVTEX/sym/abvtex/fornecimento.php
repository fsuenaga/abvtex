
<?php
    require_once("connect.inc");
    require_once("../date.inc");
    include_once("functions.php");
    
    require_once('libs/lib.sendmail.php');
    require_once('libs/mailLayout/lib.mailLayout.php');

	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
			
			<!-- HEADER -->
				<?php include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			
			<?php 
				$action = $_GET['ac'];
			    
			    switch($action){
			    	case 'excluir': include('modules/fornecimento/forn_excluir.php'); break;
			    	default: include('modules/fornecimento/fornecimento.php'); break;
			    }
			    
			?>
			
			
		</div>	
		
	</body>

</html>	
