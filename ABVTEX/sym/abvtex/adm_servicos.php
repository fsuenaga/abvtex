<?php
	if($_COOKIE["perm"]!="Administradora") { echo('<script> history.back(); </script>'); exit(0); }
	
	require_once("connect.inc");
	include_once("functions.php");
    require_once("libs/date.inc");
	require_once("libs/lib.log.php");
	
	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	
	$acao       = ''.$_GET['ac'];
	$acao_model = ''.$_GET['mac'];
	$id 		= 0+intval($_GET['id']);
	
	if($acao_model!='')
	{
		switch($acao_model){
			case 'srvadd'   : include_once('modules/adm/servicos/srv_model_add.php');      break;
			case 'srvupd'   : include_once('modules/adm/servicos/srv_model_edit.php');     break;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/visibility.js"></script>
		<!-- JS -->
	</head>

	<body>
		<div class="content">
			
			<!-- HEADER -->
				<?php include('topo.php'); ?>
			<!-- FIM HEADER --> 
			
			<?php
				switch($acao){
					case 'form':	include_once('modules/adm/servicos/srv_form.php');     break;
					default:		include_once('modules/adm/servicos/srv_list.php');     break;
				}
			?>
		</div>
	</body>
</html>