<?PHP
    require_once("../date.inc");
    include_once("functions.php");
    require_once("connect.inc");
	
    require_once('libs/lib.sendmail.php');
    require_once('libs/mailLayout/lib.mailLayout.php');
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
		<!-- Meta tags -->
		<meta name="description" content="<?php echo $metaDescription; ?>">
		<meta name="keywords" value="<?php echo $metaKeywords; ?>">
		<meta name="copyright" content="<?php echo $metaCopyright; ?>">
		<meta name="date" content="<?php echo $metaDate; ?>">
		
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Title -->
		<title><?PHP echo($systemName); ?></title>
		
		
		<!-- JS -->
			<script type="text/javascript" src="js/jquery.js"></script>
			
			<script type="text/javascript" src="js/formValidation.js"></script>
	
			<script type="text/javascript" src="js/jquery.maskedinput-1.3.js"></script>
			
			<script type="text/javascript">
				//Mascaras de entrada
				jQuery(function($){
					$(".data").mask("99/99/9999");
					$(".telefone").mask("(99) 9999-9999");
					$(".celular").mask("(99) 9999-9999");
					$(".celularSP").mask("(99) 99999-9999");
					$(".cpf").mask("999.999.999-99");
					$(".cep").mask("99999-999");
					$(".cnpj").mask("99.999.999/9999-99");
					$(".placa").mask("aaa-9999");
				}); 
			</script>
			<!-- JS -->
	</head>
		
	<body>
				<div class="content">
				
					<!-- HEADER -->
						<?php include('topo.php'); ?>
					<!-- FIM HEADER --> 
					
					
					<!-- CONTENT -->
					<div class="mainTitle">Configura&ccedil;&otilde;es</div>
					
					
					
					<div style="width: 450px; float: left;">
						<a href="dados_usuario.php?tipo=dados_us">
							<img src="images/login.jpg" alt="Dados de Usu&aacute;rio" />
						</a>
					</div>
					
					<div style="width: 450px; float: left;">
						<a href="manuais.php">
							<img src="images/manuais.jpg" alt="Manuais" />
						</a>
					</div>
					
					
					
					<!-- FIM CONTENT --> 
		</div>	
		
	</body>

</html>	