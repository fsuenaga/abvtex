<?PHP
  # ajustes para o env. dev. - inicio
  header('Content-Type: text/html; charset=ISO-8859-1');
  error_reporting(E_ERROR | E_WARNING | E_PARSE); // o servidor mudou sua configuracao para alertar sobre indices nao declarados (incluindo _GET e _POST. Esta linha serve para tirar este warning.
  # ajustes para o env. dev. - fim

  # SYSTEM VARIABLES
	$system_path = $_SERVER['REQUEST_URI'];
	
	if(strpos($system_path,"?") !== false) $system_path = substr($system_path,0,strpos($system_path,"?"));
	if(strpos($system_path,"#") !== false) $system_path = substr($system_path,0,strpos($system_path,"#"));
		
	$actual_path = "/sym/abvtex/";
	$cookie_path = "/sym/abvtex/";

	if(!isset($syspath))
	{
		$system_path = str_replace($actual_path,"",$system_path);

		$syspath = "";
		for($i=0;$i<strlen($system_path);$i++) { if($system_path[$i]=='/') $syspath .= "../"; }
	}
	if($libs_path!="") $syspath=$libs_path;
	
	$set_cookie = true;
	if(isset($dont_set_cookie) && $dont_set_cookie==true) $set_cookie=false;

	$baseurl = 'http';
	if ($_SERVER["HTTPS"] == "on") $baseurl .= "s";
	$baseurl .= "://".$_SERVER["SERVER_NAME"].'/sym/';

	//variáveis iniciais do sistema
	$systemName = "Sym";
	$sysMessage = "<br>Bem-Vindo(a) ao Sym - Versão 3.1.0";
	$sysDelimiter = "<DELIMITX8>";
	$sysInsideDelimiter = ":::";
	
	//Meta tags
	$metaDescription = "";
	$metaKeywords = "";
	$metaCopyright = "";
	$metaDate = "";
	$systemName = "Programa de Certificação de Fornecedores para o Varejo - ABVTEX";


	//função para listar os erros e salvar as configurações no banco de dados
	function errorListing($itm='') 
	{
		return str_replace("'","",str_replace("\\","",str_replace("drop","","PAG ".$_SERVER['REQUEST_URI']."
		ITM ".$itm."
		USR ".$_COOKIE["user"]."
		PSS ".$_COOKIE["pass"]."
		EMP ".$_COOKIE["empr"]."@".$_COOKIE["empresa"]."
		I.P ".$_SERVER['REMOTE_ADDR']."")));
	}

	//função para enxotar o usuário por algum acesso indevido
	function getAway($ern=5,$bin_typ=0,$itm=0)
	{
		mysql_query("INSERT INTO CC_Log (empresa,user,indice,log) VALUES ('".$empr."','".$user."','998','".errorListing(''.$itm)."')");
		header("Location: ".$syspath."index.php?ern=".$ern."&bintp=".$bin_typ); die();
		die();
	}

	//verificação dos cookies de maior uso no sistema
	$getAway = false;
	$aus = strtolower($_COOKIE["user"]);    if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,1);
	$aus = strtolower($_COOKIE["empr"]);    if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,2);
	$aus = strtolower($_COOKIE["empresa"]); if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,3);
	$aus = strtolower($_COOKIE["perm"]);    if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,8);
	$aus = strtolower($_COOKIE["nomeUser"]);    if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,8);
	$aus = strtolower($_COOKIE["sess"]);    if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,8);
	$aus = strtolower($_COOKIE["status_certificacao"]);    if(strpos($aus,"'") !== false || strpos($aus,"\\") !== false || strpos($aus,"drop") !== false) getAway(5,998,8);

	//conecta ao banco de dados
	require_once($syspath."key.inc");

	$user = str_replace("'","",$_COOKIE["user"]);
	$empr = str_replace("'","",$_COOKIE["empr"]);
	$perm = $_COOKIE["perm"];
	$sess = sessionCodefy();

	$go = false;
	$ern = -1;
	$byn_tip = 0;

	

	//verificando se usuário e empresa estão setados, caso não = logout
	if($empr != ""){
	
		// Teste de login por Email
			$query = "SELECT * FROM CC_Usuario WHERE user = '".$user."' LIMIT 1";
			$result = mysql_query($query) or die ("Erro na busca de usuário - ".mysql_error());
			if(mysql_num_rows($result) > 0){
				$go = true; $sys__user__result = mysql_fetch_assoc($result);
			}
			
			
		if($_COOKIE['perm'] == 'fornecedor'){
			// Teste de login por CNPJ ou por ID
				$query = "SELECT 
							* 
						  FROM CC_Fornecedor 
						  	JOIN CC_Fr ON CC_Fr.fornecedor = CC_Fornecedor.fornecedor AND CC_Fr.empresa = '".$_COOKIE['empresa']."'
						  	JOIN CC_Usuario_Fr ON CC_Usuario_Fr.fornecedor = CC_Fr.fornecedor
						  WHERE 
						  	(CC_Fornecedor.CNPJ = '".$empr."' OR CC_Fornecedor.fornecedor = '".$empr."')
						  LIMIT 1";
				$fornecedorResult = mysql_query($query) or die ("Erro na busca de usuário - ".mysql_error());
				if(mysql_num_rows($fornecedorResult) > 0){
					$go = true;
				}
		}else{
		
			// Teste de login por CNPJ ou por ID
				$query = "
						SELECT * FROM CC_Usuario 
							JOIN CC_Associacao ON CC_Associacao.associado = CC_Usuario.empresa
							JOIN CC_Empresa_Dados ON CC_Empresa_Dados.empresa = CC_Associacao.associado
						WHERE 
							CC_Usuario.user = '".$user."'
							AND CC_Usuario.empresa = '".$_COOKIE['empr']."'
							AND CC_Associacao.empresa = '".$_COOKIE['empresa']."'
						LIMIT 1
				";
				$fornecedorResult = mysql_query($query) or die ("Erro na busca de usuário - ".mysql_error());
				if(mysql_num_rows($fornecedorResult) > 0){
					$go = true;
				}

		}
		
					
	} else { $ern = 2; $bin_typ = 10; } //senao tiver usuario logado, sai do sistema



	//GO é true, pode passar
	if($go)
	{
		if($_COOKIE["sess"]!=$sess)
		{
			$ern = 8; 
			$go = false;
			$bin_typ = $sess;
			$sys_path = $_SERVER['REQUEST_URI'];
			if(strpos($sys_path,"?") !== false) $sys_path = substr($sys_path,0,strpos($sys_path,"?"));
			else if(strpos($sys_path,"#") !== false) $sys_path = substr($sys_path,0,strpos($sys_path,"#"));
			$system_from = "&sysf=endsess-".$sys_path."-".passCodefy($sys_path.date("d"))."&cont=2";
		}
	}
	else // se nao pode ir
	{
		if($user!="" || $empr!="") 
		mysql_query("INSERT INTO CC_Log (empresa,user,indice,log) VALUES ('".$empr."','".$user."','999','".errorListing(''.$ern)."')");
		header("Location: ".$syspath."index.php?ern=".$ern."&bintp=".$bin_typ.$system_from); 
		die();
	}



	//reescrevendo os cookies
	setcookie("user"   , $_COOKIE["user"]                            , time()+72000, $cookie_path);
	setcookie("pass"   , $_COOKIE["pass"]                            , time()+72000, $cookie_path);
	setcookie("empr"   , $_COOKIE["empr"]                            , time()+72000, $cookie_path);
	setcookie("empresa", $_COOKIE["empresa"]                         , time()+72000, $cookie_path);
	setcookie("perm"   , $perm                                       , time()+72000, $cookie_path);
	setcookie("sess"   , $sess                                       , time()+12000, $cookie_path);
	setcookie("hall"   , strtolower($_COOKIE["hall"])                , time()+72000, $cookie_path);
	
	if($perm != 'fornecedor'){
		setcookie("nivel"   , strtolower($_COOKIE["nivel"])                , time()+72000, $cookie_path);
	}
	
	if($perm == 'fornecedor'){
		setcookie("status_certificacao" , mysql_result($fornecedorResult,0,'CC_Fr.status_certificacao'), time()+72000, $cookie_path);
		setcookie("passoSistema"        , mysql_result($fornecedorResult,0,'CC_Fr.passo_sistema')      , time()+72000, $cookie_path);
		setcookie("tipoFR"              , mysql_result($fornecedorResult,0,'CC_Fr.classe')             , time()+72000, $cookie_path);
	}

?>