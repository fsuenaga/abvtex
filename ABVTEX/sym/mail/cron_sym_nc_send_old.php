<?PHP
$libs_path = '../';
require_once("../connect.inc");
require_once("class.phpmailer.php");
require_once($libs_path."date.inc");
require_once($libs_path."lists.inc");
require_once($libs_path."functions.inc");
require_once($libs_path."libs/lib.redirect.php");

$empr   = $_COOKIE["empresa"];
$hall   = explode(".",$_COOKIE["hall"]);
$nc_id  = str_replace("'","",str_replace("\\","",$_GET["ncid"])); // ID da ocorr�ncia
$nc_tp  = 0 + intval($_GET["nctp"]);							  // o tipo da ocorr�ncia
$nc_msg = 0 + intval($_GET["ncms"]);							  // o tipo da responsabilidade ou atribui��o
$nc_etp = 0 + intval($_GET["ncet"]);							  // a etapa atual do sistema
$nc_rpt = 0 + intval($_GET["ncrpt"]);                             // se � para reportar
$dataescrita = fullDate(convertToDate(date("d/m/Y")));
$nc_vars = // matriz que tem as posi��es na tabela ou zero se aquele tipo de ocorr�ncia nao a usa
array(	   //( 0n/a,1ARep,2ACor,3APre,4VEfi,5Desc,6Orig,7RspT)          ,8descricaoEditada
	1=> array(    0,"c23","c28","c33","c37","c10", "c4", 'envio_usuario',"descricaoEditada") , //Preocupa��es
	2=> array(    0,"c13","c18","c23","c27", "c9", "c3", 'envio_usuario',"descricaoEditada") , //NCs
	3=> array(    0,    0,    0,"c14","c18", "c9", "c3", 'envio_usuario',"descricaoEditada") , //OMs
	4=> array(    0,    0,    0,    0,"c18", "c9", "c3", 'envio_usuario',"descricaoEditada")   //Sugest�es
);

$nc_nomes = array(0,"Preocupa��o"      ,"N�o-Conformidade" ,"Oportunidade de Melhoria","Sugest�o"               ); // Labels de ocorr�ncias
$nc_acoes = array(0,"A��o de Repara��o","A��o de Corretiva","A��o de Preventiva"      ,"Verifica��o de Efic�cia","Respons�vel T�cnico"); // labels das ac��es
$nc_tabls = array(0,"resp_reparacao"   ,"resp_corretiva"   ,"resp_preventiva"         ,"resp_eficacia"          ,"envio_usuario"      ); // campo na tabela dos respons�veis

$nc_etapa = array(0,"Descri��o Inicial","Classifica��o","Levantamento de Informa��es","Estudo da Causa","Designa��o das a��es e seus solucionadores");

$lista_usuarios = getUsersList();
$lista_origens  = getOriginList($nc_tp);

$img_fail    = '<img src="'.$libs_path.'images/plan_fail_clean.png"  align="right">';
$img_success = '<img src="'.$libs_path.'images/plan_feito_clean.png" align="right">';

?>
<html>
<head><title><?PHP echo($systemName); ?> </title></head>
<body background="bg.gif" link="#000000" vlink="#000000" alink="#660000" leftmargin="0" topmargin="0" bgproperties="fixed" marginwidth="0" marginheight="0" style="font: medium 'Trebuchet MS', Trebuchet, Verdana, Sans-serif;">
<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="1" style="border:1px solid white;">
<?PHP
if($nc_id != "" && $nc_rpt == 1)
{
	$sql = "SELECT * FROM CC_NC".$nc_tp." WHERE empresa = '".$empr."' AND c1 = '".$nc_id."' LIMIT 1 ";
	$result = mysql_query($sql);
	if(mysql_num_rows($result)<=0)
	{
		echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> Ocorr�ncia n�o encontrada ['.$nc_id.'_'.$nc_tp.'_'.$nc_msg.'].</td></tr></table></body>');
	}
	//elseif(strtoupper($_COOKIE["empresa"]) == "BRADESCO")
	else
	{
		$resultempr = mysql_query("SELECT razaoSocial, representante_norma, representante_email, representante_norma2, representante_email2 FROM CC_Empresa WHERE empresa = '".$_COOKIE["empresa"]."' LIMIT 1 ");
		$resultuser = mysql_query("SELECT nome, email FROM CC_Usuario WHERE empresa = 'holus' AND (user = 'patricia' OR user = 'eliane') ");
		
		//$origin = mysql_query("SELECT nome FROM CC_Origem WHERE empresa = '".$_COOKIE["empresa"]."' ");
		
		if(strpos(mysql_result($resultuser,0,"email"),"@")===false)
		{
			echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O respons�vel n�o possui e-mail cadastrado.</td></tr></table></body>');
		}
		else
		{
			$origem      = $lista_origens[mysql_result($result,0,$nc_vars[$nc_tp][6])];
			$responsavel = $lista_usuarios[mysql_result($result,0,"resp_entrada")];
			
			$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body  style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
			<table width="600" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
			<tr>
			<td>
				<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr> 
				<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
				<td width="445"><div align="left"><font size="+1"><strong>Controle de Ocorr�ncias</strong></font><br>
				'.mysql_result($resultempr,0,"razaoSocial").'</div>
				<div align="right">'.$dataescrita.'</div></td>
				</tr>
				<tr> 
				<td colspan="2"><p><hr><br>Prezado(a) <REP_EMAIL>,</p>
					<blockquote> 
					Solicitamos suas providencias para atendimento � <b>'.$nc_nomes[$nc_tp].'</b> registrada no Gerenciador Sym para o SGRS '.translateNorm($hall[1]).' '.ucfirst($_COOKIE["empresa"]).', c�digo identificador ID: '.$nc_id.'<br><br>
					<b style="color:#220000">Origem: </b><br>
					'.$origem.'<br><br>
					<b style="color:#220000">Respons�vel: </b><br>
					'.$responsavel.'<br><br>
					<b style="color:#220000">Descri��o do Fato: </b><br>
					'.mysql_result($result,0,$nc_vars[$nc_tp][5]).'<br><br>
					<b style="color:#220000">Atribu�do por: </b>'.mysql_result($sys__user,0,"nome").'<br><br>
					O acesso ao sistema Sym pode ser feito <a href="http://www.techsocial.com.br/sym">clicando aqui</a>.
					</blockquote>
				<br>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
			</body>
			</html>';
			
			//fazendo o email - Telma
			if(mysql_result($resultempr,0,"representante_email")!="")
			{
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = ""; // optional, comment out and test
				$mail->Body     = $emailHTML;
				$mail->AddAddress(mysql_result($resultempr,0,"representante_email"), mysql_result($resultempr,0,"representante_norma"));
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
			//fazendo o email - Danilo
			if(mysql_result($resultempr,0,"representante_email2")!="")
			{
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = ""; // optional, comment out and test
				$mail->Body     = str_replace("<REP_EMAIL>",mysql_result($resultempr,0,"representante_norma2"),$emailHTML);
				$mail->AddAddress(mysql_result($resultempr,0,"representante_email2"), mysql_result($resultempr,0,"representante_norma2"));
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
			
			//fazendo o email
			$mail = new PHPMailer();
			$mail->IsSMTP(); // Define que a mensagem ser� SMTP
			$mail->SMTPSecure = "ssl";
			$mail->Host = "186.211.113.107:25";
			$mail->SMTPAuth = true;
			$mail->Username = "sym@keyassociados.com.br";
			$mail->Password = "VOkez9KS";
			$mail->IsHTML(true);
			
			$mail->From     = "sym@keyassociados.com.br";
			$mail->FromName = "KEYASSOCIADOS - Sym";
			$mail->Subject  = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
			$mail->AltBody  = ""; // optional, comment out and test		
			$mail->Body     = $emailHTML;
			$mail->AddAddress(mysql_result($resultuser,0,"email"), mysql_result($resultuser,0,"nome"));
			$mail->AddAddress(mysql_result($resultuser,1,"email"), mysql_result($resultuser,1,"nome"));
			$mail->AddAddress("gmonteiro@keyassociados.com.br", "Gustavo M. Monteiro"); 
			if(!$mail->Send()) {
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
			} else {
				echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
			}
			
			if(strtolower($empr)=='bradesco')
			{
				//fazendo o email
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject  = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody  = ""; // optional, comment out and test		
				$mail->Body     = $emailHTML;
				#$mail->MsgHTML(str_replace("<REP_EMAIL>",mysql_result($resultuser,0,"nome"),$emailHTML));
				#$mail->PluginDir = "mail/";
				$mail->AddAddress("4240.vanessas@bradesco.com.br", "4240.vanessas@bradesco.com.br"); 
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
			
			/*fazendo o email
			$mail = new PHPMailer();
			$mail->IsSMTP(); // Define que a mensagem ser� SMTP
			$mail->Host = "localhost"; // Endere�o do servidor SMTP
	
			$mail->IsHTML(true);
			
			$mail->From     = "sym@keyassociados.com.br";
			$mail->FromName = "KEYASSOCIADOS - Sym";
			$mail->Subject  = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
			$mail->AltBody  = ""; // optional, comment out and test		
			$mail->Body     = $emailHTML;
			$mail->AddAddress("gmonteiro@keyassociados.com.br", "Gustavo M. Monteiro"); 
			if(!$mail->Send()) {
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
			} else {
				echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
			} //*/
		}
	}
}
elseif($nc_id == "" || $nc_tp < 1 || $nc_tp > 4 || $nc_msg == 0 || "".$nc_vars[$nc_tp][$nc_msg] == "0")
{
	echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> As vari�veis alocadas n�o podem encontrar o item ['.$nc_id.'_'.$nc_tp.'_'.$nc_msg.'].</td></tr></table></body>');
}
else
{
	$sql = "SELECT * FROM CC_NC".$nc_tp." WHERE empresa = '".$empr."' AND c1 = '".$nc_id."' LIMIT 1 ";
	$result = mysql_query($sql);
	if(mysql_num_rows($result) <= 0)
	{
		echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> Ocorr�ncia n�o encontrada ['.$nc_id.'_'.$nc_tp.'_'.$nc_msg.'].</td></tr></table></body>');
	}
	else
	{
		if($nc_msg < 5) // se for abaixo de 5
		{
			$resultempr = mysql_query("SELECT razaoSocial FROM CC_Empresa WHERE empresa = '".$_COOKIE["empresa"]."' LIMIT 1 ");
			$resultuser = mysql_query("SELECT nome, email, funcao FROM CC_Usuario WHERE empresa = '".$_COOKIE["empresa"]."' AND user = '".mysql_result($result,0,$nc_tabls[$nc_msg])."' LIMIT 1 ");
			$funcao     = mysql_result($resultuser,0,"funcao");
			
			if(strpos(mysql_result($resultuser,0,"email"),"@")===false)
			{
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O respons�vel n�o possui e-mail cadastrado.</td></tr></table></body>');
			}
			else
			{
				if($funcao==11)
				{
					$corpo_email = '
					<b><font size="+1">Prezado Gerente Administrativo,</font></b>
					<br><br>
					Com o objetivo de aprimorarmos o processo de tratamento de N�o-conformidades e Oportunidades de Melhoria registradas em sua depend�ncia durante as Avalia��es de Conformidade e Auditorias Externas SA8000, disponibilizamos o envio autom�tico de recomenda��es de a��es a serem implementadas na rede de ag�ncias. 
					<br>
					Logo que receber os relat�rios de a��es sugeridas via e-mail, o Gerente Administrativo dever�:
					<ol>
					<li>Implementar e/ou executar a A��o Imediata, Corretiva e Preventiva, conforme recomendado, dentro dos prazos estabelecidos;</li>
					<li>Gerar ou coletar evid�ncias das a��es implementadas e/ou executadas;</li>
					<li>Acessar o link dispon�vel no relat�rio enviado, que o levar� � base de informa��es da ferramenta SYM;</li>
					<li>Registrar nos campos A��o Realizada, as a��es de fato implementadas e/ou executadas pelos Gerentes Administrativos em sua ag�ncia;</li>
					<li>Salvar as a��es registradas.</li>
					</ol>
					Desta maneira, voc� estar� contribuindo com o aprimoramento e manuten��o do Sistema de Gest�o de Responsabilidade Social Bradesco � Norma SA8000!
					<br><br>
					Em caso de necessidades para login e senha de acesso, bem como itens de acesso, contatar:<br>
					Andresa Bardini � abardini@keyassociados.com.br - (11) 3684-9140<br>
					Neide Medeiros - nmedeiros@keyassociados.com.br - (11) 3684-9140
					<br><br>
					Demais d�vidas sobre os apontamentos:<br>
					Danilo Vidoto - 4240.vidoto@bradesco.com.br - (11) 3684-4433 ';
				} else $corpo_email = 'Solicitamos suas providencias para atendimento � <b>'.$nc_nomes[$nc_tp].'</b> registrada no Gerenciador Sym para o SGRS '.translateNorm($hall[1]).' '.ucfirst($_COOKIE["empresa"]).', c�digo identificador ID: '.$nc_id.'';
				
				$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body  style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
				<table width="90%" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
				<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
					<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
					<td width="445"><div align="left"><font size="+1"><strong>Controle de Ocorr�ncias</strong></font><br>
					'.mysql_result($resultempr,0,"razaoSocial").'</div>
					<div align="right">'.$dataescrita.'</div></td>
					</tr>
					<tr> 
					<td colspan="2">'.($funcao==11?'':'<p><hr><br>Prezado(a) '.mysql_result($resultuser,0,"nome").',</p>').'
						<blockquote> 
						'.$corpo_email.'<br><br>
						<b style="color:#220000">Descri��o do Fato: </b><br>
						'.mysql_result($result,0,$nc_vars[$nc_tp][5]).'<br><br>
						<b style="color:#220000">Provid�ncia solicitada: '.$nc_acoes[$nc_msg].'</b><br>
						'.mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg]).'<br><b>
						Prazo: </b>'.mysql_result($result,0,"c".(intval(substr($nc_vars[$nc_tp][$nc_msg],1))+1)).'<br><br>
						<b style="color:#220000">Atribu�do por: </b>'.mysql_result($sys__user,0,"nome").'<br><br>
						O acesso ao sistema Sym pode ser feito 
						<a href="http://www.techsocial.com.br/sym/redirect.php?redr='.createRedirectLink("OCUR",$nc_id).'">clicando aqui</a>. 
						Por favor, fa�a uso deste link para ser redirecionado exatamente � a��o que lhe foi atribu�da.
						</blockquote>
					<br>
					</td>
					</tr>
					</table>
				</td>
				</tr>
				</table>
				</body>
				</html>';
				
				//fazendo o email
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = ""; // optional, comment out and test
				
				$mail->Body = $emailHTML;
				#$mail->PluginDir = "mail/";
				
				$mail->AddAddress(mysql_result($resultuser,0,"email"), mysql_result($resultuser,0,"nome"));
				
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
		}
		else // se for 5 ou mais o tipo da verifica��o
		{
			$resultempr = mysql_query("SELECT razaoSocial FROM CC_Empresa WHERE empresa = '".$_COOKIE["empresa"]."' LIMIT 1 ");
			$resultuser = mysql_query("SELECT nome, email FROM CC_Usuario WHERE empresa = '".$_COOKIE["empresa"]."' AND user = '".mysql_result($result,0,$nc_tabls[$nc_msg])."' LIMIT 1 ") or die(mysql_error());
			
			if(strpos(mysql_result($resultuser,0,"email"),"@")===false)
			{
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O respons�vel n�o possui e-mail cadastrado.</td></tr></table></body>');
			}
			else
			{
				$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body  style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
				<table width="600" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
				<tr>
				<td>
					<table width="600" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
					<td width="445"><div align="left"><font size="+1"><strong>Controle de Ocorr�ncias</strong></font><br>
					'.mysql_result($resultempr,0,"razaoSocial").'</div>
					<div align="right">'.$dataescrita.'</div></td>
					</tr>
					<tr> 
					<td colspan="2"><p><hr><br>Prezado(a) '.mysql_result($resultuser,0,"nome").',</p>
						<blockquote> 
						Solicitamos suas providencias para atendimento � <b>'.$nc_nomes[$nc_tp].'</b> registrada no Gerenciador Sym para o SGRS '.translateNorm($hall[1]).' '.ucfirst($_COOKIE["empresa"]).', c�digo identificador ID: '.$nc_id.'<br><br>
						<b style="color:#220000">Descri��o do Fato: </b><br>
						'.mysql_result($result,0,$nc_vars[$nc_tp][8]).'<br><br>
						<b style="color:#220000">Provid�ncia solicitada: '.$nc_etapa[$nc_etp].'</b><br><br>
						O acesso ao sistema Sym pode ser feito <a href="http://www.techsocial.com.br/sym/redirect.php?redr='.createRedirectLink("OCUR",$nc_id).'">clicando aqui</a>.
						</blockquote>
					<br>
					</td>
					</tr>
					</table>
				</td>
				</tr>
				</table>
				</body>
				</html>';
				
				//fazendo o email
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = ""; // optional, comment out and test
				
				$mail->Body = $emailHTML;
				#$mail->PluginDir = "mail/";
				
				$mail->AddAddress(mysql_result($resultuser,0,"email"), mysql_result($resultuser,0,"nome"));
				
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
		
		
		
		}
	}
}
?>
</table></body></html>