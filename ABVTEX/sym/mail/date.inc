<?PHP

#
# modulo library_date
#
# autor: Gustavo Monteiro
# data: 10/07/2007
# vers�o: 1.0.0
# 
# descri��o: possui procedimentos para comparar e formatar datas e horas
# 
###################################################################################################################

# Defini��es do Sistema

$VS__TAXA_ANO = 372; //taxa de multiplica��o para diferencia��o
$VS__TAXA_MES = 31;

###################################################################################################################
# fun��o validDate
# 
# Verifica se a data � valida
# 
# @param  $datainit data inicial
# @param  $dataend  data final
# @return true se a data � valida e
#         false caso contr�rio
# 

function validDate($data)
{
   if($data == null) return false;
   
   $months[1] = $months[3] = $months[5] = $months[7] = $months[8] = $months[10] = $months[12] = 31;
   $months[4] = $months[6] = $months[9] = $months[11] = 30;
   if(($data["y"]%4) == 0) $months[2] = 29;
   else $months[2] = 28;
   
   if(($data["m"] > 12)||($data["m"] < 1)) return false;
   
   if(($data["d"] > $months[$data["m"]])||($data["d"] < 1)) return false;
   
   return true;
}


###################################################################################################################
# fun��o convertToDate
# 
# converte uma string no formato DD/MM/YYYY para formato de data do sistema
# 
# @param  $string a string que ira ser convertida em data
# @return a data em vetor com formato "d","m" e "y"
# 

function convertToDate($string)
{
   $data["d"] = intval(substr($string,0,2));
   $data["m"] = intval(substr($string,3,2));
   $data["y"] = intval(substr($string,6));
   
   if (($string{2} != '/')||($string{5} != '/')||($data["d"]*$data["m"]*$data["y"] <= 0)||(!validDate($data))) return NULL;
   return $data;
}


###################################################################################################################
# fun��o convertDateDB
# 
# converte uma string no formato YYYY-MM-DD para DD/MM/YYYY
# 
# 

function convertDateDB($string)
{
   if($string == "0000-00-00" || $string == "" || $string == "--") return "";
   return substr($string,8)."/".substr($string,5,2)."/".substr($string,0,4);
}


###################################################################################################################
# fun��o convertDateSys
# 
# converte uma string no formato DD/MM/YYYY para YYYY-MM-DD
# 
# 

function convertDateSys($string)
{
   if($string == "00/00/0000" || $string == "" || $string == "//") return "";
   return substr($string,6)."-".substr($string,3,2)."-".substr($string,0,2);
}


###################################################################################################################
# fun��o compareDate
# 
# Compara duas datas
# 
# @param  $data1 vetor data
# @param  $data2 vetor data
# @return  1 se data1 maior que data2
#          0 se s�o iguais
#         -1 se data1 � menor que data2
# 

function compareDate($data1, $data2)
{
   if($data1["y"] > $data2["y"]) return 1;
   else if($data1["y"] < $data2["y"]) return -1;
   else
   {
      if($data1["m"] > $data2["m"]) return 1;
      else if($data1["m"] < $data2["m"]) return -1;
	  else
      {
         if($data1["d"] > $data2["d"]) return 1;
         else if($data1["d"] < $data2["d"]) return -1;
		 else return 0;
      }
   }
}

###################################################################################################################
# fun��o betweenDates
# 
# verifica se a data entrada est� entre duas datas
# 
# @param  $data1 vetor data
# @param  $data2 vetor data
# @param  $data3 vetor data
# @return true  se est� entre
#         false caso contr�rio
# 

function betweenDates($data1, $data2, $data3)
{
   return (compareDate($data1, $data2) + compareDate($data2, $data3) < 0) ;
}


###################################################################################################################
# fun��o elapsedDate
# 
# Diz quantos dias tem de diferen�a entre a data inicial e a data final
# 
# @param  $datainit data inicial
# @param  $dataend  data final
# @return dias de diferen�a ou
#         -1 se a data inicial � menor que a data final
# 

function elapsedDate($datainit, $dataend)
{
   $days = 0;
   
   $months[1] = $months[3] = $months[5] = $months[7] = $months[8] = $months[10] = $months[12] = 31;
   $months[4] = $months[6] = $months[9] = $months[11] = 30;
   if(($datainit["y"]%4) == 0) $months[2] = 29;
   else $months[2] = 28;
   
   if(($dataend["y"] > $datainit["y"])&&($dataend["m"] >= $datainit["m"])) $days += ($dataend["y"] - $datainit["y"])*365;
   else if($dataend["y"] < $datainit["y"]) return -1;
   
   if($dataend["m"] > $datainit["m"])
      for($i=$datainit["m"];$i<$dataend["m"];$i++) $days += $months[$i];
   else if($dataend["m"] < $datainit["m"])
   {
      if($dataend["y"] == $datainit["y"]) return -1;
      for($i=$datainit["m"];$i<13;$i++) $days += $months[$i];
      for($i=1;$i<$dataend["m"];$i++) $days += $months[$i];
   }
   $days += $dataend["d"] - $datainit["d"];
   
   return $days;
}


###################################################################################################################
# fun��o fullDate
# 
# entrega uma data inteira de cabe�alho
# 
# @param  $data a datano formato usado no sistema
# @return a data em formato extenso
# 

function fullDate($data)
{
   if(!validDate($data)) return "";
   
   $aux = "".$data["d"];
   if(strlen($aux) > 2) $aux = "0".$aux;
   
       if($data["m"]==1)  $aux .= " de Janeiro";
   elseif($data["m"]==2)  $aux .= " de Fevereiro";
   elseif($data["m"]==3)  $aux .= " de Mar�o";
   elseif($data["m"]==4)  $aux .= " de Abril";
   elseif($data["m"]==5)  $aux .= " de Maio";
   elseif($data["m"]==6)  $aux .= " de Junho";
   elseif($data["m"]==7)  $aux .= " de Julho";
   elseif($data["m"]==8)  $aux .= " de Agosto";
   elseif($data["m"]==9)  $aux .= " de Setembro";
   elseif($data["m"]==10) $aux .= " de Outubro";
   elseif($data["m"]==11) $aux .= " de Novembro";
   elseif($data["m"]==12) $aux .= " de Dezembro";
   
   $aux .= " de ".$data["y"];
   
   return $aux;
}

###################################################################################################################
# fun��o convertToHour
# 
# converte uma string no formato HH:MM para formato de hora do sistema
# 
# @param  $string a string que ira ser convertida em hora
# @return a hora em vetor com formato "h" e "m"
# 

function convertToHour($string)
{
   $hour["h"] = intval(substr($string,0,2));
   $hour["m"] = intval(substr($string,3));
   
   if (($string{2} != ':')||($hour["h"]*$hour["m"] <= 0)||($hour["h"]>23)||($hour["m"]>59)) return NULL;
   return $hour;
}


###################################################################################################################
# fun��o compareHour
# 
# Compara duas horas
# 
# @param  $hour1 vetor hora
# @param  $hour2 vetor hora
# @return 1  se hour1 maior que hour2
#         0  se s�o iguais
#         -1 se hour1 � menor que hour2
# 

function compareHour($hour1, $hour2)
{
   if($hour1["h"] > $hour2["h"]) return 1;
   else if($hour1["h"] < $hour2["h"]) return -1;
   else
   {
      if($hour1["m"] > $hour2["m"]) return 1;
      else if($hour1["m"] < $hour2["m"]) return -1;
	  else return 0;
   }
}


###################################################################################################################
# fun��o elapsedHour
# 
# Diz quantos minutos tem de diferen�a entre a hora inicial e a hora final
# 
# @param  $hourinit data inicial
# @param  $hourend  data final
# @return minutos de diferen�a
# 

function elapsedHour($hourinit, $hourend)
{
   $minutes = 0;
   if(compareHour($hourinit,$hourend) == 1) $minutes = -1*elapsedHour($hourend,$hourinit);
   else
   {
      $minutes += ($hourend["h"] - $hourinit["h"]) * 60;
      if($hourend["m"] < $hourinit["m"]) $minutes += $hourend["m"] - $hourinit["m"] + 60;
	  else $minutes += $hourend["m"] - $hourinit["m"];
   }
   return $minutes;
}

###################################################################################################################
# fun��o elapsedTime
# 
# Diz quantos minutos tem de diferen�a entre duas datas e horas
# 
# @param  $datainit data inicial
# @param  $hourinit hora inicial
# @param  $dataend  data final
# @param  $hourend  hora final
# @return minutos de diferen�a entre as datas e horas
# 

function elapsedTime($datainit, $hourinit, $dataend, $hourend)
{
   $minutes = 0;
   $minutes += elapsedDate($datainit, $dataend)*1440; # 1440 = 24 * 60
   if($minutes < 0) return -1;
   $minutes += elapsedHour($hourinit, $hourend);
   return $minutes;
}

###################################################################################################################
# fun��o maxDaysOfMonth
# 
# Diz quantos dias um m�s tem
# 
# @param  $month numero do m�s em quest�o
# @param  $year  para saber se o ano � bisexto
# @return o numero de dias do m�s
# 

function maxDaysOfMonth($month, $year)
{
   switch ($month)
   {
   case 1  :
   case 3  :
   case 5  :
   case 7  :
   case 8  :
   case 10 :
   case 12 : { return 31; }
   case 4  :
   case 6  :
   case 9  :
   case 11 : { return 30; }
   case 2  : { return ($year % 4 == 0 ? 29 : 28); }
   }
   return -1;
}

###################################################################################################################
# fun��o translateMonth
# 
# traduz o m�s de n�mero para String
# 
# @param  $month numero do m�s em quest�o
# @param  $type  0 para Abrevia��o, 1 para Longo
# @return o m�s em String
# 

function translateMonth($month, $type = 0)
{
   if($type == 0)
   {
	   switch ($month)
	   {
	   case 1  : { return "Jan"; }
	   case 2  : { return "Fev"; }
	   case 3  : { return "Mar"; }
	   case 4  : { return "Abr"; }
	   case 5  : { return "Mai"; }
	   case 6  : { return "Jun"; }
	   case 7  : { return "Jul"; }
	   case 8  : { return "Ago"; }
	   case 9  : { return "Set"; }
	   case 10 : { return "Out"; }
	   case 11 : { return "Nov"; }
	   case 12 : { return "Dez"; }
	   }
   }
   elseif($type == 1)
   {
	   switch ($month)
	   {
	   case 1  : { return "Janeiro"; }
	   case 2  : { return "Fevereiro"; }
	   case 3  : { return "Mar�o"; }
	   case 4  : { return "Abril"; }
	   case 5  : { return "Maio"; }
	   case 6  : { return "Junho"; }
	   case 7  : { return "Julho"; }
	   case 8  : { return "Agosto"; }
	   case 9  : { return "Setembro"; }
	   case 10 : { return "Outubro"; }
	   case 11 : { return "Novembro"; }
	   case 12 : { return "Dezembro"; }
	   }
   }
   return -1;
}

?>