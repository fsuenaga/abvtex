<?PHP
$libs_path = '../';
require_once("../connect.inc");
require_once("class.phpmailer.php");
require_once($libs_path."date.inc");
require_once($libs_path."lists.inc");
require_once($libs_path."functions.inc");
require_once($libs_path."libs/lib.redirect.php");
require_once($libs_path."libs/lib.log.php");

$empr   = $_COOKIE["empresa"];
$hall   = explode(".",$_COOKIE["hall"]);
$nc_id  = str_replace("'","",str_replace("\\","",$_GET["ncid"])); // ID da ocorr�ncia
$nc_tp  = 0 + intval($_GET["nctp"]);							  // o tipo da ocorr�ncia
$nc_msg = 0 + intval($_GET["ncms"]);							  // o tipo da responsabilidade ou atribui��o
$nc_etp = 0 + intval($_GET["ncet"]);							  // a etapa atual do sistema
$nc_rpt = 0 + intval($_GET["ncrpt"]);                             // se � para reportar
$dataescrita = fullDate(convertToDate(date("d/m/Y")));
$nc_vars = // matriz que tem as posi��es na tabela ou zero se aquele tipo de ocorr�ncia nao a usa
array(	   //( 0n/a,1ARep,2ACor,3APre,4VEfi,5Desc,6Orig,7RspT)          ,8descricaoEditada
	1=> array(    0,"c23","c28","c33","c37","c10", "c4", 'envio_usuario',"descricaoEditada") , //Preocupa��es
	2=> array(    0,"c13","c18","c23","c27", "c9", "c3", 'envio_usuario',"descricaoEditada") , //NCs
	3=> array(    0,    0,    0,"c14","c18", "c9", "c3", 'envio_usuario',"descricaoEditada") , //OMs
	4=> array(    0,    0,    0,    0,"c18", "c9", "c3", 'envio_usuario',"descricaoEditada")   //Sugest�es
);
$test=false;
if($nc_msg<5) sleep($nc_msg);

$nc_nomes = array(0,"Preocupa��o"      ,"N�o-Conformidade" ,"Oportunidade de Melhoria","Sugest�o"               ); // Labels de ocorr�ncias
$nc_acoes = array(0,"A��o Imediata Recomendada","A��o Corretiva Recomendada","A��o Preventiva Recomendada","Verifica��o de Efic�cia Recomendada","Respons�vel T�cnico"); // labels das ac��es
$nc_tabls = array(0,"resp_reparacao"   ,"resp_corretiva"   ,"resp_preventiva"         ,"resp_eficacia"          ,"envio_usuario"      ); // campo na tabela dos respons�veis

$nc_etapa = array(0,"Descri��o Inicial","Classifica��o","Levantamento de Informa��es","Estudo da Causa","Designa��o das a��es e seus solucionadores");

$lista_usuarios = getUsersList();
$lista_origens  = getOriginList($nc_tp);

$img_fail    = '<img src="'.$libs_path.'images/plan_fail_clean.png"  align="right">';
$img_success = '<img src="'.$libs_path.'images/plan_feito_clean.png" align="right">';

?>
<html>
<head><title><?PHP echo($systemName); ?> </title></head>
<body background="bg.gif" link="#000000" vlink="#000000" alink="#660000" leftmargin="0" topmargin="0" bgproperties="fixed" marginwidth="0" marginheight="0" style="font: medium 'Trebuchet MS', Trebuchet, Verdana, Sans-serif;">
<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="1" style="border:1px solid white;">
<?PHP
if($nc_id != "" && $nc_rpt == 1)
{
	$sql = "SELECT * FROM CC_NC".$nc_tp." WHERE empresa = '".$empr."' AND c1 = '".$nc_id."' LIMIT 1 ";
	$result = mysql_query($sql);
	if(mysql_num_rows($result)<=0)
	{
		echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> Ocorr�ncia n�o encontrada ['.$nc_id.'_'.$nc_tp.'_'.$nc_msg.'].</td></tr></table></body>');
	}
	//elseif(strtoupper($_COOKIE["empresa"]) == "BRADESCO")
	else
	{
		$resultempr = mysql_query("SELECT razaoSocial, representante_norma, representante_email, representante_norma2, representante_email2 FROM CC_Empresa WHERE empresa = '".$_COOKIE["empresa"]."' LIMIT 1 ");
		$resultuser = mysql_query("SELECT nome, email FROM CC_Usuario WHERE empresa = 'holus' AND (user = 'patricia' OR user = 'eliane') ");
		
		//$origin = mysql_query("SELECT nome FROM CC_Origem WHERE empresa = '".$_COOKIE["empresa"]."' ");
		
		if(strpos(mysql_result($resultuser,0,"email"),"@")===false)
		{
			echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O respons�vel n�o possui e-mail cadastrado.</td></tr></table></body>');
		}
		else
		{
			$origem      = $lista_origens[mysql_result($result,0,$nc_vars[$nc_tp][6])];
			$responsavel = $lista_usuarios[mysql_result($result,0,"resp_entrada")];
			
			$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body  style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
			<table width="600" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
			<tr>
			<td>
				<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr> 
				<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
				<td width="445"><div align="left"><font size="+1"><strong>Controle de Ocorr�ncias</strong></font><br>
				'.mysql_result($resultempr,0,"razaoSocial").'</div>
				<div align="right">'.$dataescrita.'</div></td>
				</tr>
				<tr> 
				<td colspan="2"><p><hr><br>Prezado(a) <REP_EMAIL>,</p>
					<blockquote> 
					Solicitamos suas providencias para atendimento � <b>'.$nc_nomes[$nc_tp].'</b> registrada no Gerenciador Sym para o SGRS '.translateNorm($hall[1]).' '.ucfirst($_COOKIE["empresa"]).', c�digo identificador ID: '.$nc_id.'<br><br>
					<b style="color:#220000">Origem: </b><br>
					'.$origem.'<br><br>
					<b style="color:#220000">Respons�vel: </b><br>
					'.$responsavel.'<br><br>
					<b style="color:#220000">Descri��o do Fato: </b><br>
					'.mysql_result($result,0,$nc_vars[$nc_tp][5]).'<br><br>
					<b style="color:#220000">Atribu�do por: </b>'.mysql_result($sys__user,0,"nome").'<br><br>
					O acesso ao sistema Sym pode ser feito <a href="https://www.techsocial.com.br/sym">clicando aqui</a>.
					</blockquote>
				<br>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
			</body>
			</html>';
			
			//fazendo o email - Telma
			if(mysql_result($resultempr,0,"representante_email")!="")
			{
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = "";
				$mail->Body     = $emailHTML;
				
				if(!$test) $mail->AddAddress(mysql_result($resultempr,0,"representante_email"), mysql_result($resultempr,0,"representante_norma"));
				else       $mail->AddAddress('gmonteiro@keyassociados.com.br','Gustavo M. Monteiro');
				
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
			//fazendo o email - Danilo
			if(mysql_result($resultempr,0,"representante_email2")!="")
			{
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = "";
				$mail->Body     = str_replace("<REP_EMAIL>",mysql_result($resultempr,0,"representante_norma2"),$emailHTML);
				
				if(!$test) $mail->AddAddress(mysql_result($resultempr,0,"representante_email2"), mysql_result($resultempr,0,"representante_norma2"));
				else       $mail->AddAddress('gmonteiro@keyassociados.com.br','Gustavo M. Monteiro');
				
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
			
			//fazendo o email
			$mail = new PHPMailer();
			$mail->IsSMTP(); // Define que a mensagem ser� SMTP
			$mail->SMTPSecure = "ssl";
			$mail->Host = "186.211.113.107:25";
			$mail->SMTPAuth = true;
			$mail->Username = "sym@keyassociados.com.br";
			$mail->Password = "VOkez9KS";
			$mail->IsHTML(true);
			
			$mail->From     = "sym@keyassociados.com.br";
			$mail->FromName = "KEYASSOCIADOS - Sym";
			$mail->Subject  = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
			$mail->AltBody  = "";		
			$mail->Body     = $emailHTML;
			$mail->AddAddress(mysql_result($resultuser,0,"email"), mysql_result($resultuser,0,"nome"));
			$mail->AddAddress(mysql_result($resultuser,1,"email"), mysql_result($resultuser,1,"nome"));
			$mail->AddAddress("gmonteiro@keyassociados.com.br", "Gustavo M. Monteiro"); 
			if(!$mail->Send()) {
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
			} else {
				echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
			}
			
			if(strtolower($empr)=='bradesco')
			{
				//fazendo o email
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject  = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody  = "";		
				$mail->Body     = $emailHTML;
				#$mail->MsgHTML(str_replace("<REP_EMAIL>",mysql_result($resultuser,0,"nome"),$emailHTML));
				#$mail->PluginDir = "mail/";
				$mail->AddAddress("4240.vanessas@bradesco.com.br", "4240.vanessas@bradesco.com.br"); 
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
			
			/*fazendo o email
			$mail = new PHPMailer();
			$mail->IsSMTP(); // Define que a mensagem ser� SMTP
			$mail->Host = "localhost"; // Endere�o do servidor SMTP
	
			$mail->IsHTML(true);
			
			$mail->From     = "sym@keyassociados.com.br";
			$mail->FromName = "KEYASSOCIADOS - Sym";
			$mail->Subject  = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
			$mail->AltBody  = "";		
			$mail->Body     = $emailHTML;
			$mail->AddAddress("gmonteiro@keyassociados.com.br", "Gustavo M. Monteiro"); 
			if(!$mail->Send()) {
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
			} else {
				echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
			} //*/
		}
	}
}
elseif($nc_id == "" || $nc_tp < 1 || $nc_tp > 4 || $nc_msg == 0 || "".$nc_vars[$nc_tp][$nc_msg] == "0")
{
	echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> As vari�veis alocadas n�o podem encontrar o item ['.$nc_id.'_'.$nc_tp.'_'.$nc_msg.'].</td></tr></table></body>');
}
else
{
	$sql = "SELECT * FROM CC_NC".$nc_tp." WHERE empresa = '".$empr."' AND c1 = '".$nc_id."' LIMIT 1 ";
	$result = mysql_query($sql);
	if(mysql_num_rows($result) <= 0)
	{
		echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> Ocorr�ncia n�o encontrada ['.$nc_id.'_'.$nc_tp.'_'.$nc_msg.'].</td></tr></table></body>');
	}
	else
	{
		if($nc_msg < 5) // se for abaixo de 5
		{
			$targeruser = mysql_result($result,0,$nc_tabls[$nc_msg]);
			$resultempr = mysql_query("SELECT razaoSocial FROM CC_Empresa WHERE empresa = '".$_COOKIE["empresa"]."' LIMIT 1 ");
			$resultuser = mysql_query("SELECT nome, email, funcao FROM CC_Usuario WHERE empresa = '".$_COOKIE["empresa"]."' AND user = '".$targeruser."' LIMIT 1 ");
			$funcao     = mysql_result($resultuser,0,"funcao");
			
			if(strpos(@mysql_result($resultuser,0,"email"),"@")===false)
			{
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O respons�vel n�o possui e-mail cadastrado.</td></tr></table></body>');
			}
			else
			{
				if($funcao==11 && $_COOKIE["empresa"]=="bradesco") // se o alvo for gerente administrativo
				{
					$sql = "SELECT * FROM CC_Log 
							WHERE empresa = '".$_COOKIE["empr"]."' AND user = '".$_COOKIE["user"]."' AND indice = '109' 
							AND log LIKE '%;ncid:".$nc_id.';user:'.$targeruser.';nctp:'.$nc_tp.";%' AND time LIKE '".date("Y-m-d")."%' LIMIT 1 ";
					if(mysql_num_rows(mysql_query($sql))>0)
					{
						echo('<tr bgcolor="#ffff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> j� foi enviado anteriormente. ['.$nc_id.']</td></tr></table></body>');
						exit(0);
					}
					//if(!$test) addLog('109','ncid:'.$nc_id.';user:'.$targeruser.';nctp:'.$nc_tp.';msg:'.$nc_msg.';fnct:11;');
					
					$texto_acoes = '';
					if($nc_tp<3)
					{
						for($nc_msg_aux=1;$nc_msg_aux<5;$nc_msg_aux++)
						{
							if(mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg_aux])!='' && $targeruser==mysql_result($result,0,$nc_tabls[$nc_msg_aux]))
							{
								$texto_acoes.= '<b style="color:#220000">'.$nc_acoes[$nc_msg_aux].':</b><br>
									'.mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg_aux]).'<br><b>
									Prazo: </b>'.mysql_result($result,0,"c".(intval(substr($nc_vars[$nc_tp][$nc_msg_aux],1))+1)).'<br><br>';
							}
						}
					}
					elseif($nc_tp==3)
					{
						for($nc_msg_aux=3;$nc_msg_aux<5;$nc_msg_aux++)
						{
							if(mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg_aux])!='' && $targeruser==mysql_result($result,0,$nc_tabls[$nc_msg_aux]))
							{
								$texto_acoes.= '<b style="color:#220000">'.$nc_acoes[$nc_msg_aux].':</b><br>
									'.mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg_aux]).'<br><b>
									Prazo: </b>'.mysql_result($result,0,"c".(intval(substr($nc_vars[$nc_tp][$nc_msg_aux],1))+1)).'<br><br>';
							}
						}
					}
					elseif($nc_tp==4)
					{
						$nc_msg_aux=4;
						if(mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg_aux])!='' && $targeruser==mysql_result($result,0,$nc_tabls[$nc_msg_aux]))
						{
							$texto_acoes.= '<b style="color:#220000">'.$nc_acoes[$nc_msg_aux].':</b><br>
								'.mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg_aux]).'<br><b>
								Prazo: </b>'.mysql_result($result,0,"c".(intval(substr($nc_vars[$nc_tp][$nc_msg_aux],1))+1)).'<br><br>';
						}
					}
					
					/*
					if($_COOKIE["user"]=="gustavo")
					{
						$reenvio = '<br><br>
							Pedimos que desconsidere o e-mail referente � '.$nc_nomes[$nc_tp].' ID: '.mysql_result($result,0,"c2").' enviado dia 03/01/2014.
							Os textos adequados para as a��es recomendadas seguem abaixo, no corpo deste e-mail.
						';
					} //*/
					//<div align="right">'.$dataescrita.'</div>
					$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
					<table width="95%" align="center" border="0" cellpadding="5" cellspacing="0" style="border:2px solid #000000;border-top:1px solid #999999;border-left:1px solid #999999;">
					<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr> 
						<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
						<td width="445"><div align="left"><font size="+1"><strong>'.mysql_result($resultempr,0,"razaoSocial").'</strong></font><br>
						SA8000 - Apontamentos da Avalia��o de Conformidade realizada de 20 a 31.10.2014
						</div>
						</td>
						</tr>
						<tr> 
						<td colspan="2">
							<blockquote>
							<b><font size="+1">Prezado Gerente Administrativo - '.mysql_result($resultuser,0,"nome").',</font></b>
							'.$reenvio.'
							<br><br>
							Com o objetivo de aprimorarmos o processo de tratamento de N�o-conformidades e Oportunidades de Melhoria registradas 
							em sua depend�ncia durante as auditorias SA8000, disponibilizamos o envio autom�tico de recomenda��es de a��es a serem 
							implementadas, DE IMEDIATO, em sua ag�ncia.
							<br>
							Assim que tomar conhecimento de seus apontamentos, conforme este email, o Gerente Administrativo dever�: 
							<ol>
							<li>Implementar e/ou executar a A��o Imediata e Corretiva, conforme recomendado, dentro dos prazos estabelecidos;</li>
							<li>Gerar ou coletar evid�ncias das a��es implementadas e/ou executadas;</li>
							<li>Registrar na ferramenta Sym, nos campos A��o Realizada, as a��es que de fato foram implementadas e/ou executadas na ag�ncia, considerando as datas de execu��o, que devem ser registradas no campo Data de Realiza��o;</li>
							<li>Salvar as a��es registradas.</li>
							</ol>
							<b style="color:#220000">Apontamento de Auditoria da Ag�ncia: ID '.$nc_id.'</b><br>
							<b style="color:#220000">Descri��o do Fato: </b><br>
							'.mysql_result($result,0,$nc_vars[$nc_tp][5]).'<br><br>'.$texto_acoes.'
							O acesso ao sistema Sym pode ser feito utilizando o site abaixo:
							<br><br>
							<table cellspagin="0" cellpadding="0" border="0"><tr><td>https://</td><td>www.</td><td>techsocial.</td><td>com.</td><td>br/sym/</td></table>
							<br>
							Ao acessar o sistema, voc� encontrar� na op��o "In�cio" e na aba "Minhas Atividades" os apontamentos que s�o de sua responsabilidade.
							<br><br>
							Em caso de d�vidas sobre login e senha de acesso ou outras informa��es, contatar:
							<ul>
							<li>Eliane Soares � esoares@keyassociados.com.br � (11) 3684-5038 </li>
							</ul>
							</blockquote>
						<br>
						BANCO BRADESCO S.A. <br>
						4240 / Departamento de Recursos Humanos <br>
						Responsabilidade Social e Clima <br> &nbsp;
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
					</body>
					</html>';
					
					$subject = "SA8000 - Apontamentos da Avalia��o de Conformidade realizada de 20 a 31.10.2014";
					//https://www.techsocial.com.br/sym/redirect.php?redr='.createRedirectLink("OCUR",$nc_id).'&t='.time().'
				}
				else //caso contrario, se nao for gerente administrativo
				{
					//if(!$test) addLog('109','empr:'.$empr.';ncid:'.$nc_id.';user:'.$targeruser.';nctp:'.$nc_tp.';msg:'.$nc_msg.'');
					
					$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
					<table width="90%" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
					<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr> 
						<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
						<td width="445"><div align="left"><font size="+1"><strong>'.mysql_result($resultempr,0,"razaoSocial").'</strong></font><br>
						Controle de Ocorr�ncias</div>
						<div align="right">'.$dataescrita.'</div></td>
						</tr>
						<tr> 
						<td colspan="2">'.($funcao==11?'':'<p><hr><br>Prezado(a) '.mysql_result($resultuser,0,"nome").',</p>').'
							<blockquote> 
							Solicitamos suas providencias para atendimento � <b>'.$nc_nomes[$nc_tp].'</b> registrada no Gerenciador Sym para o SGRS '.translateNorm($hall[1]).' '.ucfirst($_COOKIE["empresa"]).', c�digo identificador ID: '.$nc_id.'<br><br>
							<b style="color:#220000">Descri��o do Fato: </b><br>
							'.mysql_result($result,0,$nc_vars[$nc_tp][5]).'<br><br>
							<b style="color:#220000">Provid�ncia solicitada: '.$nc_acoes[$nc_msg].'</b><br>
							'.mysql_result($result,0,$nc_vars[$nc_tp][$nc_msg]).'<br><b>
							Prazo: </b>'.mysql_result($result,0,"c".(intval(substr($nc_vars[$nc_tp][$nc_msg],1))+1)).'<br><br>
							<b style="color:#220000">Atribu�do por: </b>'.mysql_result($sys__user,0,"nome").'<br><br>
							O acesso ao sistema Sym pode ser feito utilizando o link abaixo (copie ele e cole no seu navegador):<br><br>
							<table cellspagin="0" cellpadding="0" border="0"><tr><td>https://</td><td>www.</td><td>techsocial.</td><td>com.</td><td>br/sym/</td></table>
							<br><br>
							Por favor, fa�a uso deste link para ser redirecionado exatamente �s a��es que lhe foram atribu�das.
							</blockquote>
						<br>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
					</body>
					</html>';
					
					$subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				}
				
				//fazendo o email
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = ($hall[1]=='s8k'?'SA8000 - ':'')."KEYASSOCIADOS - Sym";
				$mail->Subject = $subject;
				$mail->AltBody = "";
				
				$mail->Body = $emailHTML;
				
				if(!$test) $mail->AddAddress(mysql_result($resultuser,0,"email"), mysql_result($resultuser,0,"nome"));
				else       $mail->AddAddress('gmonteiro@keyassociados.com.br','Gustavo M. Monteiro');
				
				$error = false;
				//if(false) {
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
					$error = true;
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
					addLog('109','ncid:'.$nc_id.';user:'.$targeruser.';nctp:'.$nc_tp.';msg:'.$nc_msg.';fnct:11;');
				}
				
				if($funcao==11 && $_COOKIE["empresa"]=="bradesco" && !$test && !$error)
				{
					//*fazendo o email
					$mail = new PHPMailer();
					$mail->IsSMTP(); $mail->SMTPSecure = "ssl"; $mail->Host = "186.211.113.107:25";
					$mail->SMTPAuth = true; $mail->Username = "sym@keyassociados.com.br"; $mail->Password = "VOkez9KS";
					$mail->IsHTML(true);
					$mail->From    = "sym@keyassociados.com.br"; $mail->FromName = ($hall[1]=='s8k'?'SA8000 - ':'')."KEYASSOCIADOS - Sym";
					$mail->Subject = $subject; //"Sym - Controle de Ocorr�ncias ".date("d/m/Y"); $mail->AltBody = "";
					$mail->Body = $emailHTML;
					//$mail->AddAddress('4240.vidoto@bradesco.com.br','Danilo Vidoto');
					$mail->AddAddress('esoares@keyassociados.com.br'  ,'Eliane Soares');
					$mail->AddAddress('4160.vanessac@bradesco.com.br' ,'Vanessa Cristiane Barbosa Silva');
					$mail->AddAddress('gmonteiro@keyassociados.com.br','Gustavo M. Monteiro');
					$mail->Send(); /*/
					//fazendo o email
					$mail = new PHPMailer();
					$mail->IsSMTP(); $mail->SMTPSecure = "ssl"; $mail->Host = "186.211.113.107:25";
					$mail->SMTPAuth = true; $mail->Username = "sym@keyassociados.com.br"; $mail->Password = "VOkez9KS";
					$mail->IsHTML(true);
					$mail->From    = "sym@keyassociados.com.br"; $mail->FromName = "KEYASSOCIADOS - Sym";
					$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y"); $mail->AltBody = "";
					$mail->Body = $emailHTML;
					$mail->AddAddress('gmonteiro@keyassociados.com.br','Gustavo M. Monteiro');
					$mail->Send(); //*/
				}
			}
		}
		else // se for 5 ou mais o tipo da verifica��o
		{
			$resultempr = mysql_query("SELECT razaoSocial FROM CC_Empresa WHERE empresa = '".$_COOKIE["empresa"]."' LIMIT 1 ");
			$resultuser = mysql_query("SELECT nome, email FROM CC_Usuario WHERE empresa = '".$_COOKIE["empresa"]."' AND user = '".mysql_result($result,0,$nc_tabls[$nc_msg])."' LIMIT 1 ") or die(mysql_error());
			
			if(strpos(mysql_result($resultuser,0,"email"),"@")===false)
			{
				echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O respons�vel n�o possui e-mail cadastrado.</td></tr></table></body>');
			}
			else
			{
				$emailHTML = '<html><head><title>Sym - Controle de Ocorr�ncias</title></head><body  style="font: medium \'Trebuchet MS\', Trebuchet, Verdana, Sans-serif;">
				<table width="600" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
				<tr>
				<td>
					<table width="600" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					<td width="155"><img src="http://www.keyassociados.com.br/sym/images/logo_novo.png" width="150" height="100"></td>
					<td width="445"><div align="left"><font size="+1"><strong>Controle de Ocorr�ncias</strong></font><br>
					'.mysql_result($resultempr,0,"razaoSocial").'</div>
					<div align="right">'.$dataescrita.'</div></td>
					</tr>
					<tr> 
					<td colspan="2"><p><hr><br>Prezado(a) '.mysql_result($resultuser,0,"nome").',</p>
						<blockquote> 
						Solicitamos suas providencias para atendimento � <b>'.$nc_nomes[$nc_tp].'</b> registrada no Gerenciador Sym para o SGRS '.translateNorm($hall[1]).' '.ucfirst($_COOKIE["empresa"]).', c�digo identificador ID: '.$nc_id.'<br><br>
						<b style="color:#220000">Descri��o do Fato: </b><br>
						'.mysql_result($result,0,$nc_vars[$nc_tp][8]).'<br><br>
						<b style="color:#220000">Provid�ncia solicitada: '.$nc_etapa[$nc_etp].'</b><br><br>
						O acesso ao sistema Sym deve ser feito <a href="http://www.techsocial.com.br/sym/redirect.php?redr='.createRedirectLink("OCUR",$nc_id).'">clicando aqui</a>.
						</blockquote>
					<br>
					</td>
					</tr>
					</table>
				</td>
				</tr>
				</table>
				</body>
				</html>';
				
				//fazendo o email
				$mail = new PHPMailer();
				$mail->IsSMTP(); // Define que a mensagem ser� SMTP
				$mail->SMTPSecure = "ssl";
				$mail->Host = "186.211.113.107:25";
				$mail->SMTPAuth = true;
				$mail->Username = "sym@keyassociados.com.br";
				$mail->Password = "VOkez9KS";
				$mail->IsHTML(true);
				
				$mail->From     = "sym@keyassociados.com.br";
				$mail->FromName = "KEYASSOCIADOS - Sym";
				$mail->Subject = "Sym - Controle de Ocorr�ncias ".date("d/m/Y");
				$mail->AltBody = "";
				
				$mail->Body = $emailHTML;
				#$mail->PluginDir = "mail/";
				
				if(!$test) $mail->AddAddress(mysql_result($resultuser,0,"email"), mysql_result($resultuser,0,"nome"));
				else       $mail->AddAddress('gmonteiro@keyassociados.com.br','Gustavo M. Monteiro');
				
				if(!$mail->Send()) {
					echo('<tr bgcolor="#ff9999"><td>'.$img_fail.'&nbsp;<b><font color="#FF0000">ERRO: </font></b> O sistema n�o conseguiu enviar o e-mail.</td></tr></table></body>');
				} else {
					echo('<tr bgcolor="#99ff99"><td>'.$img_success.'&nbsp;E-mail de <b>'.$nc_acoes[$nc_msg].'</b> enviado com sucesso.</td></tr></table></body>');
				}
			}
		}
	}
}
?>
</table></body></html>