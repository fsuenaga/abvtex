<?PHP
	header("Expires: 0");
	
	list($usec, $sec) = explode(' ', microtime());
    $script_start = (float) $sec + (float) $usec;

	if("".$_POST["empresa"]=="")                    die("");
	if("".$_POST["user"]   =="")                    die("");
	if($_POST["subpass"]!=3*intval(date("mYd"),10)) die("");
	
	require("../key.inc");
	require("class.phpmailer.php");
	
	$strcleaner = array("\n","\r",chr(160)," ");
	
	if(strpos($_POST["user"],"@")===false)
	{
	$sql = "SELECT * 
			FROM LYS_Pessoa p
			LEFT JOIN LYS_Resposta r ON p.empresa = r.empresa 
			AND p.user = r.user 
			AND r.feito <> 1 
			WHERE p.empresa = '".str_replace("'","",str_replace("\\","",$_POST["empresa"]))."' 
			AND p.emailWork IS NOT NULL 
			AND p.emailWork LIKE '%@%' 
			AND p.user = '".str_replace("'","",str_replace("\\","",$_POST["user"]))."' 
			AND p.data = '13/01/2012' 
			LIMIT 1 ";
	}
	else
	{
	$sql = "SELECT * 
			FROM LYS_Pessoa p
			LEFT JOIN LYS_Resposta r ON p.empresa = r.empresa 
			AND p.user = r.user 
			AND r.feito <> 1 
			WHERE p.empresa = '".str_replace("'","",str_replace("\\","",$_POST["empresa"]))."' 
			AND p.emailWork = '".str_replace("'","",str_replace("\\","",$_POST["user"]))."' 
			AND p.data = '13/01/2012' 
			LIMIT 1 ";
	}
	$result = mysql_query($sql) or die(mysql_error()); $linhas = mysql_num_rows($result);
	
	$aus2='';
	for($i=0,$e=0;$i<$linhas;$i++)
	{
		$body  = '<html><head><title>An�lise de Ader�ncia � Pol�tica</title></head><body>
<p><table width="100%" bgcolor="#22229B" style="color:white;">
<tr><td>An�lise do Grau de Ader�ncia da <br>
Pol�tica de Gerenciamento dos Recursos Humanos da Organiza��o Bradesco</td></tr></table></p>
<p><font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">Prezado(a) colaborador(a),</font>&nbsp;</p>
<p><font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">&nbsp;</font>&nbsp;<br></p>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">A </font>
<font size="3" color="#000080" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">KEY</font><font size="3" color="#ff6600" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">ASSOCIADOS</font>
<font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">
 agradece a oportunidade de conduzir o projeto 
de <b>an�lise do grau de ader�ncia da Pol�tica 
de Gerenciamento dos Recursos Humanos da Organiza��o Bradesco (ANAP)</b> 
<u>nas depend�ncias e �reas de neg�cio do pr�dio Av. Paulista 1.450</u>, 
coordenada pela Ger�ncia de Responsabilidade Social, Qualidade de Vida e 
Clima do Departamento de Recursos Humanos. </font>&nbsp;<br></p>

<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">A metodologia consiste na interpreta��o da Pol�tica por meio de frases 
que expressam atitudes do dia-a-dia dos colaboradores. Esses analisam 
as afirmativas e indicam quanto <b>� em que grau �<b> essas afirmativas 
representam e integram suas percep��es sobre o ambiente e rela��es de trabalho. 
O resultado � tabulado, permitindo identificar quais os pontos fortes 
� alto grau de implementa��o � e quais pontos merecem aten��o e recomenda��es 
de tratamento. A forma de aplica��o, em meio eletr�nico, com senha exclusiva 
de acesso e armazenamento de dados em servidor externo independente, garantem 
a preserva��o da identidade dos participantes, permitindo que todos possam 
se expressar livremente. </font>&nbsp;<br></p>

<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">A partir da an�lise dos resultados, 
um planejamento de atividades ser� desenvolvido, 
visando promover o alinhamento da cultura organizacional 
com as premissas da Pol�tica de Gerenciamento dos Recursos 
Humanos. A �ltima fase deste ciclo trata da divulga��o de 
resultados e a��es estabelecidas. </font>&nbsp;<br></p>

<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">Contando 
com a participa��o de todos, convidamos os colaboradores a participarem 
deste Projeto, acessando o seguinte link abaixo, <b>no per�odo de 16 a 31/01/2012</b> e 
por meio login e senha abaixo:</font>&nbsp;<br></p>
<p align="center"><font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif"><b>&lt; http://www.techsocial.com.br/sym/anap &gt;</b></font>&nbsp;<br/></p>
<p><strong><font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">Usu�rio: '.mysql_result($result,$i,"p.user").'<br/>
Senha: '.mysql_result($result,$i,"p.pass").'</font></strong></p>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="3" face="\'Trebuchet MS\', Trebuchet, Verdana, Sans-serif">Em 
caso de d�vidas, seguem as op��es de contato/suporte t�cnico:
<ul>
<li>
<b>KEYASSOCIADOS:</b> 
<ol>
<li>
<b>Reenvio de senha:</b> op��o dispon�vel no site da ferramenta ANAP, conforme link acima;
</li>
<li>
<b>Chat (suporte t�cnico on-line):</b> op��o dispon�vel no site da ferramenta ANAP, conforme link acima;
</li>
<li>
<b>Por meio da consultora Eliane Soares</b>, no ramal 4-9140, no tel.: 9994-0105 ou pelo e-mail esoares@keyassociados.com.br.
</li>

</li>
<li>
<b>Em sua depend�ncia:</b> por meio do Multiplicador SA 8000;
</li>
<li>
<b>DRH - Departamento de Recursos Humanos:</b> 
Claudirene dos Santos Marcelino, por meio do ramal 4-9140 ou do e-mail 4240.claudirene@bradesco.com.br
</li>
</ul>
<br>&nbsp;<br></p></body></html>';
		
		//fazendo o email
		$mail    = new PHPMailer();
		$mail->IsSMTP(); // Define que a mensagem ser� SMTP
		$mail->Host = "localhost"; // Endere�o do servidor SMTP
		$mail->IsHTML(true);
		
		$mail->From     = "gmonteiro@keyassociados.com.br";
		$mail->FromName = "An�lise de Ader�ncia � Pol�tica ".(rand(0,1)==0?",":".")." Bradesco";
		$mail->Subject  = "An�lise de Ader�ncia � Pol�tica ".(rand(0,1)==0?"-":".")." Bradesco (v".rand(1000,9999).")";
		$mail->AltBody  = ""; // optional, comment out and test
		
		$mail->Body     = $body;
		#$mail->PluginDir = "mail/";
		
		//$mail->AddAddress("gustavomont@gmail.com", "Gustavo Monteiro");
		$email = strtolower(trim(str_replace($strcleaner,"",mysql_result($result,$i,"p.emailWork"))));
		$mail->AddAddress($email, mysql_result($result,$i,"p.nome"));
		
		if(!$mail->Send()) {
			$aus = ord($email[strlen($email)-1])." ".ord($email[strlen($email)-2]);
			$aus2.='<div style="background-color:#FFFFFF; color: #FF0000; border: 1px #FF0000 solid;" width="400"><span width="300">ERRO!</span> - <span width="100">'.strtolower(mysql_result($result,$i,"p.emailWork")).'</span> - <span width="100">'.mysql_result($result,$i,"p.user").'</span> - <span width="100">'.mysql_result($result,$i,"p.nome").' - '.mysql_result($result,$i,"p.data").' '.$aus.'</span></div>';
		} else {
			$e++;
			$aus2.='<div style="background-color:#FFFFFF; border: 1px #000000 solid;" width="400"><span width="300">Enviado</span> - <span width="100">'.strtolower(mysql_result($result,$i,"p.emailWork")).'</span> - <span width="100">'.mysql_result($result,$i,"p.user").'</span> - <span width="100">'.mysql_result($result,$i,"p.data").' | '.$mail->Subject.'</span></div>';
		}//*/
	}
	
	list($usec, $sec) = explode(' ', microtime());
    $script_end = (float) $sec + (float) $usec;
	
	$elapsed_time = round($script_end - $script_start, 5);
	
	if($e==0)
		echo("<script>alert('N�o foi poss�vel encontrar o usu�rio.'); history.back(); </script>");
	else
		echo("<script>alert('E-mail enviado com sucesso.'); window.open('http://www.techsocial.com.br/sym/anap','_self'); </script>");
		
	die();
?>