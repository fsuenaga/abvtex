<?PHP

###################################################################################################################
# fun��o codefy
# 
# Codifica e Descodifica uma entrada
# 
# @param  $str a string
# @return a tring codificada se n�o-codificada e vice-versa
# 
	
function codefy($str)
{
	$resp = "";
	if("".$str=="") return "";
	if("".$str[0] != "0")
	{
		$aux = str_replace(" ","^",$str);
		$max = strlen($aux);
		for($i=0;$i<$max;$i++,$aux=substr($aux,1)) $resp .= chr(ord($aux) + 1);
		return "0".$resp;
	}
	else
	{
		$aux = substr($str,1);
		$max = strlen($aux);
		for($i=0;$i<$max;$i++,$aux=substr($aux,1)) $resp .= chr(ord($aux) - 1);
		$resp = str_replace("^"," ",$resp);
		return $resp;
	}
}
