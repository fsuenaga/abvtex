<?PHP

#
# modulo library_functions
#
# autor: Gustavo Monteiro
# data: 17/01/2008
# vers�o: 1.0.0
# 
# descri��o: possui procedimentos espec�ficos para o sistema Sym
# 
###################################################################################################################

# Defini��es do Sistema - Nenhuma

###################################################################################################################
# fun��o origem
# 
# Retorna o valor string referente � origem em num�rico
# 
# @param  $origem a origem (pode ser em string ou integer)
# @return o nome completo da origem
# 

   function origem($origem,$max=false,$empr="")
   {
	   static $vetor;
	   static $start = false;
	   
	   if(!$start)
	   {
	       if($empr=="") $aue = $_COOKIE["empresa"]; else $aue = $empr;
		   $result = mysql_query("SELECT numero, nome FROM CC_Origem WHERE empresa = '".$aue."' ORDER BY nome ");
		   for($i=0;$i<mysql_num_rows($result);$i++) $vetor[mysql_result($result,$i,"numero")] = mysql_result($result,$i,"nome");
		   $start = true;
	   }
	   
	   if($max) return sizeof($vetor);
	   else     return $vetor[$origem];
   }
	
###################################################################################################################
# fun��o codefy
# 
# Codifica e Descodifica uma entrada
# 
# @param  $str a string
# @return a tring codificada se n�o-codificada e vice-versa
# 
	
	function codefy($str)
	{
		$resp = "";
		if("".$str=="") return "";
		if("".$str[0] != "0")
		{
			$aux = str_replace(" ","^",$str);
			$max = strlen($aux);
			for($i=0;$i<$max;$i++,$aux=substr($aux,1)) $resp .= chr(ord($aux) + 1);
			return "0".$resp;
		}
		else
		{
			$aux = substr($str,1);
			$max = strlen($aux);
			for($i=0;$i<$max;$i++,$aux=substr($aux,1)) $resp .= chr(ord($aux) - 1);
			$resp = str_replace("^"," ",$resp);
			return $resp;
		}
	}
	function codefyFuzzy($str,$fuzzy=2)
	{
		$resp = "";
		if("".$str=="") return "";
		if("".$str[0] != "0")
		{
			$aux = str_replace(" ","^",$str);
			$max = strlen($aux);
			for($i=0;$i<$max;$i++,$aux=substr($aux,1)) $resp .= chr(ord($aux) + (($i%$fuzzy) + 1));
			return "0".$resp;
		}
		else
		{
			$aux = substr($str,1);
			$max = strlen($aux);
			for($i=0;$i<$max;$i++,$aux=substr($aux,1)) $resp .= chr(ord($aux) - (($i%$fuzzy) + 1));
			$resp = str_replace("^"," ",$resp);
			return $resp;
		}
	}
	
	
###################################################################################################################
# fun��o translateNorm
# 
# Traduz a norma pro codigo do sistema e vice-versa
# 
# @param  $str o norma da norma
# @return a string traduzida
# 
	function translateNorm($str)
	{
		if(strlen($str)<=0) return "undefined";
		if($str[strlen($str)-1] == "k") //decodifica
		{
			switch($str)
			{
			case "s8k"     : return "SA 8000";
			case "ssok"    : return "SSO";
			case "lxk"     : return "Monitoramento de Requisitos Legais";
			case "stk"     : return "Stakeholders";
			case "gisk"    : return "GIS";
			case "crmk"    : return "CRM";
			case "sgak"    : return "Gest�o Ambiental";
			case "frk"     : return "Gest�o da Cadeia de Fornecedores";
			case "gesk"    : return "Gest�o";
			case "kmk"     : return "Gest�o do Conhecimento";
			case "onak"    : return "ONA";
			case "nbr16k"  : return "NBR 16001";
			case "nbr17k"  : return "NBR 17025";
			case "i09k"    : return "ISO 9001";
			case "i14k"    : return "ISO 14001";
			case "o18k"    : return "OHSAS 18001";
			case "s8_smp_k": return "WS SA 8000";
			}
		}
		else //codifica
		{
			switch($str)
			{
			case "SA 8000"                            : return "s8k";
			case "SSO"                                : return "ssok";
			case "Monitoramento de Requisitos Legais" : return "lxk";
			case "Stakeholders"                       : return "stk";
			case "GIS"                                : return "gisk";
			case "CRM"                                : return "crmk";
			case "Gest�o Ambiental"                   : return "sgak";
			case "Gest�o da Cadeia de Fornecedores"   : return "frk";
			case "Gest�o"                             : return "gesk";
			case "Gest�o do Conhecimento"             : return "kmk";
			case "NBR 16001"                          : return "nbr16k";
			case "NBR 17025"                          : return "nbr17k";
			case "ONA"                                : return "onak";
			case "ISO 9001"                           : return "i09k";
			case "ISO 14001"                          : return "i14k";
			case "OHSAS 18001"                        : return "o18k";
			case "WS SA 8000"                          : return "s8_smp_k";
			}
		}
		return "undefined";
	}
	
	
###################################################################################################################
# fun��o translateNorm
# 
# Traduz a norma pro codigo do sistema e vice-versa
# 
# @param  $str o norma da norma
# @return a string traduzida
# 
	function translateMinNorm($str)
	{
		if(strlen($str)<=0) return "undefined";
		if($str[strlen($str)-1] == "k") //decodifica
		{
			switch($str)
			{
			case "s8k"    : return "SA8000";
			case "ssok"   : return "SSO";
			case "sgak"   : return "SGA";
			case "gesk"   : return "Gest�o";
			case "gisk"   : return "GIS";
			case "kmk"    : return "KM";
			case "lxk"    : return "Lex";
			case "nbr16k" : return "NBR16001";
			case "nbr17k" : return "NBR17025";
			case "i09k"   : return "ISO9001";
			case "i14k"   : return "ISO14001";
			case "o18k"   : return "OHSAS18001";
			}
		}
		else //codifica
		{
			switch($str)
			{
			case "SA8000"     : return "s8k";
			case "SSO"        : return "ssok";
			case "SGA"        : return "sgak";
			case "Gest�o"     : return "gesk";
			case "GIS"        : return "gisk";
			case "KM"         : return "kmk";
			case "Lax"        : return "lxk";
			case "NBR16001"   : return "nbr16k";
			case "NBR17025"   : return "nbr17k";
			case "ISO9001"    : return "i09k";
			case "ISO14001"   : return "i14k";
			case "OHSAS18001" : return "o18k";
			}
		}
		return "undefined";
	}

#### troca o ascii de acento por vogal
function switchChar($chr)
{
	if($chr>=224&&$chr<=227)  return 97;  //a
	if($chr>=232&&$chr<=234)  return 101; //e
	if($chr>=236&&$chr<=238)  return 105; //i
	if($chr>=242&&$chr<=245)  return 111; //o
	if($chr>=249&&$chr<=252)  return 117; //u
	return $chr;
}

#### troca o ascii de acento por vogal
function removerAcentos($str)
{
	static $arr = array(
		'a' => array('�','�','�','�'),
		'e' => array('�','�','�'),
		'i' => array('�','�','�'),
		'o' => array('�','�','�','�'),
		'u' => array('�','�','�'),
		'c' => array('�'),
		'A' => array('�','�','�','�'),
		'E' => array('�','�','�'),
		'I' => array('�','�','�'),
		'O' => array('�','�','�','�'),
		'U' => array('�','�','�'),
		'C' => array('�')
		);
	
	foreach($arr as $key => &$arr2)
	{
		$str = str_replace($arr2,$key,$str);
	}
	return $str;
}

###################################################################################################################
# fun��o compareString
# 
# compara duas strings
# 
# @param  $str1, $str2 as strings a serem comparadas
# @return < 0 se str1 menor que str2;
#         > 0 caso contrario;
#		  0 se ambas sao iguais
# 
	
	function compareString($str1,$str2)
	{
		if(strlen($str1) > strlen($str2)) $max = strlen($str2);
		else $max = strlen($str1);
		
		for($i=0;$i<$max;$i++)
		{
			$i1 = switchChar(ord(strtolower($str1[$i]))); 
			$i2 = switchChar(ord(strtolower($str2[$i])));
			echo('<!-- '.$i1.' '.$i2.' //-->');
			if($i1>$i2)      return 1;
			else if($i1<$i2) return -1;
		}
		if(strlen($str1) > strlen($str2))      return 1;
		else if(strlen($str1) < strlen($str2)) return -1;
		return 0;
	}

###################################################################################################################
# fun��o captchaMaker
# 
# faz uma sctring com letras randomicas
# 
# @param  $lenght o tamanho da string
# @return a string captcha
# 	
	
	function captchaMaker($lenght = 5)
	{	
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
		$chave = "";
		for ($j = 0; $j < $lenght; $j++)
		{
			$pos = rand(0, 34);
			$chave .= $chars[$pos];
		}
		return $chave;
	}
?>